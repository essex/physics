#include "matfuncs.h"

#ifdef GHOST_IDX64_GLOBAL
  #define idx_t uint64_t
#else
  #define idx_t uint32_t
#endif


int cols_compar(  const void *a, const void *b ){ 
	return (int)(*(ghost_gidx *)a - *(ghost_gidx *)b );
	}

void cols_qsort(ghost_gidx *cols, char * vals ,  size_t size_v ,  ghost_gidx n ){
	
	size_t size_i = sizeof(ghost_gidx);
	size_t size   = size_v + size_i;
	char * base   = malloc( n*size );
    if (!base) {
        printf("abort -> malloc failed!\n");
        exit(0);
    }
	ghost_gidx i;
	for(i=0;i<n;i++) {
		memcpy( base + i*size,          cols + i       , size_i);
		memcpy( base + i*size + size_i, vals + i*size_v, size_v);}
	
	qsort ( base, n, size , cols_compar);
	
	for(i=0;i<n;i++) {
		memcpy( cols + i       , base + i*size         , size_i);
		memcpy( vals + i*size_v, base + i*size + size_i, size_v);}
	
	free(base);
}

int cols_error_check(ghost_gidx dim, ghost_gidx row, ghost_gidx nnz,  ghost_gidx * cols ){
	
	ghost_gidx i;
	for(i=0; i<nnz; i++) if( (cols[i] >= dim) || (cols[i] < 0) ) {printf("error: row %"PRIDX": col[%"PRIDX"] = %"PRIDX" >= %"PRIDX" \n", row, i, cols[i], dim); exit(0);}
	
	return 0;
	
	}


//#define DEBUG

#include <limits.h>

char * int2bin(int i)
{
    size_t bits = sizeof(int) * CHAR_BIT;

    char * str = malloc(bits + 1);
    if(!str) return NULL;
    str[bits] = 0;

    // type punning because signed shift is implementation-defined
    unsigned u = *(unsigned *)&i;
    for(; bits--; u >>= 1)
    	str[bits] = u & 1 ? '1' : '0';

    return str;
}

#define BIT_OR  |
#define BIT_AND &
#define BIT_XOR ^
#define BIT_NOT ~
#define BIT_SHIFT_L <<
#define BIT_SHIFT_R >>

#define bitor( a, b ) (((idx_t)(a))|((idx_t)(b)))
#define bitand( a, b ) (((idx_t)(a))&((idx_t)(b)))
#define bitxor( a, b ) (((idx_t)(a))^((idx_t)(b)))
#define bitnot( a ) (~((idx_t)(a)))
#define bitshift_l( a, b ) (((idx_t)(a))<<((idx_t)(b)))
#define bitshift_r( a, b ) (((idx_t)(a))>>((idx_t)(b)))
#define bitor( a, b ) (((idx_t)(a))|((idx_t)(b)))


/*
idx_t bitor( idx_t a,  idx_t b ){
	return a|b;
}

idx_t bitand( idx_t a,  idx_t b ){
	return a&b;
}

idx_t bitxor( idx_t a,  idx_t b ){
	return a^b;
}

idx_t bitnot( idx_t a ){
	return ~a;
}


idx_t bitshift_l( idx_t z,  int32_t n ){
	ghost_gidx i;
	
#ifdef GHOST_HAVE_LONGIDX_
	ghost_gidx r=0;
	uint32_t tmp;
	uint32_t * t0 = (uint32_t *)( &z);
	uint32_t * t1 = (uint32_t *)( &r);
	
	if (n < 32 ){
		t1[0] = t0[0] << n; t1[1] = t0[1] << n;
		tmp =  t0[0] >> (32-n);
		t1[1] |=  tmp;
	}
	else{
		tmp =  t0[0] << (n-32);
		t1[1] |=  tmp;
	}
	return r;
	
	//if (n > 0) for( i=0; i<n; i++ ) z *= 2;
	//else       for( i=0; i<n; i++ ) z /= 2;
	//return z;
#else
	return z << n;
#endif
	}

idx_t bitshift_r( idx_t z,  int32_t  n ){
	idx_t i;

#ifdef GHOST_HAVE_LONGIDX_
		idx_t r=0;
	uint32_t tmp;
	uint32_t * t0 = (uint32_t *)( &z);
	uint32_t * t1 = (uint32_t *)( &r);
	
	if (n < 32 ){
		t1[0] = t0[0] >> n; t1[1] = t0[1] >> n;
		tmp =  t0[1] << (32-n);
		t1[0] |=  tmp;
	}
	else{
		tmp =  t0[1] >> (n-32);
		t1[0] |=  tmp;
	}
	return r;
	
	
	//if (n > 0) for( i=0; i<n; i++ ) z /= 2;
	//else       for( i=0; i<n; i++ ) z *= 2;
	//return z;
#else
	return z >> n;
#endif
	}
*/

#define CHECK_DIFF_NN(  target  , nn_mask ) ( !((nn_mask == ( bitand(nn_mask , target) )) || !( bitand(nn_mask , target) )) )

idx_t ishftc( idx_t i, idx_t s, idx_t L ){
	if(s<0) s += L;
	//return  ((1 << L ) - 1) & ((i << s) | ( i >> (L-s)) );
	return  bitand((bitshift_l(1 , L ) - 1) , bitor(bitshift_l(i , s) , bitshift_r( i , (L-s)) ));
	
}

idx_t power_of_2( idx_t n){
	//return (ghost_gidx)(1) << n;
	return bitshift_l( 1 , n );
	}


idx_t Binomial(idx_t N, ghost_gidx k){
	
	if((k> N)||(k< 0)) return 0;
	if((k==N)||(k==0)) return 1;
	
	if( 2*k > N ) k=N-k;
	idx_t i;
	idx_t B=1;
	for (i=1;i<=k;i++) B = (B*((N-k)+i))/i;
	
	return B;
	}

idx_t bitcount(idx_t i){
	
	idx_t count = 0;
	//while (count < i){	count++;
	//			i = i & (i-1);  }
	
	while ( i ){	count += bitand(i,1);
			//i >>= 1;
			i = bitshift_r( i,  1 );
			}
	return count;
	}



/* compare first index of pair of ints */
int cmpfst(  const void *a, const void *b ){ 
	int x,y;
	x=*(int *) a;
	y=*(int *) b;
	// prone to overflow - not here.
	return (a-b);
	}
	
/* naive version */
void CuthillMcKee(ghost_gidx n, ghost_gidx *rptr, ghost_gidx *cind, ghost_gidx * idperm) {
	ghost_gidx * vdeg, * newvd;
	ghost_gidx * alist, * newlist;
    
    int * isdone;
    // principally pairs of int
    int * sortarray;
    
    int ndone,ntodo,itotreat;

    int nzcrs,nalist,nnew;

    int i,idx,nodix;
    int tmpi;
    
    // does not account for duplicates
    nzcrs=0;
    for (i=0;i<n;i++) {nzcrs=MAX(nzcrs,rptr[i+1]-rptr[i]);}
    // printf("NZCRS: %d\n",nzcrs);
    //idperm =malloc(sizeof * idperm * n);
    vdeg   =malloc(sizeof * vdeg * n);
    alist  =malloc(sizeof * alist * nzcrs);
    newlist=malloc(sizeof * newlist * nzcrs);
    newvd  =malloc(sizeof * newvd * nzcrs);
    isdone =malloc(sizeof * isdone * n);
    //    sortarray=malloc((sizeof * newvd + sizeof * newlist) * nzcrs);
    sortarray=malloc(sizeof( * newvd) * (2*nzcrs));
        
    if (!vdeg || !alist || !newlist || !newvd || !isdone || !sortarray) {
        printf("abort -> malloc failed!\n");
        exit(0);
    }
    
    for (i=0;i<n;i++) {
       vdeg[i]=rptr[i+1]-1-rptr[i];
    }

    //create permutation

    for (i=0;i<n;i++) {
		isdone[i]=0;
	}

    //add a node with smallest vertex degree first
    ndone=0;
    ntodo=n;
    
    tmpi=nzcrs+1; //anything large enough
    idx=-1;
    for (i=0;i<n;i++) {
		if (vdeg[i]<tmpi) {
			tmpi=vdeg[i];
			idx=i;
		}
	}
    
    if (idx < 0) {printf("Error in CMK: idx < 0 \n"); exit(1);}
    
    // printf("Node with smallest vertex degree: %d\n",idx);
    
    idperm[0]=idx;
    ndone=1;
    isdone[idx]=1;
    itotreat=1;
  
    while (ndone < n) {
	//	printf("ndone: %d\n",ndone);
       //current node
       if (itotreat>ndone) {printf("Problem %d > %d\n",itotreat,ndone); exit(1);}
       nodix=idperm[itotreat-1];  // !from 0
       //construct adjacency list
       nalist=rptr[nodix+1]-rptr[nodix];
       for (i=0;i<nalist;i++) {
          alist[i]=cind[i+rptr[nodix]];
	   }
       //find index of new nodes
       nnew=0;
       for (i=0;i<nalist;i++) {
          if (isdone[alist[i]] == 0) {
             nnew=nnew+1;
             newlist[nnew-1]=alist[i];
             newvd[nnew-1]=vdeg[alist[i]];
             isdone[alist[i]]=1;
          }
       }
       
       //sort new nodes in order of increasing vertex degree
       
       // we cheat a little.
       for (i=0;i<nnew;i++)
       {
		   sortarray[2*i  ]=newvd[i];
		   sortarray[2*i+1]=newlist[i];
	   }
       qsort(sortarray,nnew,(sizeof * newvd + sizeof *newlist),cmpfst);
       for (i=0;i<nnew;i++)
       {
		   newlist[i]=sortarray[2*i+1];
	   }
              
       //and add them
       for (i=0;i<nnew;i++)
       { idperm[ndone+i]=newlist[i]; }
       
       ndone=ndone+nnew;
              
       itotreat=itotreat+1;
    }
    
    // check logic
    
    for (i=0;i<n;i++) {
		if ( isdone[i] == 0) 
		{  printf("error in CMK:Not all done\n"); exit(1);}
    }
 
  free(vdeg);
	free(alist);
	free(newlist);
	free(newvd);
	free(isdone);
    free(sortarray);
    
}


/* to be called as FORTRAN */
#ifdef GHOST_IDX64_GLOBAL
extern void smallspinmatrix64_(ghost_gidx *, ghost_gidx *, ghost_gidx *);
#define SMALLSPINMATRIX(L,rpt,col) smallspinmatrix64_(L,rpt,col)
#else
extern void smallspinmatrix32_(ghost_gidx *, ghost_gidx *, ghost_gidx *);
#define SMALLSPINMATRIX(L,rpt,col) smallspinmatrix32_(L,rpt,col)
#endif

void SpinChainSZLeadperm(ghost_gidx L, ghost_gidx * perm){
	ghost_gidx i;
	// small local sparse matrix pattern
	static ghost_gidx * rptr;
	static ghost_gidx * cind;
	int * checkit;
	
	int DoCMK;
	
	ghost_gidx ns, ne;
	
	ns=power_of_2(L);
	ne=(2*L+1)*ns;
	rptr = malloc(sizeof *rptr * (ns+1));
	cind = malloc(sizeof *cind * ne);
        
    if (!rptr || !cind) {
        printf("abort -> malloc failed!\n");
        exit(0);
    }
	
	DoCMK=1;
	
	if (DoCMK) 
	{
	
	  // printf("Generate leading permutation\n");
			
	printf("Call SmallSpinMatrix (FORTRAN)\n");
	//smallspinmatrix32_(&L,rptr,cind);
	SMALLSPINMATRIX(&L,rptr,cind);
	
	// for (i=0;i<ns+1;i++) {printf("%d : %d\n",i,rptr[i]);}
	
	// printf("Call CuthillMcKee\n");
	CuthillMcKee(ns,rptr,cind,perm);
	
	// printf("Permutation generated\n");
	//for (i=0;i<ns;i++) { printf("%d : %d\n",i,perm[i]); }
	
    }	
		else
	{
	  // printf("Use identity permutation\n");
		
		/* identity permutation */
		for(i=0;i<power_of_2(L);i++) 
		{
			//perm[i]=power_of_2(L)-i-1;
			perm[i]=i;
		}		
		
	}
	
	// printf("Check permutation: ");
	checkit = malloc(sizeof * checkit * ns);
   
    if (!checkit) {
        printf("abort -> malloc failed!\n");
        exit(0);
    }
    
    
    for (i=0;i<ns;i++) {checkit[i]=0;}
	for (i=0;i<ns;i++) {
		if (checkit[perm[i]])
		{printf("Permutation incorrect\n"); exit(1);}
		else {checkit[perm[i]]=1;}
		}
	free(checkit);
	// printf("OK\n");
				
	free(rptr);
	free(cind);
}



int SpinChainSZ( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg ){
    UNUSED(arg);

	static ghost_lidx L   = 0;
	static ghost_lidx NUp = 0;
	static double Jz  = 1.;
	static double Jxy = 1.;
	static double   disorder_strength  = 0.0;
	static double * disorder           = NULL;
	static int      disorder_seed      = 4123;
	static ghost_lidx useOBC = 0;
	useOBC &= 1;
	
	
	ghost_gidx Ns = Binomial( L, NUp);
	static idx_t * lutlead;
	static idx_t * luttrail;
	static idx_t * cnt;
	static idx_t * lutrev;
	static idx_t * revcnt;
	//static idx_t * imask;

	static ghost_lidx usePERM = 1;
	static idx_t * leadperm;
	static idx_t * invleadperm;
	
	// L+3 should suffice instead of 2L+1
	ghost_lidx max_row_nnz = 2*L+1;

	if ((row >-1) && (row <Ns)){
		
		double * dvals = vals;
		*nnz = 0;
		
		// binary search
		ghost_gidx min_il = 0;
		ghost_gidx max_il = power_of_2(L/2);
		ghost_gidx il = 0;
		ghost_lidx i;
		for(i = 0; i < L+1; i++)
		{
			il = (min_il + max_il)/2;
			if( row >= lutlead[il+1] )
				min_il = il+1;
			else if( max_il != min_il )
				max_il = il;
			else
				break;
		}

		// bad linear search, makes matrix building VERY expensive!
		//  ghost_gidx il = 0;
		//  while ( row >= lutlead[il+1] ) il++;
		
		
		idx_t bp;
		//bp = (il << (L/2)) + lutrev[ row - lutlead[il] + revcnt[NUp-bitcount(il)]]; 
		if(usePERM) bp = bitshift_l(leadperm[il] , (L/2)) + lutrev[ row - lutlead[il] + revcnt[NUp-bitcount(leadperm[il])]]; 
		else        bp = bitshift_l(         il  , (L/2)) + lutrev[ row - lutlead[il] + revcnt[NUp-bitcount(         il )]]; 
		if ( bitcount(bp) != NUp) {  printf("error 1\n"); exit(0);} 
		
		
		ghost_lidx hp = 0;
		
		idx_t l;
		
		 if( Jz != 0. ){
			for(l=0;l<L-useOBC;l++){
				//if (bitcount( bp &  ishftc( 1|2 , l , L)) == 1)   hp -= 1;
				if (CHECK_DIFF_NN (bp , ishftc( 1|2 , l , L)) )    hp -= 1;
				else                                              hp += 1;
				
				//int32_t tmp =  ishftc( (bp & imask[l]) , -l , L );   // ( xxXXxxxx & 00110000 ) ->  000000XX
				//switch(tmp){
				//	case 0: hp += 1; break;  // ..00
				//	case 1: hp -= 1; break;  // ..01
				//	case 2: hp -= 1; break;  // ..10
				//	case 3: hp += 1; break;  // ..11
				//	default: printf("error 2\n"); exit(0);
				//}
			}
		 }
		
		double dis_val = 0.;
		for(l=0;l<L;l++){
			if ( bitand (bp , bitshift_l( 1 , l ) ) )   dis_val -= disorder[l];
			else                                        dis_val += disorder[l];
		}
		
		if( hp != 0 || dis_val != 0. ){
			cols[ *nnz] = row;
			dvals[*nnz] = hp*Jz + dis_val;
			*nnz += 1;
			//printf("%ld  %g\n", row, dvals[*nnz]);
		}
		
		if( Jxy != 0. ){
			for(l=0;l<L-useOBC;l++){
				
				//int32_t tmp = ishftc( (bp & imask[l]), -l , L);
				//if( (tmp == 1) || (tmp == 2) ){
				//if( bitcount( bp &  ishftc( 1|2 , l , L) ) == 1 ){
				if (CHECK_DIFF_NN (bp , ishftc( 1|2 , l , L))) {
					
					//int32_t bp2 =  bp ^ imask[l];
					idx_t bp2 =  bitxor(bp , ishftc( 1|2 , l , L));
					
					//ghost_gidx lbp = bp2 >> (L/2);
					idx_t lbp = bitshift_r( bp2,  L/2 );
					idx_t tbp = bitand(bp2 , (power_of_2(L/2)-1));
					
					if(usePERM) cols[ *nnz] = lutlead[invleadperm[lbp]] + luttrail[tbp] -1;
					else        cols[ *nnz] = lutlead[            lbp]  + luttrail[tbp] -1;
					
					dvals[*nnz] = 2.*Jxy;
					*nnz += 1;
				}
			}
		 }
		
		cols_error_check( Ns , row, *nnz, cols );
		cols_qsort( cols, vals , sizeof(double) ,  *nnz );
		return 0;
		
	}else if( row == -1 ){
		
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
		info->nrows     = Ns;
		info->ncols     = Ns;
		info->row_nnz   = max_row_nnz;

		info->hermit    =  1;
		info->eig_info  =  0;
		//info->eig_down  = -8.;
		//info->eig_top   =  8.;

		return 0;
		}

	// =================================================================
	else if ( row == -2 ) {
		L      = nnz[0];
		NUp    = nnz[1];
		useOBC = nnz[2];
		if(L&1) { printf("abort -> need even L!\n"); exit(0); }
		lutlead  = malloc( sizeof(idx_t)* (power_of_2(L/2)+1));
		luttrail = malloc( sizeof(idx_t)*  power_of_2(L/2)   );
		lutrev   = malloc( sizeof(idx_t)* (power_of_2(L/2)+1));
		   cnt   = malloc( sizeof(idx_t)* (L+1) );
		revcnt   = malloc( sizeof(idx_t)* (L+1) );
		//imask    = malloc( sizeof(ghost_gidx)*  L    );
		
		idx_t i;
		
		disorder = malloc( sizeof(double)* L );
		srand(disorder_seed);
		for(i=0; i<L; i++)
			disorder[i] =  disorder_strength*((double)(rand())/RAND_MAX - 0.5);
		
		if (!lutlead || !luttrail || !lutrev || !cnt || !revcnt) {
			printf("abort -> malloc failed!\n");
			exit(0);
		}
		
		if(usePERM){
			leadperm = malloc( sizeof(idx_t)* power_of_2(L/2) );
			invleadperm = malloc( sizeof(idx_t)* power_of_2(L/2) );
			
			if (!leadperm || !invleadperm) {
				printf("abort -> malloc failed!\n");
				exit(0);
			}
			
		}
		
		
		*cols = Binomial( L, NUp);
		
		
		if(usePERM){
			SpinChainSZLeadperm(L/2,leadperm);
			for(i=0;i<power_of_2(L/2);i++) invleadperm[leadperm[i]]=i; //inverse permutation
		}
		
		
		for(i=0;i<=L;i++)    cnt[i] = 0;
		for(i=0;i<=L;i++) revcnt[i] = 0;
		
		
		for(i=0;i<power_of_2(L/2);i++){
			idx_t tmp = bitcount(i);
			cnt[tmp]++;
			luttrail[i] = cnt[tmp];
		}
		
		                  revcnt[0] = 0;
		for(i=1;i<=L;i++)  revcnt[i] = revcnt[i-1] + cnt[i-1];
		
		for(i=0;i<power_of_2(L/2);i++){
			idx_t tmp = bitcount(i);
			lutrev[revcnt[tmp]]=i;
			revcnt[tmp]++;
		}
		
		                  revcnt[0] = 0;
		for(i=1;i<=L;i++)  revcnt[i] = revcnt[i-1] + cnt[i-1];
		
		lutlead[0]=0;
		for(i=0;i<power_of_2(L/2);i++){
			idx_t tmp;
			if(usePERM)  tmp = bitcount(leadperm[i]);
			else         tmp = bitcount(         i );
			tmp = NUp-tmp;
			if ( (tmp>=0) && (tmp<=NUp) )   lutlead[i+1] = lutlead[i]+cnt[tmp];
			else                            lutlead[i+1] = lutlead[i];
		}
		
		//for(i=0;i<L-useOBC;i++) imask[i]= ishftc( 1|2 , i, L);  //00000011 00000110, 00001100 .. 11000000, (1000001)
#ifdef DEBUG
        Ns = *nnz;
		printf("N  %ld\n", Ns);
		printf("kl %ld\n", lutlead[power_of_2(L/2)]);
		
		printf("lutlead\n");
		for(i=power_of_2(L/2)-4;i<=power_of_2(L/2);i++)  printf("%ld  ", lutlead[i]);
		
		printf("\n\nluttrail\n");
		for(i=power_of_2(L/2)-4;i< power_of_2(L/2);i++)  printf("%ld  ", luttrail[i]);
		
		printf("\n\nlutrev\n");
		for(i=power_of_2(L/2)-4;i<=power_of_2(L/2);i++)  printf("%ld  ", lutrev[i]);
		
		printf("\n\ncnt\n");
		for(i=0;i<=L;i++)  printf("%ld  ", cnt[i]);
		
		printf("\n\nrevcnt\n");
		for(i=0;i<L;i++)  printf("%ld  ", revcnt[i]);
		
		//printf("\n\nimask\n");
		//for(i=0;i<L;i++)  printf("%d  ", imask[i]);
		
		printf("\n");
#endif
		return 0;
	}
	
	else if( row == -3 ){
		free(lutlead);
		free(luttrail);
		free(lutrev);
		free(cnt);
		free(revcnt);
		if(usePERM){
			free(leadperm);
			free(invleadperm);
		}
		//free(imask);
		return 0;
	}
	else if ( row == -4 ) {
		usePERM = 0;
		return 0;
	}
	else if ( row == -5 ) {
		disorder_strength = *(double*)(vals);
		disorder_seed = *(int*)(nnz);
		return 0;
	}

	printf("SpinChainSZ(): error in row %"PRIDX"\n",row);
	*nnz = -1;
	return 1;              //  error
}


int SpinChainSZPreorder( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg ){
    UNUSED(arg);
		printf("WARNING: SpinChainSZPreorder() is outdated -- please use SpinChainSZ()\n");
        return 1;
#if 0
	static ghost_gidx L   = 0;
	static ghost_gidx NUp = 0;
	static double Jz  = 1.;
	static double Jxy = 1.;
	static ghost_gidx useOBC = 0;
	useOBC &= 1;
	
	
	ghost_gidx Ns = Binomial( L, NUp);
	static idx_t * lutlead;
	static idx_t * luttrail;
	static idx_t * cnt;
	static idx_t * lutrev;
	static idx_t * revcnt;
	//static idx_t * imask;
	static idx_t * leadperm;
	static idx_t * invleadperm;
	
	// L+3 should suffice instead of 2L+1
	ghost_gidx max_row_nnz = 2*L+1;

	if ((row >-1) && (row <Ns)){
		
		double * dvals = vals;
		*nnz = 0;
		
		// binary search
		ghost_gidx min_il = 0;
		ghost_gidx max_il = power_of_2(L/2);
		ghost_gidx il = 0;
		ghost_gidx i;
		for(i = 0; i < L+1; i++)
		{
			il = (min_il + max_il)/2;
			if( row >= lutlead[il+1] )
				min_il = il+1;
			else if( max_il != min_il )
				max_il = il;
			else
				break;
		}

		// bad linear search, makes matrix building VERY expensive!
		//  ghost_gidx il = 0;
		//  while ( row >= lutlead[il+1] ) il++;
		
		
		idx_t bp;
		//bp = (il << (L/2)) + lutrev[ row - lutlead[il] + revcnt[NUp-bitcount(il)]]; 
		/* permute here (2) */
		bp = bitshift_l(leadperm[il] , (L/2)) + lutrev[ row - lutlead[il] + revcnt[NUp-bitcount(leadperm[il])]]; 
		if ( bitcount(bp) != NUp) {  printf("error 1\n"); exit(0);} 
		
		
		ghost_gidx hp = 0;
		
		idx_t l;
		
		 if( Jz != 0. ){
			for(l=0;l<L-useOBC;l++){
				//if (bitcount( bp &  ishftc( 1|2 , l , L)) == 1)   hp -= 1;
				if (CHECK_DIFF_NN (bp , ishftc( 1|2 , l , L)) )    hp -= 1;
				else                                              hp += 1;
				
				//int32_t tmp =  ishftc( (bp & imask[l]) , -l , L );   // ( xxXXxxxx & 00110000 ) ->  000000XX
				//switch(tmp){
				//	case 0: hp += 1; break;  // ..00
				//	case 1: hp -= 1; break;  // ..01
				//	case 2: hp -= 1; break;  // ..10
				//	case 3: hp += 1; break;  // ..11
				//	default: printf("error 2\n"); exit(0);
				//}
			}
		 }
		
		if( hp != 0 ){
			cols[ *nnz] = row;
			dvals[*nnz] = hp*Jz;
			*nnz += 1;
		}
		
		/* if( Jz != 0. ){ */
		if (Jxy != 0. ){
			for(l=0;l<L-useOBC;l++){
				
				//int32_t tmp = ishftc( (bp & imask[l]), -l , L);
				//if( (tmp == 1) || (tmp == 2) ){
				//if( bitcount( bp &  ishftc( 1|2 , l , L) ) == 1 ){
				if (CHECK_DIFF_NN (bp , ishftc( 1|2 , l , L))) {
					
					//int32_t bp2 =  bp ^ imask[l];
					idx_t bp2 =  bitxor(bp , ishftc( 1|2 , l , L));
					
					//ghost_gidx lbp = bp2 >> (L/2);
					idx_t lbp = bitshift_r( bp2,  L/2 );
					idx_t tbp = bitand(bp2 , (power_of_2(L/2)-1));
					
					/* permute here (3) */
					cols[ *nnz] = lutlead[invleadperm[lbp]] + luttrail[tbp] -1;
					
					dvals[*nnz] = 2.*Jxy;
					*nnz += 1;
				}
			}
		 }
		
		cols_error_check( Ns , row, *nnz, cols );
		cols_qsort( cols, vals , sizeof(double) ,  *nnz );
		return 0;
		
	}else if( row == -1 ){
		
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
		info->nrows     = Ns;
		info->ncols     = Ns;
		info->row_nnz   = max_row_nnz;

		info->hermit    =  1;
		info->eig_info  =  0;
		//info->eig_down  = -8.;
		//info->eig_top   =  8.;

		return 0;
		}

	// =================================================================
	else if ( row == -2 ) {
		
		
		L      = cols[0];
		NUp    = cols[1];
		useOBC = cols[2];
		if(L&1) { printf("abort -> need even L!\n"); exit(0); }
		lutlead  = malloc( sizeof(idx_t)* (power_of_2(L/2)+1));
		luttrail = malloc( sizeof(idx_t)*  power_of_2(L/2)   );
		lutrev   = malloc( sizeof(idx_t)* (power_of_2(L/2)+1));
		   cnt   = malloc( sizeof(idx_t)* (L+1) );
		revcnt   = malloc( sizeof(idx_t)* (L+1) );
		//imask    = malloc( sizeof(ghost_gidx)*  L    );
		leadperm = malloc( sizeof(idx_t)* power_of_2(L/2) );
		invleadperm = malloc( sizeof(idx_t)* power_of_2(L/2) );
        
        if (!lutlead || !luttrail || !lutrev || !cnt || !revcnt || !leadperm || !invleadperm) {
            printf("abort -> malloc failed!\n");
            exit(0);
        }
		
		*nnz = Binomial( L, NUp);
		
		idx_t i;
		
		ghost_gidx ii;
		
		
		int j;
	
		
		SpinChainSZLeadperm(L/2,leadperm);
			
		/* inverse permutation */
		for(i=0;i<power_of_2(L/2);i++) 
		{
			invleadperm[leadperm[i]]=i;
		}
		
		
		for(i=0;i<=L;i++)    cnt[i] = 0;
		for(i=0;i<=L;i++) revcnt[i] = 0;
		
		
		for(i=0;i<power_of_2(L/2);i++){
			idx_t tmp = bitcount(i);
			cnt[tmp]++;
			luttrail[i] = cnt[tmp];
		}
		
		                  revcnt[0] = 0;
		for(i=1;i<=L;i++)  revcnt[i] = revcnt[i-1] + cnt[i-1];
		
		for(i=0;i<power_of_2(L/2);i++){
			idx_t tmp = bitcount(i);
			lutrev[revcnt[tmp]]=i;
			revcnt[tmp]++;
		}
		
		                  revcnt[0] = 0;
		for(i=1;i<=L;i++)  revcnt[i] = revcnt[i-1] + cnt[i-1];
		
		lutlead[0]=0;
		for(i=0;i<power_of_2(L/2);i++){
			/* permute here (1) */
			idx_t tmp = bitcount(leadperm[i]);
			tmp = NUp-tmp;
			if ( (tmp>=0) && (tmp<=NUp) )   lutlead[i+1] = lutlead[i]+cnt[tmp];
			else                            lutlead[i+1] = lutlead[i];
		}
		
	//	printf("Generated lookup tables");
		
		//for(i=0;i<L-useOBC;i++) imask[i]= ishftc( 1|2 , i, L);  //00000011 00000110, 00001100 .. 11000000, (1000001)
#ifdef DEBUG
        Ns = *nnz;
		printf("N  %ld\n", Ns);
		printf("kl %ld\n", lutlead[power_of_2(L/2)]);
		
		printf("lutlead\n");
		for(i=power_of_2(L/2)-4;i<=power_of_2(L/2);i++)  printf("%ld  ", lutlead[i]);
		
		printf("\n\nluttrail\n");
		for(i=power_of_2(L/2)-4;i< power_of_2(L/2);i++)  printf("%ld  ", luttrail[i]);
		
		printf("\n\nlutrev\n");
		for(i=power_of_2(L/2)-4;i<=power_of_2(L/2);i++)  printf("%ld  ", lutrev[i]);
		
		printf("\n\ncnt\n");
		for(i=0;i<=L;i++)  printf("%ld  ", cnt[i]);
		
		printf("\n\nrevcnt\n");
		for(i=0;i<L;i++)  printf("%ld  ", revcnt[i]);
		
		//printf("\n\nimask\n");
		//for(i=0;i<L;i++)  printf("%d  ", imask[i]);
		
		printf("\n");
#endif
		return 0;
	}
	
	else if( row == -3 ){
		free(lutlead);
		free(luttrail);
		free(lutrev);
		free(cnt);
		free(revcnt);
		free(leadperm);
		free(invleadperm);
		//free(imask);
		return 0;
	}

	printf("SpinChainSZ(): error in row %"PRIDX"\n",row);
	*nnz = -1;
	return 1;              //  error
#endif
}





int crsSpinChain( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg ){
    UNUSED(arg);

	static ghost_lidx L   = 1;
	static double Jx = 1.;
	static double Jy = 1.;
	static double Jz = 1.;
	static double Bx = 1.;
	static double Bz = 1.;
	static int32_t useOBC = 0;
	useOBC &= 1;
	
	ghost_gidx  Ns = power_of_2( L );
	
	
	ghost_lidx max_row_nnz = 2*L+1;

	if ((row >-1) && (row <Ns)){
		
		double * dvals = vals;
		*nnz = 0;
		ghost_gidx il = row;
		
		
		ghost_gidx hp_Bz = 0;
		ghost_gidx hp_Jz = 0;
		
		int32_t l;
		 if( Bz != 0. ) for(l=0;l<L;l++)if ( (1 << l) &  il ) hp_Bz += 1;
		                                 else                  hp_Bz -= 1;
		
		if( Jz != 0. ){
			for(l=0;l<L-useOBC;l++){
				//if (bitcount( il &  ishftc( 1|2 , l , L)) == 1)   hp_Jz -= 1;
				if ( CHECK_DIFF_NN (il , ishftc( 1|2 , l , L)) ) hp_Jz -= 1;
				else                                              hp_Jz += 1;
			}
		 }
		
		if ( hp_Jz*Jz + hp_Bz*Bz != 0. ){
			cols[ *nnz] = row;
			dvals[*nnz] = hp_Jz*Jz + hp_Bz*Bz;
			*nnz += 1;
		}
		
		if( Bx != 0. ) for(l=0;l<L;l++){
			cols[ *nnz] = il ^ (1 << l);
			dvals[*nnz] = Bx;
			*nnz += 1;
		}
		
		
		if( (Jx != 0.) || (Jy != 0.) ){
			double hp;
			for(l=0;l<L-useOBC;l++){
				//if (bitcount( il &  ishftc( 1|2 , l , L)) == 1)   hp = Jx+Jy;
				if ( CHECK_DIFF_NN (il , ishftc( 1|2 , l , L)) ) hp = Jx+Jy;
				else                                              hp = Jx-Jy;
				
				if ( hp != 0. ){
					cols[ *nnz] = il ^ ishftc( 1|2 , l , L);
					dvals[*nnz] = hp;
					*nnz += 1;
				}
			}
		}
		
		cols_qsort( cols, vals , sizeof(double) ,  *nnz );
		return 0;
		
	}
	else if( row == -1 ){
		
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
		info->nrows     = Ns;
		info->ncols     = Ns;
		info->row_nnz   = max_row_nnz;

		info->hermit    =  1;
		info->eig_info  =  0;
		//info->eig_down  = -8.;
		//info->eig_top   =  8.;
		

		return 0;
		}

	// =================================================================
	else if ( row == -2 ) {
		L      = nnz[0];
		useOBC = nnz[2];
		
		*cols = power_of_2( L );
		

		return 0;
	}
	
	printf("SpinChainSZ(): error in row %"PRIDX"\n",row);
	*nnz = -1;
	return 1;              //  error
}

