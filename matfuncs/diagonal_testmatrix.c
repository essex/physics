#include "matfuncs.h"
#include <complex.h>

static ghost_gidx N = 0;

int diagonal_testmatrix(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *col, void *vval, void *arg)
{
    UNUSED(arg);

    *currowlen = 1;
    ((double *)vval)[0] = 1.;
    col[0] = currow;
    
    
    return 0;
}

int zdiagonal_testmatrix(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *col, void *vval, void *arg)
{
    UNUSED(arg);

    *currowlen = 1;
    ((complex double *)vval)[0] = 1. + 0.*I;
    col[0] = currow;
    
    
    return 0;
}
