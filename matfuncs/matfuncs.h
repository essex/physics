#ifndef MATFUNCS_H
#define MATFUNCS_H

#include <ghost.h>

#ifdef __cplusplus
#include <cstdio>
#include <cstdlib>
#else
#include <stdio.h>
#include <stdlib.h>
#endif

#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288
#endif

#ifndef MATFUNCS_INFO_T
#define MATFUNCS_INFO_T

#define UNUSED(x) (void)(x)

typedef struct {

	int32_t version;
	int32_t base;
	int32_t symmetry;
	int32_t datatype;
	ghost_gidx nrows;
	ghost_gidx ncols;
	ghost_lidx row_nnz;

	int32_t      hermit;
	int32_t      eig_info;
	double       eig_down;
	double       eig_top;

	} matfuncs_info_t;
#endif


typedef struct {
	double x;
	double y;
	double z;
	} double_3d;

typedef struct {
	int mpi_size_x;
	int mpi_size_y;
	int mpi_size_z;
	ghost_gidx g_x;
	ghost_gidx g_y;
	ghost_gidx g_z;
	ghost_gidx global_dim;
	ghost_gidx l_x;
	ghost_gidx l_y;
	ghost_gidx l_z;
	ghost_gidx local_dim;
	ghost_gidx OBC_x;
	ghost_gidx OBC_y;
	ghost_gidx OBC_z;
	
	double * V;
	int32_t use_V;
	
	double_3d * BZ_fild;
	int32_t use_BZ_fild;
	
	double_3d * peierls_fild;
	int32_t use_peierls_fild;

	double m;
	double gp;
	double gm;
	double D1;
	double D2;
	double_3d BZ;
	double Phi_y;
	double Phi_z;

	int32_t use_D;
	int32_t use_Z;

	double Dot_R;
	double Dot_V;
	int32_t use_Dot;
	
	ghost_gidx k_num;
	ghost_gidx k_block_size;
	ghost_gidx k_index_offset;
	double * kx;
	double * ky;
	double * kz;
	
	ghost_gidx LDOS_num;
	ghost_gidx LDOS_block_size;
	ghost_gidx LDOS_index_offset;
	ghost_gidx * LDOS_nx;
	ghost_gidx * LDOS_ny;
	ghost_gidx * LDOS_nz;
	

	} matfuncs_topi_sheet_t;


#ifdef __cplusplus
extern "C" {
#endif


//int init_Graphene_Sheet(int mpi_size_x, int mpi_size_y);
//ghost_idx_t getDIM_Graphene_Sheet();
//int crsGraphene_Sheet( ghost_idx_t row, ghost_idx_t *nnz, ghost_idx_t *cols, void *vals);

int SpinChainSZ(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int SpinChainSZPreorder(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int crsSpinChain(  ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int crsGraphene(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);

int crsTopi_V_init_anderson_disorder( int seed, double gamma);
ghost_datatype crsTopi_get_DT();
double crsTopi_get_eig_range_low();
double crsTopi_get_eig_range_high();
ghost_gidx crsTopi_get_dim();
ghost_lidx crsTopi_get_max_nnz();
int crsTopi_set_size( ghost_gidx l, ghost_gidx w, ghost_gidx h );
int crsTopi(       ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);


matfuncs_topi_sheet_t * init_Topi_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z );
int set_local_size_Topi_Sheet(ghost_gidx l_x, ghost_gidx l_y, ghost_gidx l_z);
int set_parm_delta_Topi_Sheet( double D1, double D2);
int set_parm_m_Topi_Sheet( double m);
int set_parm_dot_Topi_Sheet( double R, double V);
ghost_gidx getDIM_Topi_Sheet();
ghost_gidx getH_Topi_Sheet();
int crsTopi_sheet( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int init_Topi_Sheet_LDOS_states( ghost_gidx Nx, ghost_gidx Ny, ghost_gidx Nx0, ghost_gidx Ny0, ghost_gidx dx, ghost_gidx dy, ghost_gidx blocksize  );
int init_Topi_Sheet_wave_states( ghost_gidx k_num, double kx_min, double kx_max, double ky_min, double ky_max, double kz_min, double kz_max, ghost_gidx block );
int vecTopi_sheet_LDOS_states_offset_increment( ghost_gidx N );
int vecTopi_sheet_wave_offset_increment( ghost_gidx N );
int vecTopi_sheet_wave_states( ghost_gidx row, ghost_lidx col,  void * val, void *arg);
int vecTopi_sheet_surface_state( ghost_gidx row, ghost_lidx col,  void * val, void *arg);
int vecTopi_sheet_LDOS_states( ghost_gidx row, ghost_lidx col,  void * val, void *arg);
void print_Topi_sheet_LDOS_sites( );
ghost_gidx crs_Topi_Sheet_get_max_nnz();
double * init_Topi_Sheet_anderson_disorder(  int seed, double gamma);
int init_Topi_Sheet_anderson_disorder_BZ(  int seed, double gamma, double Bx_offset, double By_offset, double Bz_offset, double gp, double gm);
int init_Topi_Sheet_anderson_disorder_peierls(  int seed, double gamma);
int vecTopi_sheet_Observable_z(void *Observable, ghost_gidx row, ghost_gidx n_cols, void *vec_vals);
//int vecTopi_sheet_Observable_z_reduce(void *Observable);
ghost_gidx grid3D_b4_check_z_layer(ghost_gidx layer  );
ghost_datatype crs_Topi_Sheet_get_DT();
void vecTopi_export_grid_indexing( char * file_name );
int vecTopi_sheet_Observable_xy(double * xy_plane, ghost_densemat * vec, double * z_view , int rank);
int vecTopi_sheet_Observable_z_dense(double *z_dense, ghost_densemat * vec, int rank);

// 3D test case for solvers: Anderson model on L x L x L domain, periodic BC
int anderson(      ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);


int hpccg(         ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int hpccg_onthefly(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);

int diagonal_testmatrix(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *col, void *vval, void *arg);
int zdiagonal_testmatrix(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *col, void *vval, void *arg);

matfuncs_topi_sheet_t * init_Anderson_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z );
int set_local_size_Anderson_Sheet(ghost_gidx l_x, ghost_gidx l_y, ghost_gidx l_z);
ghost_gidx getDIM_Anderson_Sheet();
double * init_Anderson_Sheet_anderson_disorder(  int seed, double gamma);
ghost_datatype crs_Anderson_Sheet_get_DT();
ghost_gidx crs_Anderson_Sheet_get_max_nnz();
int crsAnderson_sheet( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);


#ifdef __cplusplus
}
#endif

#endif
