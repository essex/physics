#include "matfuncs.h"


void crsChain( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg ){
    UNUSED(arg);

	static ghost_gidx N = 10;
	static ghost_lidx max_row_nnz = 2;

	if ((row >-1) && (row <N)){

		double * dvals = vals;
		ghost_gidx i = 0;

		if(row==0)         { cols[i] =   1;   dvals[i] = -1.; i++; }
		else if (row==N-1) { cols[i] = N-2;   dvals[i] = -1.; i++; }
		else               { cols[i] = row-1; dvals[i] = -1.; i++;
		                     cols[i] = row+1; dvals[i] = -1.; i++; }

		*nnz = i;
		return;
	}else if(row == -1){
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
		info->nrows     = N;
		info->ncols     = N;
		info->row_nnz   = max_row_nnz;

		info->hermit    =  1;
		info->eig_info  =  1;
		info->eig_down  = -2.;
		info->eig_top   =  2.;

		return;
		}

	// =================================================================
	else if ( row < -1) {        // config  my_crsChain()
		N = *nnz;
		return;
	}

	printf("crsChain(): error in row %"PRIDX"\n",row);
	*nnz = -1;
	return;              //  error
}
