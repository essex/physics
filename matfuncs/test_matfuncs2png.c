
#include "matfuncs_png.h"


int read_args(char * option , int argc, char * argv[]){
//============================ Reading commnad line arguments with flags ===============//
	int i=0;
	for ( i = 1; i < argc; i++)  
		if ( !strcmp(argv[i], option ) )
			return i;
	return 0;
}


int main( int argc, char* argv[] ){
	
	int i;
	
	ghost_lidx graphene_config[2] = {8,8};
	i = read_args( "-graphene_size" , argc, argv );
	if( i ) {
		graphene_config[0] = atoi(  argv[i+1] );
		graphene_config[1] = atoi(  argv[i+2] );}

	ghost_lidx spin_config[3];
	spin_config[0]= 4;  // L
	spin_config[1]= 2;
	spin_config[2]= 0;  // PBC/OBC
	
	i = read_args( "-spin_L" , argc, argv );
	if( i )  spin_config[0] = atoi(  argv[i+1] );
	spin_config[1] = spin_config[0] >> 1;
	
	i = read_args( "-spin_NUp" , argc, argv );
	if( i )  spin_config[1] = atoi(  argv[i+1] );
	
	i = read_args( "-spin_OBC" , argc, argv );
	if( i )  spin_config[2] = 1;
	
	i = read_args( "-spin_PBC" , argc, argv );
	if( i )  spin_config[2] = 0;
	
	
	int t_s[3] = {8,8,8};
	int t_g[3] = {1,1,1};
	
	i = read_args( "-topi_size" , argc, argv );
	if( i ) {
		t_g[0] = atoi(  argv[i+1] );
		t_g[1] = atoi(  argv[i+2] );
		t_g[2] = atoi(  argv[i+3] );
		t_s[0] = atoi(  argv[i+4] );
		t_s[1] = atoi(  argv[i+5] );
		t_s[2] = atoi(  argv[i+6] );
		printf("set topi size %d-%d-%d %d-%d-%d\n", t_g[0], 
                                                    t_g[1], 
                                                    t_g[2], 
                                                    t_s[0], 
                                                    t_s[1], 
                                                    t_s[2]  );
	}


	int test_case = 1;
	i = read_args( "-test_case" , argc, argv );
	if( i ) test_case = atoi(  argv[i+1] );

	ghost_sparsemat_rowfunc matfunc;
	ghost_gidx DIM=0;
	
	switch (test_case){
		case 0:    
			crsGraphene( -2, graphene_config, NULL, NULL, NULL);
			matfunc = crsGraphene;
		break;
		case 1:
			crsSpinChain( -2, spin_config, &DIM, NULL, NULL);
			matfunc = crsSpinChain;
		break;
		case 2:
			SpinChainSZ( -2, spin_config, &DIM,  NULL, NULL );
			matfunc = SpinChainSZ;
		break;
		//case 3:
		//	init_Graphene_Sheet(1,1);
		//	matfunc = crsGraphene_Sheet;
		//break;
		case 3:
			

			
			init_Topi_Sheet(           t_g[0], t_g[1], t_g[2]);
			set_local_size_Topi_Sheet( t_s[0], t_s[1], t_s[2] );
			
			//vecTopi_export_grid_indexing( "topi_indexing.dat" );
			DIM = getDIM_Topi_Sheet();
			matfunc = crsTopi_sheet;
			
			//crsSpinChain( -2, spin_config, &DIM,  NULL, NULL );
			//matfunc = crsSpinChain;
		break;

	}
	
	
	
	int  max_size[2] = {16,16};
	i = read_args( "-max_size" , argc, argv );
	if( i ) {
		max_size[0] = atoi(  argv[i+1] );
		max_size[1] = atoi(  argv[i+2] );
		printf("set png size %dx%d\n", max_size[0],max_size[1]  );
		
		}
	
	char   png_filename_default[] = "mat.png";
	char * png_filename;
	i = read_args( "-png_filename" , argc, argv );
	if( i ) png_filename = argv[i+1] ;
	else    png_filename = png_filename_default;
	
	matfunc_2_png ( png_filename , matfunc  ,  max_size );
	
	return 0;
}


