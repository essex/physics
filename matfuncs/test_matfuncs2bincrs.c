
#include "matfuncs.h"


int read_args(char * option , int argc, char * argv[]){
//============================ Reading commnad line arguments with flags ===============//
	int i=0;
	for ( i = 1; i < argc; i++)  
		if ( !strcmp(argv[i], option ) )
			return i;
	return 0;
}

void matfunc_2_bincrs ( char * crs_filename , ghost_sparsemat_rowfunc matfunc ){
	
	matfuncs_info_t info;
	matfunc( -1, NULL, NULL, &info,NULL);
	
	size_t DZ = 0;
	void * dummy;
	switch ( info.datatype ){
		case GHOST_DT_FLOAT |GHOST_DT_REAL:    DZ = 4; break;
		case GHOST_DT_FLOAT |GHOST_DT_COMPLEX: DZ = 8; break;
		case GHOST_DT_DOUBLE|GHOST_DT_REAL:    DZ = 8; break;
		case GHOST_DT_DOUBLE|GHOST_DT_COMPLEX: DZ =16; break;
	}
	
	dummy = malloc(DZ*info.row_nnz);
	
	ghost_gidx * cols = malloc(sizeof(ghost_gidx)*info.row_nnz);
	ghost_lidx nnz;
	int64_t * rowptr = malloc((info.nrows+1)*sizeof(int64_t));
	rowptr[0]=0L;
	
	ghost_gidx row;
	for (row = 0; row < info.nrows; row++){
		matfunc(row, &nnz, cols, dummy, NULL );
		rowptr[row+1] = rowptr[row] + nnz;
		}
	
	int32_t tmp32;
	int64_t tmp64;
	FILE * pFile = fopen( crs_filename ,"wb");
	tmp32 = 0;                  fwrite ( &tmp32,            1, sizeof(int32_t), pFile );
	tmp32 = 1;                  fwrite ( &tmp32,            1, sizeof(int32_t), pFile );
	tmp32 = 0;                  fwrite ( &tmp32,            1, sizeof(int32_t), pFile );
	tmp32 = info.datatype;      fwrite ( &tmp32,            1, sizeof(int32_t), pFile );
	tmp64 = info.nrows;         fwrite ( &tmp64,            1, sizeof(int64_t), pFile );
	tmp64 = info.ncols;         fwrite ( &tmp64,            1, sizeof(int64_t), pFile );
	tmp64 = rowptr[info.nrows]; fwrite ( &tmp64,            1, sizeof(int64_t), pFile );
	                            fwrite ( rowptr, info.nrows+1, sizeof(int64_t), pFile );
	int i;
	for (row = 0; row < info.nrows; row++){
		matfunc(row, &nnz, cols, dummy, NULL );
		for (i = 0; i < nnz; i++)  rowptr[i] = cols[i];
		fwrite ( rowptr, nnz, sizeof(int64_t), pFile );
		}
	
	for (row = 0; row < info.nrows; row++){
		matfunc(row, &nnz, cols, dummy, NULL );
		fwrite ( dummy, nnz, DZ, pFile );
		}
	
	fclose(pFile);
	
	free(dummy);
	free(rowptr);
}

int main( int argc, char* argv[] ){
	
	int i;
	


	ghost_lidx spin_config[3];
	spin_config[0]= 4;  // L
	spin_config[1]= 2;
	spin_config[2]= 0;  // PBC/OBC
	
	i = read_args( "-spin_L" , argc, argv );
	if( i )  spin_config[0] = atoi(  argv[i+1] );
	spin_config[1] = spin_config[0] >> 1;
	
	i = read_args( "-spin_NUp" , argc, argv );
	if( i )  spin_config[1] = atoi(  argv[i+1] );
	
	i = read_args( "-spin_OBC" , argc, argv );
	if( i )  spin_config[2] = 1;
	
	i = read_args( "-spin_PBC" , argc, argv );
	if( i )  spin_config[2] = 0;

	ghost_lidx graphene_config[2];

	int test_case = 0;
	i = read_args( "-test_case" , argc, argv );
	if( i ) test_case = atoi(  argv[i+1] );

	ghost_sparsemat_rowfunc matfunc;
	ghost_gidx DIM=0;
	double gamma;
	
	switch (test_case){
		case 0:
			i = read_args( "-graphene_size" , argc, argv );
			if( i ) {
				graphene_config[0] = atoi(  argv[i+1] );
				graphene_config[1] = atoi(  argv[i+2] );}
			else{
				graphene_config[0] =  256;
				graphene_config[1] = 2048;}
			
			crsGraphene( -2, graphene_config, NULL, NULL, NULL);
			
			i = read_args( "-graphene_OBC" , argc, argv );
			if( i ) {
				graphene_config[0] = 0;
				graphene_config[1] = 0;
				crsGraphene( -4, graphene_config, NULL, NULL, NULL);}
			
			i = read_args( "-graphene_anderson" , argc, argv );
			if( i ) {
				gamma =  atof(  argv[i+1] );
				crsGraphene( -5, NULL, NULL, &gamma, NULL);
			}
			
			matfunc = crsGraphene;
		break;
		case 1:
			crsSpinChain( -2, spin_config, &DIM,  NULL, NULL );
			matfunc = crsSpinChain;
		break;
		case 2:
			SpinChainSZ( -2, spin_config, &DIM,  NULL, NULL );
			matfunc = SpinChainSZ;
		break;
		case 3:
			init_Topi_Sheet( 2, 2, 2);
			set_local_size_Topi_Sheet(4,4,4);
			
			vecTopi_export_grid_indexing( "topi_indexing.dat" );
			DIM = getDIM_Topi_Sheet();
			matfunc = crsTopi_sheet;
			
			crsSpinChain( -2, spin_config, &DIM,  NULL, NULL );
			matfunc = crsSpinChain;
			
		break;

	}
	
	
	char   crs_filename_default[] = "mat.crs";
	char * crs_filename;
	i = read_args( "-crs_filename" , argc, argv );
	if( i ) crs_filename = argv[i+1] ;
	else    crs_filename = crs_filename_default;
	
	matfunc_2_bincrs ( crs_filename , matfunc );
	
	return 0;
}


