#include "matfuncs.h"
#include <math.h>
#include <complex.h>

/*
int crsTopi( ghost_idx_t row, ghost_idx_t *nnz, ghost_idx_t *cols, void *vals){

	static ghost_idx_t L = 4;
	static ghost_idx_t W = 4;
	static ghost_idx_t H = 4;
	       ghost_idx_t P = 4;
	       ghost_idx_t N = L*W*H*P;

	static double m     = 1.;
	static double gp    = 0.;
	static double gm    = 0.;
	static double D1    = 0.;
	static double D2    = 0.;
	static double Bx    = 0.;
	static double By    = 0.;
	static double Bz    = 0.;
	static double Phi_y = 0.;
	static double Phi_z = 0.;

	static int32_t use_D   = 0;
	static int32_t use_Z   = 0;

	ghost_idx_t max_row_nnz = P*7;

	if ((row >-1 ) && (row <N)){       //  defined output -- write entries of #row in *cols and *vals
	                                   //                    return number of entries
		double complex * zvals = vals;
		ghost_idx_t i = 0 ;

		ghost_idx_t p =  row%P;
		ghost_idx_t h =  ((row-p)/P)%H;
		ghost_idx_t w =  ((row-p-P*h)/(P*H))%W;
		ghost_idx_t l =  ((row-p-P*h-P*H*w)/(P*H*W))%L;

		//ghost_idx_t l =  row                  /(W*H*P);
		//ghost_idx_t w = (row-  l*W   *H   *P )/(  H*P);
		//ghost_idx_t h = (row- (l*W+w)*H   *P )/(    P);
		//ghost_idx_t p =  row-((l*W+w)*H+h)*P;


		double complex  phase_x = cexp(2.*M_PI*I*(Phi_y*h + Phi_z*w));
		double complex cphase_x = conj(phase_x);
		ghost_idx_t   iHx[4][2]   = {{ 0 ,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHx_l[4][2] = {{-0.5,        0.5},
		                              {    0.5,  0.5},
		                              {   -0.5, -0.5},
		                              {-0.5,        0.5}};
		double complex vHx_u[4][2] = {{-0.5,       -0.5},
		                              {    0.5, -0.5},
		                              {    0.5, -0.5},
		                              { 0.5,        0.5}};

		ghost_idx_t   iHy[4][2]   = {{ 0L,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHy_l[4][2] = {{  -0.5,            -I*0.5},
		                              {         0.5,-I*0.5},
		                              {      -I*0.5,  -0.5},
		                              {-I*0.5,              0.5}};
		double complex vHy_u[4][2] = {{  -0.5,             I*0.5},
		                              {         0.5, I*0.5},
		                              {       I*0.5,  -0.5},
		                              { I*0.5,              0.5}};

		ghost_idx_t   iHz[4][2]   = {{ 0 , 1        },
		                              { 0 , 1        },
		                              {        2 , 3 },
		                              {        2 , 3 }};
		double complex vHz_l[4][2] = {{  -0.5,  0.5           },
		                              {  -0.5,  0.5           },
		                              {              -0.5, 0.5},
		                              {              -0.5, 0.5}};
		double complex vHz_u[4][2] = {{  -0.5, -0.5           },
		                              {   0.5,  0.5           },
		                              {             -0.5, -0.5},
		                              {              0.5,  0.5}};

		if(l>0   ) {  zvals[i] = vHx_l[p][0]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_l[p][1]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		if(w>0   ) {  zvals[i] = vHy_l[p][0];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_l[p][1];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][1];  i++;  }
		if(h>0   ) {  zvals[i] = vHz_l[p][0];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_l[p][1];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][1];  i++;  }
	switch(p) {
		case 0 :             zvals[i] =  m; if(use_Z) zvals[i] +=  Bz*(gp+gm);     cols[i] = row;     i++;
		         if(use_D) { zvals[i] =  D1-I*D2;                                  cols[i] = row+1 ;  i++;}
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp+gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 1 : if(use_D) { zvals[i] = -D1+I*D2;                                  cols[i] = row-1 ;  i++;}
		                     zvals[i] = -m; if(use_Z) zvals[i] +=  Bz*(gp-gm);     cols[i] = row;     i++;
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp-gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 2 : if(use_Z){ zvals[i] = (Bx+I*By)*(gp+gm);                          cols[i] = row-2 ;  i++;}
		                     zvals[i] =  m;  if(use_Z) zvals[i] -= Bz*(gp+gm);     cols[i] = row;     i++;
		         if(use_D) { zvals[i] = +D1-I*D2;                                  cols[i] = row+1 ;  i++;}
		     break;
		case 3 : if(use_Z) { zvals[i] = (Bx+I*By)*(gp-gm);                          cols[i] = row-2 ;  i++;}
		         if(use_D) { zvals[i] = -D1+I*D2;                                   cols[i] = row-1 ;  i++;}
		                     zvals[i] = -m; if(use_Z) zvals[i] -=  Bz*(gp-gm);      cols[i] = row;     i++;
		     break;
		}
		if(h<H-1 ) {  zvals[i] = vHz_u[p][0];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_u[p][1];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][1];  i++;  }
		if(w<W-1 ) {  zvals[i] = vHy_u[p][0];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_u[p][1];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][1];  i++;  }
		if(l<L-1 ) {  zvals[i] = vHx_u[p][0]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_u[p][1]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][1];  i++;  }

	*nnz = i;
	return 0;
	}
	// -----------------------------------------------------------------
	else if (row == -1  ) {            //  defined output -- write header in *vals
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_COMPLEX;
		info->nrows     = N;
		info->ncols     = N;
		info->row_nnz   = max_row_nnz;

		info->hermit    =  1;
		info->eig_info  =  0;
		//info->eig_down  = -4.;
		//info->eig_top   =  4.;

		return 0;
	}
	// =================================================================
	else if ( row < -1) {        // config  my_crsChain()
		L = cols[0];
		W = cols[1];
		H = cols[2];

		*nnz = 0;
		return 0;
	}

	*nnz = -1;
	return 1;              //  error
}

*/

// default init

double * Topi_V = NULL;
int32_t Topi_use_V = 0;

ghost_gidx L = 8;
ghost_gidx W = 8;
ghost_gidx H = 8;
#define       P  4


int32_t PBC_L = 1;
int32_t PBC_W = 1;
int32_t PBC_H = 0;

double m     = 2.;
double gp    = 0.;
double gm    = 0.;
double D1    = 0.;
double D2    = 0.;
double Bx    = 0.;
double By    = 0.;
double Bz    = 0.;
double Phi_y = 0.;
double Phi_z = 0.;

int32_t use_D   = 0;
int32_t use_Z   = 0;
int32_t model   = 0;


double Dot_R = 25.;
//double Dot_V = 0.52;
double Dot_V = 0.;



int crsTopi_V_init_anderson_disorder( int seed, double gamma){ //, COMM_t * comm ){
	
	srand(seed);
	ghost_gidx i,N = L*W*H;
	Topi_V = malloc(sizeof(double)*N);
	Topi_use_V = 1;
	
	for(i=0;i<N;i++) Topi_V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
	
	//MPI_Bcast(Topi_V, N, 0, *comm );
	return 0;
	}

ghost_datatype crsTopi_get_DT(){
	return (GHOST_DT_DOUBLE|GHOST_DT_COMPLEX);
}

double crsTopi_get_eig_range_low(){
	return -5.2;
}

double crsTopi_get_eig_range_high(){
	return  5.5;
}

ghost_gidx crsTopi_get_dim(){
	return  L*W*H*P;
}

ghost_lidx crsTopi_get_max_nnz(){
	return  P*7;
}

int crsTopi_set_size( ghost_gidx l, ghost_gidx w, ghost_gidx h ){
	L=l;
	W=w;
	H=h;
	return  0;
}


int crsTopi( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg){
    UNUSED(arg);

	ghost_gidx N = L*W*H*P;

	if ((row >-1 ) && (row <N)){       //  defined output -- write entries of #row in *cols and *vals
	                                   //                    return number of entries
		double complex * zvals = vals;
		ghost_gidx i = 0 ;
/*
		ghost_idx_t p =  row%P;
		ghost_idx_t h =  ((row-p)/P)%H;
		ghost_idx_t w =  ((row-p-P*h)/(P*H))%W;
		ghost_idx_t l =  ((row-p-P*h-P*H*w)/(P*H*W))%L;
*/
		ghost_gidx l =  row                  /(W*H*P);
		ghost_gidx w = (row-  l*W   *H   *P )/(  H*P);
		ghost_gidx h = (row- (l*W+w)*H   *P )/(    P);
		ghost_gidx p =  row-((l*W+w)*H+h)*P;
		
		//if((p >=4) || (p<0)) { printf("error crsTopi\n"); fflush(stdout); exit(0); }
		
		double V = 0.;
		if(Topi_use_V) V += Topi_V[(l*W+w)*H+h];
		double Dot_D = 4*Dot_R;
		double Dot_cx= 2*Dot_R;
		double Dot_cy= 2*Dot_R;
		double Dot_x = fmod( l , Dot_D );
		double Dot_y = fmod( w , Dot_D );
		
		if( (((Dot_cx-Dot_x)*(Dot_cx-Dot_x) + (Dot_cy-Dot_y)*(Dot_cy-Dot_y)) < Dot_R*Dot_R )
		     &&  ( (h==0) ) 
		      )  V += Dot_V;
		

		double complex  phase_x = cexp(2.*M_PI*I*(Phi_y*h + Phi_z*w));
		double complex cphase_x = conj(phase_x);
		
		if( !model ){
		
		ghost_gidx   iHx[P][2]   = {{ 0 ,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHx_l[P][2] = {{-0.5,        0.5},
		                              {    0.5,  0.5},
		                              {   -0.5, -0.5},
		                              {-0.5,        0.5}};
		double complex vHx_u[P][2] = {{-0.5,       -0.5},
		                              {    0.5, -0.5},
		                              {    0.5, -0.5},
		                              { 0.5,        0.5}};

		ghost_gidx   iHy[P][2]   = {{ 0L,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHy_l[P][2] = {{  -0.5,            -I*0.5},
		                              {         0.5,-I*0.5},
		                              {      -I*0.5,  -0.5},
		                              {-I*0.5,              0.5}};
		double complex vHy_u[P][2] = {{  -0.5,             I*0.5},
		                              {         0.5, I*0.5},
		                              {       I*0.5,  -0.5},
		                              { I*0.5,              0.5}};

		ghost_gidx   iHz[P][2]   = {{ 0 , 1        },
		                              { 0 , 1        },
		                              {        2 , 3 },
		                              {        2 , 3 }};
		double complex vHz_l[P][2] = {{  -0.5,  0.5           },
		                              {  -0.5,  0.5           },
		                              {              -0.5, 0.5},
		                              {              -0.5, 0.5}};
		double complex vHz_u[P][2] = {{  -0.5, -0.5           },
		                              {   0.5,  0.5           },
		                              {             -0.5, -0.5},
		                              {              0.5,  0.5}};

		if(l>0   )   {  zvals[i] = vHx_l[p][0]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][0];  i++;
		                zvals[i] = vHx_l[p][1]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		else if(PBC_L){zvals[i] = vHx_l[p][0]*cphase_x;   cols[i] = P*(H*(W*(L-1 ) + w    ) + h    ) + iHx[p][0];  i++;
		                zvals[i] = vHx_l[p][1]*cphase_x;   cols[i] = P*(H*(W*(L-1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		if(w>0   )   {  zvals[i] = vHy_l[p][0];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][0];  i++;
		                zvals[i] = vHy_l[p][1];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][1];  i++;  }
		else if(PBC_W){zvals[i] = vHy_l[p][0];            cols[i] = P*(H*(W* l     + W-1  ) + h    ) + iHy[p][0];  i++;
		                zvals[i] = vHy_l[p][1];            cols[i] = P*(H*(W* l     + W-1  ) + h    ) + iHy[p][1];  i++;  }
		if(h>0   )   {  zvals[i] = vHz_l[p][0];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][0];  i++;
		                zvals[i] = vHz_l[p][1];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][1];  i++;  }
		else if(PBC_H){zvals[i] = vHz_l[p][0];            cols[i] = P*(H*(W* l     + w    ) + H-1  ) + iHz[p][0];  i++;
		                zvals[i] = vHz_l[p][1];            cols[i] = P*(H*(W* l     + w    ) + H-1  ) + iHz[p][1];  i++;  }
		switch(p) {
		case 0 :             zvals[i] = V + m; if(use_Z) zvals[i] +=  Bz*(gp+gm); cols[i] = row;     i++;
		         if(use_D) { zvals[i] =  D1-I*D2;                                  cols[i] = row+1 ;  i++;}
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp+gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 1 : if(use_D) { zvals[i] = -D1+I*D2;                                 cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] +=  Bz*(gp-gm);  cols[i] = row;     i++;
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp-gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 2 : if(use_Z){ zvals[i] = (Bx+I*By)*(gp+gm);                         cols[i] = row-2 ;  i++;}
		                     zvals[i] =  V + m;  if(use_Z) zvals[i] -= Bz*(gp+gm); cols[i] = row;     i++;
		         if(use_D) { zvals[i] = +D1-I*D2;                                  cols[i] = row+1 ;  i++;}
		     break;
		case 3 : if(use_Z) { zvals[i] = (Bx+I*By)*(gp-gm);                        cols[i] = row-2 ;  i++;}
		         if(use_D) { zvals[i] = -D1+I*D2;                                  cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] -=  Bz*(gp-gm);  cols[i] = row;     i++;
		     break;
		}
		if(h<H-1 ) {    zvals[i] = vHz_u[p][0];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][0];  i++;
		                zvals[i] = vHz_u[p][1];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][1];  i++;  }
		else if(PBC_H){zvals[i] = vHz_u[p][0];            cols[i] = P*(H*(W* l     + w    ) +   0  ) + iHz[p][0];  i++;
		                zvals[i] = vHz_u[p][1];            cols[i] = P*(H*(W* l     + w    ) +   0  ) + iHz[p][1];  i++;  }
		if(w<W-1 ) {    zvals[i] = vHy_u[p][0];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][0];  i++;
		                zvals[i] = vHy_u[p][1];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][1];  i++;  }
		else if(PBC_W){zvals[i] = vHy_u[p][0];            cols[i] = P*(H*(W* l     +   0  ) + h    ) + iHy[p][0];  i++;
		                zvals[i] = vHy_u[p][1];            cols[i] = P*(H*(W* l     +   0  ) + h    ) + iHy[p][1];  i++;  }
		if(l<L-1 ) {    zvals[i] = vHx_u[p][0]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][0];  i++;
		                zvals[i] = vHx_u[p][1]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		else if(PBC_L){zvals[i] = vHx_u[p][0]*phase_x;    cols[i] = P*(H*(W*(  0 ) + w    ) + h    ) + iHx[p][0];  i++;
		                zvals[i] = vHx_u[p][1]*phase_x;    cols[i] = P*(H*(W*(  0 ) + w    ) + h    ) + iHx[p][1];  i++;  }
			
			
		}else{
		
		ghost_gidx  iHx[4][2] = {{ 0 ,        3 },
		                                {     1 , 2    },
		                                {     1 , 2    },
		                                { 0 ,        3 }};
		double complex vHx_l[4][2] = {{-0.5,                   I},
		                              {      -0.5,       I       },
		                              {           I, 0.5         },
		                              {     I,              0.5  }};
		double complex vHx_u[4][2] = {{-0.5,              -    I},
		                              {      -0.5,  -    I       },
		                              {      -    I, 0.5         },
		                              {-    I,              0.5  }};

		ghost_gidx   iHy[4][2]   = {{ 0,         3 },
		                                   {     1 , 2    },
		                                   {     1 , 2    },
		                                   { 0 ,        3 }};
		double complex vHy_l[4][2] = {{  -0.5,             1.0 },
		                              {        -0.5,  -1.0      },
		                              {         1.0,   0.5      },
		                              {  -1.0,              0.5 }};
		double complex vHy_u[4][2] = {{  -0.5,              -1.0},
		                              {        -0.5,   1.0       },
		                              {        -1.0,   0.5       },
		                              {   1.0,              0.5  }};

		ghost_gidx   iHz[4][2] = {{ 0,      2    },
		                                 {     1 ,    3 },
		                                 { 0 ,     2    },
		                                 {     1 ,    3 }};
		double complex vHz_l[4][2] = {{  -0.5,         I       },
		                              {        -0.5,       -I   },
		                              {    I       ,   0.5      },
		                              {         -I,         0.5 }};
		double complex vHz_u[4][2] = {{  -0.5,         -I       },
		                              {        -0.5,         I   },
		                              {    -I,         0.5       },
		                              {          I,         0.5  }};

		if(l>0   ) {  zvals[i] = vHx_l[p][0]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_l[p][1]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		if(w>0   ) {  zvals[i] = vHy_l[p][0];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_l[p][1];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][1];  i++;  }
		if(h>0   ) {  zvals[i] = vHz_l[p][0];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_l[p][1];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][1];  i++;  }
		switch(p) {
		case 0 :  
		                     zvals[i] = V + 2.;  cols[i] = row;     i++;
		     break;
		case 1 : 
		                     zvals[i] = V + 2.;  cols[i] = row;     i++;
		     break;
		case 2 : 
		                     zvals[i] = V - 2.;  cols[i] = row;     i++;
		     break;
		case 3 : 
		                     zvals[i] = V - 2.;  cols[i] = row;     i++;
		     break;
		}
		if(h<H-1 ) {  zvals[i] = vHz_u[p][0];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_u[p][1];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][1];  i++;  }
		if(w<W-1 ) {  zvals[i] = vHy_u[p][0];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_u[p][1];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][1];  i++;  }
		if(l<L-1 ) {  zvals[i] = vHx_u[p][0]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_u[p][1]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		
		
		}




	*nnz = i;
	return  0;
	}

	*nnz = -1;
	return  1;              //  error
}

