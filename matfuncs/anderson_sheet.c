#include "matfuncs.h"
#include <math.h>
#include <complex.h>
#include <ghost.h>
#include <ghost/math.h>

typedef struct {
	int mpi_rank_x;
	int mpi_rank_y;
	int mpi_rank_z;
	ghost_gidx l_x;
	ghost_gidx l_y;
	ghost_gidx l_z;
	ghost_gidx b;
	} grid_site_3D_b4;

matfuncs_topi_sheet_t a_grid;

matfuncs_topi_sheet_t * init_Anderson_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z ){
	
	a_grid.OBC_x = 0;
	a_grid.OBC_y = 0;
	a_grid.OBC_z = 0;
	
	a_grid.l_x = 10;
	a_grid.l_y = 10;
	a_grid.l_z = 10;
	
	a_grid.local_dim = a_grid.l_z*a_grid.l_y*a_grid.l_x;
	
	a_grid.mpi_size_x = mpi_size_x;
	a_grid.mpi_size_y = mpi_size_y;
	a_grid.mpi_size_z = mpi_size_z;
	
	
	a_grid.global_dim = a_grid.mpi_size_z * a_grid.mpi_size_y * a_grid.mpi_size_x * a_grid.local_dim;
	a_grid.g_x = a_grid.mpi_size_x * a_grid.l_x;
	a_grid.g_y = a_grid.mpi_size_y * a_grid.l_y;
	a_grid.g_z = a_grid.mpi_size_z * a_grid.l_z;
	
	
return &a_grid;
}


int set_local_size_Anderson_Sheet(ghost_gidx l_x, ghost_gidx l_y, ghost_gidx l_z){

	a_grid.l_x = l_x;
	a_grid.l_y = l_y;
	a_grid.l_z = l_z;
	a_grid.local_dim = a_grid.l_z*a_grid.l_y*a_grid.l_x;
	
	a_grid.global_dim = a_grid.mpi_size_z * a_grid.mpi_size_y * a_grid.mpi_size_x * a_grid.local_dim;
	a_grid.g_x = a_grid.mpi_size_x * a_grid.l_x;
	a_grid.g_y = a_grid.mpi_size_y * a_grid.l_y;
	a_grid.g_z = a_grid.mpi_size_z * a_grid.l_z;
	
return 0;
}


ghost_gidx getDIM_Anderson_Sheet(){
	return a_grid.g_x*a_grid.g_y*a_grid.g_z;
}

grid_site_3D_b4 a_index_2_grid3D_b4( ghost_gidx idx ){
	grid_site_3D_b4 site;
	
	int rank = (int)(idx/a_grid.local_dim);
	ghost_gidx l_idx = idx - rank*a_grid.local_dim;
	//printf("%d, %d, %d\n",rank, a_grid.local_dim, idx);
	
	site.mpi_rank_x = rank/(a_grid.mpi_size_y*a_grid.mpi_size_z);                   rank -= a_grid.mpi_size_y*a_grid.mpi_size_z*site.mpi_rank_x;
	site.mpi_rank_y = rank/(                a_grid.mpi_size_z); site.mpi_rank_z = rank -                  a_grid.mpi_size_z*site.mpi_rank_y;
	
	
	site.l_x = l_idx/(a_grid.l_y*a_grid.l_z);           l_idx -= a_grid.l_y*a_grid.l_z * site.l_x;
	site.l_y = l_idx/(         a_grid.l_z);           l_idx -=          a_grid.l_z * site.l_y;
	site.l_z = l_idx                    ;  site.b = l_idx -                     site.l_z;
	
	return site;
	}



ghost_gidx a_grid3D_b4_2_index( grid_site_3D_b4  site ){
	
	if ( site.l_x >= a_grid.l_x ) { site.l_x -= a_grid.l_x; site.mpi_rank_x += 1; }
	if ( site.l_x <  0        ) { site.l_x += a_grid.l_x; site.mpi_rank_x -= 1; }
	if ( site.l_y >= a_grid.l_y ) { site.l_y -= a_grid.l_y; site.mpi_rank_y += 1; }
	if ( site.l_y <  0        ) { site.l_y += a_grid.l_y; site.mpi_rank_y -= 1; }
	if ( site.l_z >= a_grid.l_z ) { site.l_z -= a_grid.l_z; site.mpi_rank_z += 1; }
	if ( site.l_z <  0        ) { site.l_z += a_grid.l_z; site.mpi_rank_z -= 1; }
	
	if( site.mpi_rank_x == a_grid.mpi_size_x )  site.mpi_rank_x = 0;
	if( site.mpi_rank_x == -1              )  site.mpi_rank_x = a_grid.mpi_size_x-1;
	if( site.mpi_rank_y == a_grid.mpi_size_y )  site.mpi_rank_y = 0;
	if( site.mpi_rank_y == -1              )  site.mpi_rank_y = a_grid.mpi_size_y-1;
	if( site.mpi_rank_z == a_grid.mpi_size_z )  site.mpi_rank_z = 0;
	if( site.mpi_rank_z == -1              )  site.mpi_rank_z = a_grid.mpi_size_z-1;
	
	return ((((((     site.mpi_rank_x)*a_grid.mpi_size_y 
	                 + site.mpi_rank_y)*a_grid.mpi_size_z
	                 + site.mpi_rank_z)*a_grid.l_x
	                 + site.l_x       )*a_grid.l_y 
	                 + site.l_y       )*a_grid.l_z
	                 + site.l_z       )
	                 + site.b;
	}



double * init_Anderson_Sheet_anderson_disorder(  int seed, double gamma){ //, COMM_t * comm ){
	srand(seed);
	ghost_gidx i;
	ghost_gidx N = a_grid.g_x*a_grid.g_y*a_grid.g_z;
	
	if(!a_grid.V) a_grid.V = malloc(sizeof(double)*N);
	
	
	for(i=0;i<N;i++){
		/*
		grid_site_3D_b4 site = a_index_2_grid3D_b4( i );
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*a_grid.l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*a_grid.l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*a_grid.l_z;
		ghost_gidx p = site.b;
		if( (h==0) || (h==a_grid.g_z-1) )  a_grid.V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
		else                             a_grid.V[i] = 0.;
		*/
		a_grid.V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
	}
	
	
	
	a_grid.use_V = 1;
	
	//MPI_Bcast(Topi_V, N, 0, *comm );
	
	return a_grid.V;
}

ghost_datatype crs_Anderson_Sheet_get_DT(){
	return (GHOST_DT_DOUBLE|GHOST_DT_REAL);
}


ghost_gidx crs_Anderson_Sheet_get_max_nnz(){
	return  7;
}


int crsAnderson_sheet( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg){
    UNUSED(arg);

	matfuncs_topi_sheet_t * topi_s;
	topi_s = &a_grid;

double * Topi_V = topi_s->V;
int32_t Topi_use_V = topi_s->use_V;

ghost_gidx L = topi_s->g_x;
ghost_gidx W = topi_s->g_y;
ghost_gidx H = topi_s->g_z;
ghost_gidx P = 1;
//#define       P  4


ghost_gidx OBC_L = topi_s->OBC_x;
ghost_gidx OBC_W = topi_s->OBC_y;
ghost_gidx OBC_H = topi_s->OBC_z;


	ghost_gidx N = L*W*H*P;

	if ((row >-1 ) && (row <N)){       //  defined output -- write entries of #row in *cols and *vals
	                                   //                    return number of entries
		double * dvals = vals;
		ghost_gidx i = 0 ;
		
		grid_site_3D_b4 site = a_index_2_grid3D_b4( row );
		grid_site_3D_b4 site_tmp;
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi_s->l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi_s->l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi_s->l_z;
		ghost_gidx p = site.b;
		
		double V = 0.;
		
		if(topi_s->use_V) V += topi_s->V[ ((l)*topi_s->l_y+w)*topi_s->l_z + h];
		site_tmp.b=0;
		if(l>=OBC_L)   {  dvals[i] = -1.;
		                     site_tmp = site; site_tmp.l_x--; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}
		if(w>=OBC_W)   {  dvals[i] = -1.;
		                     site_tmp = site; site_tmp.l_y--; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}
		if(h>=OBC_H)   {  dvals[i] = -1.;            
		                     site_tmp = site; site_tmp.l_z--; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}
		                  dvals[i] = V; cols[i] = row;     i++;
		
		if(h<H-OBC_H ) {  dvals[i] = -1.;            
		                     site_tmp = site; site_tmp.l_z++; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}
		if(w<W-OBC_W) {   dvals[i] = -1.;            
		                     site_tmp = site; site_tmp.l_y++; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}
		if(l<L-OBC_L) {   dvals[i] = -1.;            
		                     site_tmp = site; site_tmp.l_x++; cols[i]= a_grid3D_b4_2_index(site_tmp);
		                     i++;}


	//*nnz = i;
	*nnz = (ghost_lidx)(i);
	return  0;
	}

	*nnz = -1;
	return  1;              //  error
}




