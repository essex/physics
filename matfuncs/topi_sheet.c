#include "matfuncs.h"
#include <math.h>
#include <complex.h>
#include <ghost.h>
#include <ghost/math.h>

typedef struct {
	int mpi_rank;
	int mpi_rank_x;
	int mpi_rank_y;
	int mpi_rank_z;
	ghost_lidx local_row;
	ghost_gidx l_x;
	ghost_gidx l_y;
	ghost_gidx l_z;
	ghost_gidx b;
	} grid_site_3D_b4;

matfuncs_topi_sheet_t topi;

matfuncs_topi_sheet_t * init_Topi_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z ){
	
	topi.OBC_x = 0;
	topi.OBC_y = 0;
	topi.OBC_z = 1;
	
	topi.l_x = 10;
	topi.l_y = 10;
	topi.l_z = 10;
	
	topi.local_dim = topi.l_z*topi.l_y*topi.l_x*4;
	
	topi.mpi_size_x = mpi_size_x;
	topi.mpi_size_y = mpi_size_y;
	topi.mpi_size_z = mpi_size_z;
	
	
	topi.global_dim = topi.mpi_size_z * topi.mpi_size_y * topi.mpi_size_x * topi.local_dim;
	topi.g_x = topi.mpi_size_x * topi.l_x;
	topi.g_y = topi.mpi_size_y * topi.l_y;
	topi.g_z = topi.mpi_size_z * topi.l_z;
	
	topi.V = NULL;
	topi.use_V  = 0;
	
	topi.BZ_fild = NULL;
	topi.use_BZ_fild  = 0;
	
	topi.peierls_fild = NULL;
	topi.use_peierls_fild  = 0;
	
	topi.m = 2.;
	
	topi.gp = 0.;
	topi.gm = 0.;
	topi.D1 = 0.;
	topi.D2 = 0.;
	topi.BZ.x = 0.;
	topi.BZ.y = 0.;
	topi.BZ.z = 0.;
	topi.Phi_y = 0.;
	topi.Phi_z = 0.;

	topi.use_D;
	topi.use_Z;

	topi.Dot_R = 25.;
	topi.Dot_V = 0.15327;  //0.527, 0.302
	topi.use_Dot = 0;
	
	topi.k_num = -1;
	topi.k_block_size = 0;
	topi.k_index_offset = 0;
	topi.kx    = NULL;
	topi.ky    = NULL;
	topi.kz    = NULL;
	
	topi.LDOS_num = -1;
	topi.LDOS_block_size = 0;
	topi.LDOS_index_offset = 0;
	topi.LDOS_nx = NULL;
	topi.LDOS_ny = NULL;
	topi.LDOS_nz = NULL;
	
return &topi;
}

int init_Topi_Sheet_LDOS_states( ghost_gidx Nx, ghost_gidx Ny, ghost_gidx Nx0, ghost_gidx Ny0, ghost_gidx dx, ghost_gidx dy, ghost_gidx blocksize  ){
	
	topi.LDOS_block_size = blocksize;
	topi.LDOS_index_offset = 0;
	
	
	ghost_gidx N = Nx*Ny;
	if ( N%blocksize ) N += blocksize - N%blocksize;
	
	topi.LDOS_num = N;
	
	topi.LDOS_nx = malloc(sizeof(ghost_gidx)*N);
	topi.LDOS_ny = malloc(sizeof(ghost_gidx)*N);
	topi.LDOS_nz = malloc(sizeof(ghost_gidx)*N);
	
	ghost_gidx i,j;
	for(i=0;i<N;i++)  topi.LDOS_nx[i] = 0;
	for(i=0;i<N;i++)  topi.LDOS_ny[i] = 0;
	for(i=0;i<N;i++)  topi.LDOS_nz[i] = 0;
	
	for(i=0;i<Nx;i++)for(j=0;j<Ny;j++){
		topi.LDOS_nx[i*Ny+j] = Nx0 + i*dx;
		topi.LDOS_ny[i*Ny+j] = Ny0 + j*dy;
		}
	
	return 0;
}

int vecTopi_sheet_LDOS_states_offset_increment( ghost_gidx N ){
	
	topi.LDOS_index_offset += N;
	if( topi.LDOS_index_offset < topi.LDOS_num ) return 0;
	
	return 1;
}

int init_Topi_Sheet_wave_states( ghost_gidx k_num, double kx_min, double kx_max, double ky_min, double ky_max, double kz_min, double kz_max, ghost_gidx blocksize){
	
	topi.k_block_size = blocksize;
	topi.k_index_offset = 0;
	
	
	topi.k_num = k_num;
	if ( topi.k_num%blocksize ) topi.k_num += blocksize - topi.k_num%blocksize;
	                
	
	double kx_d = (kx_max-kx_min)/(k_num);
	double ky_d = (ky_max-ky_min)/(k_num);
	double kz_d = (kz_max-kz_min)/(k_num);
	
	topi.kx = malloc(sizeof(double)*topi.k_num);
	topi.ky = malloc(sizeof(double)*topi.k_num);
	topi.kz = malloc(sizeof(double)*topi.k_num);
	
	ghost_gidx i;
	for (i=0; i<topi.k_num; i++) topi.kx[i] = 0.;
	for (i=0; i<topi.k_num; i++) topi.ky[i] = 0.;
	for (i=0; i<topi.k_num; i++) topi.kz[i] = 0.;
	
	for (i=0; i<k_num; i++) topi.kx[i] = kx_min + i*kx_d;
	for (i=0; i<k_num; i++) topi.ky[i] = ky_min + i*ky_d;
	for (i=0; i<k_num; i++) topi.kz[i] = kz_min + i*kz_d;
	
return 0;
}

int vecTopi_sheet_wave_offset_increment( ghost_gidx N ){
	
	topi.k_index_offset += N;
	if( topi.k_index_offset < topi.k_num ) return 0;
	
	return 1;
}


int set_local_size_Topi_Sheet(ghost_gidx l_x, ghost_gidx l_y, ghost_gidx l_z){

	topi.l_x = l_x;
	topi.l_y = l_y;
	topi.l_z = l_z;
	topi.local_dim = topi.l_z*topi.l_y*topi.l_x*4;
	
	topi.global_dim = topi.mpi_size_z * topi.mpi_size_y * topi.mpi_size_x * topi.local_dim;
	topi.g_x = topi.mpi_size_x * topi.l_x;
	topi.g_y = topi.mpi_size_y * topi.l_y;
	topi.g_z = topi.mpi_size_z * topi.l_z;
	
	printf(" set_local_size_Topi_Sheet:  gl_grid: %d-%d-%d lo_size: %d-%d-%d\n", topi.mpi_size_x, topi.mpi_size_y, topi.mpi_size_z, topi.l_x, topi.l_y, topi.l_z );
	
return 0;
}

int set_parm_delta_Topi_Sheet( double D1, double D2){
	topi.use_D = 1;
	topi.D1    = D1;
	topi.D2    = D2;
	return 0;
}

int set_parm_dot_Topi_Sheet( double R, double V){
	topi.Dot_R = R;
	topi.Dot_V = V;
	topi.use_Dot = 1;
	return 0;
}

int set_parm_m_Topi_Sheet( double m){
	topi.m    = m;
	return 0;
}


ghost_gidx getDIM_Topi_Sheet(){
	return topi.g_x*topi.g_y*topi.g_z*4;
}

ghost_gidx getH_Topi_Sheet(){
	return topi.g_z;
}

grid_site_3D_b4 index_2_grid3D_b4( ghost_gidx idx ){
	grid_site_3D_b4 site;
	
	
	int rank = (int)(idx/topi.local_dim);
	//ghost_gidx l_idx = idx%topi.local_dim;
	ghost_gidx l_idx = idx - rank*topi.local_dim;
	//printf("%d, %d, %d\n",rank, topi.local_dim, idx);
	
	site.mpi_rank = rank;
	site.local_row = (ghost_lidx)l_idx;
	
	site.mpi_rank_x = rank/(topi.mpi_size_y*topi.mpi_size_z);                   rank -= topi.mpi_size_y*topi.mpi_size_z*site.mpi_rank_x;
	site.mpi_rank_y = rank/(                topi.mpi_size_z); site.mpi_rank_z = rank -                  topi.mpi_size_z*site.mpi_rank_y;
	
	
	site.l_x = l_idx/(topi.l_y*topi.l_z*4);           l_idx -= topi.l_y*topi.l_z*4 * site.l_x;
	site.l_y = l_idx/(         topi.l_z*4);           l_idx -=          topi.l_z*4 * site.l_y;
	site.l_z = l_idx/                   4 ;  site.b = l_idx -                    4 * site.l_z;
	
	return site;
	}

ghost_gidx grid3D_b4_check_z_layer(ghost_gidx layer  ){
	
	ghost_gidx i, count=0,N=getDIM_Topi_Sheet();
	
	for (i=0;i<N;i++){
		grid_site_3D_b4 site = index_2_grid3D_b4( i );
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		if (h == layer) count++;
	}
	
	return count;
}


ghost_gidx grid3D_b4_2_index( grid_site_3D_b4  site ){
	
	if ( site.l_x >= topi.l_x ) { site.l_x -= topi.l_x; site.mpi_rank_x += 1; }
	if ( site.l_x <  0        ) { site.l_x += topi.l_x; site.mpi_rank_x -= 1; }
	if ( site.l_y >= topi.l_y ) { site.l_y -= topi.l_y; site.mpi_rank_y += 1; }
	if ( site.l_y <  0        ) { site.l_y += topi.l_y; site.mpi_rank_y -= 1; }
	if ( site.l_z >= topi.l_z ) { site.l_z -= topi.l_z; site.mpi_rank_z += 1; }
	if ( site.l_z <  0        ) { site.l_z += topi.l_z; site.mpi_rank_z -= 1; }
	
	if( site.mpi_rank_x == topi.mpi_size_x )  site.mpi_rank_x = 0;
	if( site.mpi_rank_x == -1              )  site.mpi_rank_x = topi.mpi_size_x-1;
	if( site.mpi_rank_y == topi.mpi_size_y )  site.mpi_rank_y = 0;
	if( site.mpi_rank_y == -1              )  site.mpi_rank_y = topi.mpi_size_y-1;
	if( site.mpi_rank_z == topi.mpi_size_z )  site.mpi_rank_z = 0;
	if( site.mpi_rank_z == -1              )  site.mpi_rank_z = topi.mpi_size_z-1;
	
	return ((((((     site.mpi_rank_x)*topi.mpi_size_y 
	                 + site.mpi_rank_y)*topi.mpi_size_z
	                 + site.mpi_rank_z)*topi.l_x
	                 + site.l_x       )*topi.l_y 
	                 + site.l_y       )*topi.l_z
	                 + site.l_z       )*4
	                 + site.b;
	}

int init_Topi_Sheet_anderson_disorder_BZ(  int seed, double gamma, double Bx_offset, double By_offset, double Bz_offset, double gp, double gm){ //, COMM_t * comm ){
	srand(seed);
	ghost_gidx i;
	ghost_gidx N = topi.g_x*topi.g_y*topi.g_z;
	
	
	if(!topi.BZ_fild) topi.BZ_fild = malloc(sizeof(double_3d)*N);
	double_3d tmp;
	double r;
	double phi;
	double theta;
	
	for(i=0;i<N;i++) {
		
		
		
		
		r     =   gamma*  pow( ((double)rand())/((double)RAND_MAX), 0.333333333333333);
		phi   = 2.*M_PI*( ((double)rand())/((double)RAND_MAX) );
		theta =    M_PI*( ((double)rand())/((double)RAND_MAX) );
		tmp.x = Bx_offset + r*cos(phi)*sin(theta);
		tmp.y = By_offset + r*sin(phi)*sin(theta);
		tmp.z = Bz_offset + r*         cos(theta);
		topi.BZ_fild[i] = tmp;
		/*
		tmp.x = 0.;
		tmp.y = 0.;
		tmp.z = 0.;
		grid_site_3D_b4 site = index_2_grid3D_b4( i );
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		if( (h==0) || (h==topi.g_z-1) ) topi.BZ_fild[i] = tmp;
		*/
		
	}
		
	topi.use_BZ_fild = 1;
	topi.use_Z = 1;
	topi.gp = gp;
	topi.gm = gm;
	//MPI_Bcast(Topi_V, N, 0, *comm );
	
	return 0;
}

int init_Topi_Sheet_anderson_disorder_peierls(  int seed, double gamma){ //, COMM_t * comm ){
	srand(seed);
	ghost_gidx i;
	ghost_gidx N = topi.g_x*topi.g_y*topi.g_z;
	
	
	if(!topi.peierls_fild) topi.peierls_fild = malloc(sizeof(double_3d)*N);
	double_3d tmp;
	
	for(i=0;i<N;i++) {

		tmp.x =   gamma*M_PI*(( ((double)rand())/((double)RAND_MAX) ) - 0.5);
		tmp.y =   gamma*M_PI*(( ((double)rand())/((double)RAND_MAX) ) - 0.5);
		tmp.z =   gamma*M_PI*(( ((double)rand())/((double)RAND_MAX) ) - 0.5);
		topi.peierls_fild[i] = tmp;
	}
		
	topi.use_peierls_fild = 1;
	//MPI_Bcast(Topi_V, N, 0, *comm );
	
	return 0;
}


double * init_Topi_Sheet_anderson_disorder(  int seed, double gamma){ //, COMM_t * comm ){
	srand(seed);
	ghost_gidx i;
	ghost_gidx N = topi.g_x*topi.g_y*topi.g_z;
	
	if(!topi.V) topi.V = malloc(sizeof(double)*N);
	
	
	for(i=0;i<N;i++){
		/*
		grid_site_3D_b4 site = index_2_grid3D_b4( i );
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		ghost_gidx p = site.b;
		if( (h==0) || (h==topi.g_z-1) )  topi.V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
		else                             topi.V[i] = 0.;
		*/
		topi.V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
	}
	
	
	
	topi.use_V = 1;
	
	//MPI_Bcast(Topi_V, N, 0, *comm );
	
	return topi.V;
}

ghost_datatype crs_Topi_Sheet_get_DT(){
	return (GHOST_DT_DOUBLE|GHOST_DT_COMPLEX);
}

/*
double crs_Topi_Sheet_get_eig_range_low(){
	double val = -5.2;
	if ( topi.use_D ) val -= fabs(topi.D1) + fabs(topi.D2);
	if ( topi.use_Z ) val -= (fabs(topi.gp)+fabs(topi.gm))*(fabs(topi.Bx)+fabs(topi.By)+fabs(topi.Bz));
	return val -  fabs(topi.Dot_V);
}

double crs_Topi_Sheet_eig_range_high(){
	double val = 5.5;
	if ( topi.use_D ) val += fabs(topi.D1) + fabs(topi.D2);
	if ( topi.use_Z ) val += (fabs(topi.gp)+fabs(topi.gm))*(fabs(topi.Bx)+fabs(topi.By)+fabs(topi.Bz));
	return  val + fabs(topi.Dot_V);
}
*/

ghost_gidx crs_Topi_Sheet_get_max_nnz(){
	return  4*7;
}


int crsTopi_sheet( ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg){
    UNUSED(arg);

	matfuncs_topi_sheet_t * topi_s;
	topi_s = &topi;

double * Topi_V = topi_s->V;
int32_t Topi_use_V = topi_s->use_V;

ghost_gidx L = topi_s->g_x;
ghost_gidx W = topi_s->g_y;
ghost_gidx H = topi_s->g_z;
ghost_gidx P = 4;
//#define       P  4


ghost_gidx OBC_L = topi_s->OBC_x;
ghost_gidx OBC_W = topi_s->OBC_y;
ghost_gidx OBC_H = topi_s->OBC_z;

double m     = topi_s->m;
double gp    = topi_s->gp;
double gm    = topi_s->gm;
double D1    = topi_s->D1;
double D2    = topi_s->D2;
double_3d BZ = topi_s->BZ;
//double Bx    = topi_s->Bx;
//double By    = topi_s->By;
//double Bz    = topi_s->Bz;
double Phi_y = topi_s->Phi_y;
double Phi_z = topi_s->Phi_z;

int32_t use_D   = topi_s->use_D;
int32_t use_Z   = topi_s->use_Z;


double Dot_R = topi_s->Dot_R;
double Dot_V = topi_s->Dot_V;

	ghost_gidx N = L*W*H*P;

	if ((row >-1 ) && (row <N)){       //  defined output -- write entries of #row in *cols and *vals
	                                   //                    return number of entries
		double complex * zvals = vals;
		ghost_gidx i = 0 ;
		
		grid_site_3D_b4 site = index_2_grid3D_b4( row );
		grid_site_3D_b4 site_tmp;
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi_s->l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi_s->l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi_s->l_z;
		ghost_gidx p = site.b;
		
		
		double V = 0.;
		
		
		if(topi.use_BZ_fild){ 
			BZ.x += topi.BZ_fild[ ((l)*topi.l_y+w)*topi.l_z + h].x;
			BZ.y += topi.BZ_fild[ ((l)*topi.l_y+w)*topi.l_z + h].y;
			BZ.z += topi.BZ_fild[ ((l)*topi.l_y+w)*topi.l_z + h].z;
		}
		
		if(topi.use_V) V += topi.V[ ((l)*topi.l_y+w)*topi.l_z + h];
		
		double Dot_D = 4*Dot_R;
		double Dot_cx= 2*Dot_R;
		double Dot_cy= 2*Dot_R;
		double Dot_x = fmod( l , Dot_D );
		double Dot_y = fmod( w , Dot_D );
		
		if (topi.use_Dot){
		  if( (((Dot_cx-Dot_x)*(Dot_cx-Dot_x) + (Dot_cy-Dot_y)*(Dot_cy-Dot_y)) < Dot_R*Dot_R )
		     //&&  (    (h==0) || (h==H-1) 
		     //      || (h==1) || (h==H-2)
		     //    ) 
		      )  V += Dot_V;
		  }

		double complex  phase_x_h = 1.;
		double complex  phase_x_l = 1.;
		double complex  phase_y_h = 1.;
		double complex  phase_y_l = 1.;
		double complex  phase_z_h = 1.;
		double complex  phase_z_l = 1.;
		
		
		if (topi.use_peierls_fild){
			  phase_x_l *= cexp(-I*topi.peierls_fild[ (  l             *topi.l_y+  w             )*topi.l_z +   h              ].x);
			  phase_x_h *= cexp( I*topi.peierls_fild[ (((l+1)%topi.g_x)*topi.l_y+  w             )*topi.l_z +   h              ].x);
			  phase_y_l *= cexp(-I*topi.peierls_fild[ (  l             *topi.l_y+  w             )*topi.l_z +   h              ].y);
			  phase_y_h *= cexp( I*topi.peierls_fild[ (  l             *topi.l_y+((w+1)%topi.g_y))*topi.l_z +   h              ].y);
			  phase_z_l *= cexp(-I*topi.peierls_fild[ (  l             *topi.l_y+  w             )*topi.l_z +   h              ].z);
			  phase_z_h *= cexp( I*topi.peierls_fild[ (  l             *topi.l_y+  w             )*topi.l_z + ((h+1)%topi.g_z) ].z);
			}
		
		
		ghost_gidx   iHx[4][2]   = {{ 0 ,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHx_l[4][2] = {{-0.5,        0.5},
		                              {    0.5,  0.5},
		                              {   -0.5, -0.5},
		                              {-0.5,        0.5}};
		double complex vHx_u[4][2] = {{-0.5,       -0.5},
		                              {    0.5, -0.5},
		                              {    0.5, -0.5},
		                              { 0.5,        0.5}};

		ghost_gidx   iHy[4][2]   = {{ 0L,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHy_l[4][2] = {{  -0.5,            -I*0.5},
		                              {         0.5,-I*0.5},
		                              {      -I*0.5,  -0.5},
		                              {-I*0.5,              0.5}};
		double complex vHy_u[4][2] = {{  -0.5,             I*0.5},
		                              {         0.5, I*0.5},
		                              {       I*0.5,  -0.5},
		                              { I*0.5,              0.5}};

		ghost_gidx   iHz[4][2]   = {{ 0 , 1        },
		                              { 0 , 1        },
		                              {        2 , 3 },
		                              {        2 , 3 }};
		double complex vHz_l[4][2] = {{  -0.5,  0.5           },
		                              {  -0.5,  0.5           },
		                              {              -0.5, 0.5},
		                              {              -0.5, 0.5}};
		double complex vHz_u[4][2] = {{  -0.5, -0.5           },
		                              {   0.5,  0.5           },
		                              {             -0.5, -0.5},
		                              {              0.5,  0.5}};

		

		

		if(l>=OBC_L)   {  zvals[i] = vHx_l[p][0]*phase_x_l;
		                     site_tmp = site; site_tmp.l_x--; site_tmp.b=iHx[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHx_l[p][1]*phase_x_l;            
		                     site_tmp = site; site_tmp.l_x--; site_tmp.b=iHx[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(w>=OBC_W)   {  zvals[i] = vHy_l[p][0]*phase_y_l;
		                     site_tmp = site; site_tmp.l_y--; site_tmp.b=iHy[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHy_l[p][1]*phase_y_l;            
		                     site_tmp = site; site_tmp.l_y--; site_tmp.b=iHy[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(h>=OBC_H)   {  zvals[i] = vHz_l[p][0]*phase_z_l;            
		                     site_tmp = site; site_tmp.l_z--; site_tmp.b=iHz[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHz_l[p][1]*phase_z_l;            
		                     site_tmp = site; site_tmp.l_z--; site_tmp.b=iHz[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		switch(p) {
		case 0 :             zvals[i] = V + m; if(use_Z) zvals[i] +=BZ.z*(gp+gm);   cols[i] = row;     i++;
		         if(use_D) { zvals[i] =  D1-I*D2;                                   cols[i] = row+1 ;  i++;}
		         if(use_Z) { zvals[i] = (BZ.x-I*BZ.y)*(gp+gm);                      cols[i] = row+2 ;  i++;}
		     break;
		case 1 : if(use_D) { zvals[i] =  D1+I*D2;                                    cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] +=  BZ.z*(gp-gm);  cols[i] = row;     i++;
		         if(use_Z) { zvals[i] = (BZ.x-I*BZ.y)*(gp-gm);                       cols[i] = row+2 ;  i++;}
		     break;
		case 2 : if(use_Z){ zvals[i] = (BZ.x+I*BZ.y)*(gp+gm);                        cols[i] = row-2 ;  i++;}
		                     zvals[i] =  V + m;  if(use_Z) zvals[i] -= BZ.z*(gp+gm); cols[i] = row;     i++;
		         if(use_D) { zvals[i] = -D1+I*D2;                                    cols[i] = row+1 ;  i++;}
		     break;
		case 3 : if(use_Z) { zvals[i] = (BZ.x+I*BZ.y)*(gp-gm);                        cols[i] = row-2 ;  i++;}
		         if(use_D) { zvals[i] = -D1-I*D2;                                     cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] -=  BZ.z*(gp-gm);   cols[i] = row;     i++;
		     break;
		}
		
		if(h<H-OBC_H ) {    zvals[i] = vHz_u[p][0]*phase_z_h;            
		                     site_tmp = site; site_tmp.l_z++; site_tmp.b=iHz[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHz_u[p][1]*phase_z_h;            
		                     site_tmp = site; site_tmp.l_z++; site_tmp.b=iHz[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(w<W-OBC_W) {    zvals[i] = vHy_u[p][0]*phase_y_h;            
		                     site_tmp = site; site_tmp.l_y++; site_tmp.b=iHy[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHy_u[p][1]*phase_y_h;            
		                     site_tmp = site; site_tmp.l_y++; site_tmp.b=iHy[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(l<L-OBC_L) {    zvals[i] = vHx_u[p][0]*phase_x_h;            
		                     site_tmp = site; site_tmp.l_x++; site_tmp.b=iHx[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHx_u[p][1]*phase_x_h;            
		                     site_tmp = site; site_tmp.l_x++; site_tmp.b=iHx[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }

			
			




	//*nnz = i;
	*nnz = (ghost_lidx)(i);
	return  0;
	}else if( row == -1 ){
		
		matfuncs_info_t *info = vals;

		info->version   = 1;
		info->base      = 0;
		info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
		info->datatype  = crs_Topi_Sheet_get_DT();
		info->nrows     = getDIM_Topi_Sheet();
		info->ncols     = getDIM_Topi_Sheet();
		info->row_nnz   = crs_Topi_Sheet_get_max_nnz();

		info->hermit    =  1;
		info->eig_info  =  0;
		//info->eig_down  = -8.;
		//info->eig_top   =  8.;

		return 0;
	}


	*nnz = -1;
	return  1;              //  error
}



int vecTopi_sheet_Observable_z(void *Observable, ghost_gidx row, ghost_gidx n_cols, void *vec_vals){
	
	//ghost_gidx N = L*W*H*P;
	grid_site_3D_b4 site = index_2_grid3D_b4( row );
	
	//ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
	//ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
	ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
	//ghost_gidx p = site.b;
	
	
	
	double complex * vals = vec_vals;
	double * sum = Observable;
	
	ghost_gidx i;
	sum[h] += creal(vals[0])*creal(vals[0]) + cimag(vals[0])*cimag(vals[0]);
	//for(i=0;i<n_cols;i++) sum[h] += creal(vals[i])*creal(vals[i]) + cimag(vals[i])*cimag(vals[i]);
	
	return 0;
}

int vecTopi_sheet_Observable_z_dense(double *z_dense, ghost_densemat * vec, int rank){
	
	double complex * vals = (double complex *)(vec->val);
	//int rank;
	//ghost_rank(&rank,vec->context->mpicomm);
	ghost_lidx i;
	ghost_gidx j;
	
	for( j=0; j < topi.g_z; j++) z_dense[j] = 0.;
	
	for( i=0; i < vec->map->dim; i++) { 
	    ghost_gidx     global_row_index = i + vec->map->offs;
	    grid_site_3D_b4 site = index_2_grid3D_b4( global_row_index );
	    ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
	    ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
	    ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
	    double complex val = vals[ i*vec->traits.ncols ];
	    z_dense[h] += ( creal(val)*creal(val) + cimag(val)*cimag(val));
	}
/*	
#ifdef GHOST_HAVE_MPI
        {
            ghost_mpi_op op;
            ghost_mpi_datatype dt;
            ghost_mpi_op_sum(&op,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            ghost_mpi_datatype(&dt,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            MPI_Allreduce( MPI_IN_PLACE, z_dense, topi.g_z, dt, op, vec->context->mpicomm);
        }
#endif
*/	
	
	return 0;
}

int vecTopi_sheet_Observable_xy(double * xy_plane, ghost_densemat * vec, double * z_view , int rank){
	
	double complex * vals = (double complex *)(vec->val);
	//int rank;
	//ghost_rank(&rank,vec->context->mpicomm);
	ghost_lidx i;
	ghost_gidx j;
	
	for( j=0; j < topi.g_y*topi.g_x; j++) xy_plane[j] = 0.;
	
	for( i=0; i < vec->map->dim; i++) { 
	    ghost_gidx     global_row_index = i + vec->map->offs;
	    grid_site_3D_b4 site = index_2_grid3D_b4( global_row_index );
	    ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
	    ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
	    ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
	    double complex val = vals[ i*vec->traits.ncols ];
	    xy_plane[l*topi.g_y + w] += z_view[h] *( creal(val)*creal(val) + cimag(val)*cimag(val));
	}
/*	
#ifdef GHOST_HAVE_MPI
        {
            ghost_mpi_op op;
            ghost_mpi_datatype dt;
            ghost_mpi_op_sum(&op,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            ghost_mpi_datatype(&dt,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            MPI_Allreduce( MPI_IN_PLACE, xy_plane, topi.g_y*topi.g_x, dt, op, vec->context->mpicomm);
        }
#endif
*/	
	return 0;
}


/*
int vecTopi_sheet_Observable_z_reduce(void *Observable){
#ifdef GHOST_HAVE_MPI
        {
            ghost_mpi_op op;
            ghost_mpi_datatype dt;
            ghost_mpi_op_sum(&op,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            ghost_mpi_datatype(&dt,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            MPI_Allreduce( MPI_IN_PLACE, Observable, topi.g_z, dt, op, MPI_COMM_WORLD);
        }
#endif
return 0;
}
*/



int vecTopi_sheet_surface_state( ghost_gidx row, ghost_lidx col,  void * val, void *arg){
	
		grid_site_3D_b4 site = index_2_grid3D_b4( row );
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		ghost_gidx p = site.b;
		
		double complex spinor[4][4];
		spinor[0][0]= 0.5;
		spinor[0][1]= 0.5;
		spinor[0][2]= 0.0;
		spinor[0][3]= 0.0;
		
		spinor[1][0]= 0.0;
		spinor[1][1]= 0.0;
		spinor[1][2]= 0.5;
		spinor[1][3]= 0.5;
		
		spinor[2][0]= 0.5;
		spinor[2][1]=-0.5;
		spinor[2][2]= 0.0;
		spinor[2][3]= 0.0;
		
		spinor[3][0]= 0.0;
		spinor[3][1]= 0.0;
		spinor[3][2]= 0.5;
		spinor[3][3]=-0.5;
		
		double complex * zval = val;
		
		ghost_gidx i;
		//for(i=0;i<4*K_num;i++)  zval[i] = 0.;
		//for(i=0;i<  K_num;i++)  zval[p+i*4] =  cexp( I*(l*kx[i] + w*ky[i] + h*kz[i]) );
		
		if( h == 0 )   *zval = (spinor[0][p] + 1.*I*spinor[1][p])/sqrt(topi.g_x*topi.g_y );
		//if(!(H-1-h))   *zval = spinor[2][p] + I*spinor[3][p];
		else           *zval = 0.;
		
	return 0;
}


int vecTopi_sheet_wave_states( ghost_gidx row, ghost_lidx col,  void * val, void *arg){
	
		grid_site_3D_b4 site = index_2_grid3D_b4( row );
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		ghost_gidx p = site.b;
		
		
		double complex * zval = val;
		
		ghost_gidx col_n = col/4;
		ghost_gidx col_b = col & (1|2);
		
		//if( (p == col_b) && (col_n<topi.k_num) ){
		//if(  (p == col_b) && (h == 0) ){
		if(  (p == col_b) ){
			
				*zval = cexp( I*( topi.kx[topi.k_index_offset+col_n]*l
				                + topi.ky[topi.k_index_offset+col_n]*w
				                + topi.kz[topi.k_index_offset+col_n]*h ));
			
		}else	*zval = 0.;
		
		
		
	return 0;
}


void print_Topi_sheet_LDOS_sites( ){
	
	ghost_gidx i;
	for ( i=0;i<topi.LDOS_num; i++ ) 
	  printf( "%ld  %ld  %ld  \n", topi.LDOS_nx[i], topi.LDOS_ny[i], topi.LDOS_nz[i]);
	
}

int vecTopi_sheet_LDOS_states( ghost_gidx row, ghost_lidx col,  void * val, void *arg){
	
		grid_site_3D_b4 site = index_2_grid3D_b4( row );
		
		ghost_gidx l = site.l_x + site.mpi_rank_x*topi.l_x;
		ghost_gidx w = site.l_y + site.mpi_rank_y*topi.l_y;
		ghost_gidx h = site.l_z + site.mpi_rank_z*topi.l_z;
		ghost_gidx p = site.b;
		
		
		double complex * zval = val;
		
		ghost_gidx col_n = col/4;
		ghost_gidx col_b = col & (1|2);
		
		if(  (topi.LDOS_nx[col_n + topi.LDOS_index_offset] == l) 
		  && (topi.LDOS_ny[col_n + topi.LDOS_index_offset] == w)
		  && (topi.LDOS_nz[col_n + topi.LDOS_index_offset] == h) && (p == col_b)) *zval = 1.;
		else *zval = 0.;
		
	return 0;
}

void vecTopi_export_grid_indexing( char * file_name ){
	
	FILE * out = fopen( file_name, "w" );
	if(!out) {
		printf("ERROR: vecTopi_export_grid_indexing(): could not open output file %s \n", file_name);
		return;
		}
	
	grid_site_3D_b4 site;
	ghost_gidx row;
	for(row=0;row<topi.global_dim;row++){
		site = index_2_grid3D_b4( row );
		fprintf(out, "row: %05ld\t (node: %03d lrow: %04d)\tx: %ld\ty: %ld\tz:%ld\tb: %ld\n", row , site.mpi_rank, site.local_row,
		                                               site.l_x + site.mpi_rank_x*topi.l_x,
		                                               site.l_y + site.mpi_rank_y*topi.l_y, 
		                                               site.l_z + site.mpi_rank_z*topi.l_z, site.b);
	}
	
	fclose(out);
	
	return ;
}

