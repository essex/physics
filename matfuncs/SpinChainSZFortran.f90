! call with size(rptr) >= 2^L+1,
! size(cind) >= (2*L+1)*2^L.

!!$  uses int32
subroutine smallspinmatrix32(L,rptr,cind)
  integer L
  
  integer rptr(*),cind(*)
  
  logical :: IncludeEnds=.true.
  
  integer NS,NE,il,kl,j
  
  integer :: imask(L)

  ! -1-1- patterns for nearest neighbours
  !  allocate(imask(L)) 
  imask(1)=3
  do j=2,L
     imask(j)=ishftc(imask(j-1),1,L)
  end do

  Ns=2**L

  ! NE=(2*L+1)*NS   !tentatively,   CHECK

  NE=0

  !loop over all states,
  !represented as bit patterns with length L
  do il=0,Ns-1

     !fill one row
     rptr(il+1)=NE+1      !index starts at 1

     !nearest-neighbor terms, given spin switch from J+ J- and J- J+

     do j=1,L
        if (j==L) exit

        kl=ishftc(iand(il,imask(j)),-(j-1),L)
        !now kl is 00,  01,10,  11
        if ((kl==1).or.(kl==2)) then
           !switch 01 <-> 10 spins
           kl=ieor(il,imask(j))
           NE=NE+1
           cind(NE)=kl+1    !index starts at 1
        end if
     end do

     if (IncludeEnds) then
        !flip spin at ends
        kl=ieor(il,1)
        NE=NE+1
        cind(NE)=kl+1
        kl=ieor(il,2**(L-1))
        NE=NE+1
        cind(NE)=kl+1
     end if
     
  end do
  
  rptr(NS+1) = NE+1

  !adjust index for C
  cind(1:NE)=cind(1:NE)-1
  rptr(1:NS+1)=rptr(1:NS+1)-1

end subroutine smallspinmatrix32

!!$ uses int64
subroutine smallspinmatrix64(L,rptr,cind)
  integer(kind=8) L
  
  integer(kind=8) rptr(*),cind(*)
  
  logical :: IncludeEnds=.true.
  
  integer NS,NE,il,kl,j
  
  integer :: imask(L)

  ! -1-1- patterns for nearest neighbours
  !  allocate(imask(L)) 
  imask(1)=3
  do j=2,L
     imask(j)=ishftc(imask(j-1),1,L)
  end do

  Ns=2**L

  ! NE=(2*L+1)*NS   !tentatively,   CHECK

  NE=0

  !loop over all states,
  !represented as bit patterns with length L
  do il=0,Ns-1

     !fill one row
     rptr(il+1)=NE+1      !index starts at 1

     !nearest-neighbor terms, given spin switch from J+ J- and J- J+

     do j=1,L
        if (j==L) exit

        kl=ishftc(iand(il,imask(j)),-(j-1),L)
        !now kl is 00,  01,10,  11
        if ((kl==1).or.(kl==2)) then
           !switch 01 <-> 10 spins
           kl=ieor(il,imask(j))
           NE=NE+1
           cind(NE)=kl+1    !index starts at 1
        end if
     end do

     if (IncludeEnds) then
        !flip spin at ends
        kl=ieor(il,1)
        NE=NE+1
        cind(NE)=kl+1
        kl=ieor(il,2**(L-1))
        NE=NE+1
        cind(NE)=kl+1
     end if
     
  end do
  
  rptr(NS+1) = NE+1

  !adjust index for C
  cind(1:NE)=cind(1:NE)-1
  rptr(1:NS+1)=rptr(1:NS+1)-1

end subroutine smallspinmatrix64


