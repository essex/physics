#include <png.h>
#include <ghost.h>
#include "matfuncs.h"



int  my_print_png ( int ngx_png, int ngy_png, int * a , char * png_file_name ){
    
    unsigned int i;
    FILE * file_ptr;
    
    unsigned int * color_map = (unsigned int  *) malloc(5*sizeof(unsigned int ));
    
     //                     Rot             Gruen         Blau       Transpanenz
    color_map[0]= 0x00000001*255 + 0x00000100*255 + 0x00010000*255 + 0xff000000 ; // weiss
    color_map[1]= 0x00000001*180 + 0x00000100*180 + 0x00010000*180 + 0xff000000 ; // grau
    color_map[2]= 0x00000001*0   + 0x00000100*0   + 0x00010000*0   + 0xff000000 ; // schwarz
    color_map[3]= 0x00000001*255 + 0x00000100*0   + 0x00010000*0   + 0xff000000 ; // rot
    color_map[4]= 0x00000001*0   + 0x00000100*0   + 0x00010000*255 + 0xff000000 ; // blau
     
    /* Strukturdatentyp fuer gesamtes png-Objekt */
    png_structp png;
    
    /* Strukturdatentyp fuer Informationen ueber png-Objekt */
    png_infop info;
  
    int bitdepth = 8; // -> 0 .. 255  
    int rowbytes = ngx_png * bitdepth/8;  // -> gesamte Bittiefe pro Pixsel = 32 = 4 bytes = siteof(int)
                                          //       .. je 8 fuer  r g b a  

    unsigned int * raster = (unsigned int *) malloc(rowbytes*ngy_png*sizeof(unsigned int )) ;
    
    png_bytepp row_pointers = (png_bytepp) malloc(ngy_png*sizeof(png_bytep));

    for (i=0; i<ngy_png; i++)  row_pointers[i] =(unsigned char *)( raster + i*rowbytes);

// #pragma omp parallel for 
  for(i=0; i<ngx_png*ngy_png; i++)  raster[i] = color_map[a[i]];

   file_ptr = fopen(png_file_name, "wb");

  /* Erzeuge png-Struktur */
    png = png_create_write_struct ( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  /* Erzeuge info-Strukur */
  info = png_create_info_struct (png);

  /* Setze jump-buffer */
   setjmp (png_jmpbuf (png));

  /* Initialisiere I/O */
  png_init_io(png, file_ptr);

  /* Generiere ImageHeader */
  png_set_IHDR (png, info, ngx_png, ngy_png, bitdepth, 
                PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, 
                PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  /* Schreibe info-Struktur in png-Datei */
  png_write_info (png, info);

  /* Schreibe Bilddaten in png-File */
  png_write_image (png, row_pointers);
  
  /* Aufraeumarbeiten */
  png_write_end (png, info);
  png_destroy_write_struct (&png, &info);

  fclose(file_ptr);

  free(row_pointers);
  free(raster);
  free(color_map);
return 0;
}



int matfunc_2_png ( char * png_file_name, ghost_sparsemat_rowfunc matfunc  , int * max_size )
{
	//if ( argc < 3) { printf("error: too few arguments\nUse: %s [MatrixName.crs] [ImageName.png]\n", argv[0] ); return 2; }
	
	int default_size[2] = {1024,1024};
	
	if( max_size == NULL ) max_size = default_size;
	
	int bitshift_row = 0;
	int bitshift_col = 0;
	
	matfuncs_info_t info;
	matfunc( -1, NULL, NULL, &info, NULL);
	
	int row_per_pix = 1;
	int col_per_pix = 1;

	if( max_size[0] > info.nrows ) 
		max_size[0] = info.nrows;
	else{
         row_per_pix = info.nrows/max_size[0];
         if( row_per_pix*max_size[0] != info.nrows ) row_per_pix++;
    }

	if( max_size[1] > info.ncols ) 
		max_size[1] = info.ncols;
	else{
         col_per_pix = info.ncols/max_size[1];
         if( col_per_pix*max_size[1] != info.ncols ) col_per_pix++;
    }

	int png_rows = max_size[0];
	int png_cols = max_size[1];
	
	//while( (info.nrows >> bitshift_row) > max_size[0] ) bitshift_row++;
	//while( (info.ncols >> bitshift_col) > max_size[1] ) bitshift_col++;
	
	//int png_rows = info.nrows >> bitshift_row;
	//int png_cols = info.ncols >> bitshift_col;

	int * png_array = (int *) malloc( png_cols*png_rows*sizeof(int));
	
	void * dummy;
	switch ( info.datatype ){
		case GHOST_DT_FLOAT |GHOST_DT_REAL:    dummy = malloc( 4*info.row_nnz); break;
		case GHOST_DT_FLOAT |GHOST_DT_COMPLEX: dummy = malloc( 8*info.row_nnz); break;
		case GHOST_DT_DOUBLE|GHOST_DT_REAL:    dummy = malloc( 8*info.row_nnz); break;
		case GHOST_DT_DOUBLE|GHOST_DT_COMPLEX: dummy = malloc(16*info.row_nnz); break;
	}
	
	
	ghost_gidx * cols = malloc(sizeof(ghost_gidx)*info.row_nnz);
	ghost_lidx nnz;
	int n;
	for( n = 0; n < png_cols*png_rows; n++) png_array[n] = 0;
	
	
	//printf("Matrix %d x %d  (max_row_nnz %d)\n",info.nrows, info.ncols, info.row_nnz);
	
	int row;
	for (row = 0; row < info.nrows; row++){
		matfunc(row, &nnz, cols, dummy, NULL );
		//printf("row %d -> nnz %d\n", row, nnz);
		for (n = 0; n < nnz; n++){ png_array[png_cols*(row / row_per_pix) + (cols[n] / col_per_pix )] = 2; 
			//printf("  %d", cols[n]);
		 }
		//printf("\n");
		//for (n = 0; n < png_cols; n++) printf(" %d", png_array[png_cols*(row >> png_rows) + n] );printf("\n");
	}
	
	//for (row = 0; row < png_rows; row++){
	//for (n = 0; n < png_cols; n++) printf(" %d", png_array[png_cols*row + n] );
	//  printf("\n");}
	
	free(cols);
	free(dummy);
	
	my_print_png ( png_cols, png_rows, png_array , png_file_name );
	
	free(png_array);
	
	return 0;
}


