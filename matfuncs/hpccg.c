#include "matfuncs.h"

static double *val;
static ghost_gidx *col;
static ghost_gidx *rpt;
static ghost_gidx NX = 0, NY = 0, NZ = 0;
static ghost_lidx SEVENPOINT = 0;

static void constructMatrix()
{
    int iz, iy, ix, sz, sy, sx;
    int nnz = 0;
    ghost_gidx total_nrow = NX*NY*NZ;

    val = (double *)malloc(sizeof(double)*NX*NY*NZ*27);
    col = (ghost_gidx *)malloc(sizeof(ghost_gidx)*NX*NY*NZ*27);
    rpt = (ghost_gidx *)malloc(sizeof(ghost_gidx)*(NX*NY*NZ+1));

    if (!val || !col || !rpt) {
        fprintf(stderr,"Error in malloc");
        return;
    }

    rpt[0] = 0;

    long long nNZglobal = 0;
    for (iz=0; iz<NZ; iz++) {
        for (iy=0; iy<NY; iy++) {
            for (ix=0; ix<NX; ix++) {
                int curlocalrow = iz*NX*NY+iy*NX+ix;
                int currow = /*start_row+*/iz*NX*NY+iy*NX+ix;
                rpt[currow+1] = rpt[currow];
                int nNZrow = 0;
                for (sz=-1; sz<=1; sz++) {
                    for (sy=-1; sy<=1; sy++) {
                        for (sx=-1; sx<=1; sx++) {
                            int curcol = currow+sz*NX*NY+sy*NX+sx;
                            if ((ix+sx>=0) && (ix+sx<NX) && (iy+sy>=0) && (iy+sy<NY) && (curcol>=0 && curcol<total_nrow)) {
                                if (!SEVENPOINT || (sz*sz+sy*sy+sx*sx<=1)) {
                                    if (curcol==currow) {
                                        val[nnz] = 27.0;
                                    }
                                    else {
                                        val[nnz] = -1.0;
                                    }
                                    col[nnz] = curcol;
                                    rpt[currow+1]++;
                                    nnz++;
                                }
                            }
                        } // end sx loop
                    } // end sy loop
                } // end sz loop
            } // end ix loop
        } // end iy loop
    } // end iz loop


}


int hpccg(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *curcol, void *vval, void *arg)
{
    UNUSED(arg);
    double *curval = (double *)vval;


    if (currow < -1) {
        NX = curcol[0];
        NY = curcol[1];
        NZ = curcol[2];
        constructMatrix();
        return 0;
    }

    memcpy(curval,&val[rpt[currow]],sizeof(double)*(rpt[currow+1]-rpt[currow]));
    memcpy(curcol,&col[rpt[currow]],sizeof(ghost_gidx)*(rpt[currow+1]-rpt[currow]));
    *currowlen = rpt[currow+1]-rpt[currow];

    return 0;
}


int hpccg_onthefly(ghost_gidx currow, ghost_lidx *currowlen, ghost_gidx *col, void *vval, void *arg)
{
    UNUSED(arg);
    double *curval = (double *)vval;

    if (currow < -1) {
        NX = col[0];
        NY = col[1];
        NZ = col[2];
        return 0;
    }

    ghost_gidx ix, iy, iz;
    ghost_gidx sx, sy, sz;
    iz =  currow                  /(NX*NY);
    iy = (currow -  iz*NY    *NX) / NX;
    ix =  currow - (iz*NY+iy)*NX;

    ghost_lidx nnz = 0;
    ghost_gidx total_nrow = NX*NY*NZ;

    for (sz=-1; sz<=1; sz++) {
        for (sy=-1; sy<=1; sy++) {
            for (sx=-1; sx<=1; sx++) {
                ghost_gidx curcol = currow+sz*NX*NY+sy*NX+sx;
                if ((ix+sx>=0) && (ix+sx<NX) && (iy+sy>=0) && (iy+sy<NY) && (curcol>=0 && curcol<total_nrow)) {
                    if (!SEVENPOINT || (sz*sz+sy*sy+sx*sx<=1)) {
                        if (curcol==currow) {
                            curval[nnz] = 27.0;
                        }
                        else {
                            curval[nnz] = -1.0;
                        }
                        col[nnz] = curcol;
                        nnz++;
                    }
                }
            } // end sx loop
        } // end sy loop
    } // end sz loop

    *currowlen = nnz;

    return 0;
}
