#ifndef MATFUNCS_PNG_H
#define MATFUNCS_PNG_H

#include <ghost.h>
#include "matfuncs.h"

int matfunc_2_png ( char * png_file_name, ghost_sparsemat_rowfunc matfunc  , int * max_size );

#endif
