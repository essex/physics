#ifndef CHEB_TOOLBOX_H

#include "color_comm_handle.h"

#ifdef CXX_COMPLEX
#include <complex>
#endif
//#define I std::complex<double>(0.0,1.0)

#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288
#endif

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum{
    ChebLoop_DEFAULT      = 0,
    ChebLoop_NAIVE_KERNEL = 1,
    ChebLoop_SINGLEVEC    = 2,
    ChebLoop_REDUCE_OFTEN = 4,
    ChebLoop_DISABLE_CHAIN_AXPBY = 8
}ChebLoop_Options;

typedef struct{
	color_comm_handle  color_comm;
	double eig_down;
	double eig_top;
	double lambda_min;
	double lambda_max;
	double eps;
	int    N_space;
	char * kernel;
	double * kernel_val;
	double  ploy_factor;
	double border_shift;
	int    max_Iter;
	int    save_memory_mode;
	int    final_cut_mode;
	char * file_prefix;
	ghost_spmv_flags globalSpmvmOptions;
	//FILE * file_res_counter;
}cheb_fd_handle;

int cheb_kpm_z( ghost_densemat * x, ghost_sparsemat * mat, double scale,  double shift_d, int M, double * mu , ghost_spmv_flags *globalSpmvmOptions, ChebLoop_Options Loop_Options);
int cheb_kpm_d( ghost_densemat * x, ghost_sparsemat * mat, double scale,  double shift_d, int M, double * mu , ghost_spmv_flags *globalSpmvmOptions, ChebLoop_Options Loop_Options);

int cheb_dos_d( ghost_sparsemat * mat, double scale,  double shift_d, int M, int R, double * mu, ghost_spmv_flags globalSpmvmOptions, ChebLoop_Options Loop_Options);
int cheb_dos_z( ghost_sparsemat * mat, double scale,  double shift_d, int M, int R, double * mu, ghost_spmv_flags globalSpmvmOptions, ChebLoop_Options Loop_Options);


int cheb_mpoly_d(ghost_densemat ** xx, int N, ghost_densemat * x, int M, double * coeff , ghost_sparsemat * mat, double scale,  double shift,  ghost_spmv_flags spMVM_Options);
int cheb_poly_d( ghost_densemat * x, int M, double         * coeff , ghost_sparsemat * mat, double scale,  double shift, double * mu, ghost_spmv_flags spMVM_Options);
#ifdef CXX_COMPLEX
int cheb_poly_z( ghost_densemat * x, int M, std::complex<double> * coeff , ghost_sparsemat * mat, double scale,  double shift, double * mu, ghost_spmv_flags spMVM_Options);
#else
int cheb_poly_z( ghost_densemat * x, int M, complex double * coeff , ghost_sparsemat * mat, double scale,  double shift, double * mu, ghost_spmv_flags spMVM_Options);
#endif
int cheb_tp_z( ghost_densemat * x,                       double dt , ghost_sparsemat * mat, double scale,  double shift, ghost_spmv_flags spMVM_Options, double eps);
int cheb_fd_window_d(ghost_sparsemat * mat, cheb_fd_handle handle );
int cheb_fd_window_z(ghost_sparsemat * mat, cheb_fd_handle handle );


int doubleDotProducts_2_ChebMoments( int M, int N, double * mu );
int floatDotProducts_2_ChebMoments( int M, int N, float * mu );

int cheb_toolbox_kpm_1D_reconstruction_line (int const M, double * const mu, int const N, double * const dos );

int cheb_toolbox_kpm_print_density(char * file_name, int M, int N, double * mu , double scale, double shift, double * info, void gibbs (int const , double * , int const  ) );
int cheb_toolbox_kpm_eval_density_at_engegie(double * dense, double omega, int M, int N, double * mu , double scale, double shift, void gibbs (int const , double * , int const ) );

double cheb_toolbox_get_window_poly_degree(double lambda_min, double lambda_max, double scale, double shift );
double cheb_toolbox_get_window_poly_degree_Ja_degree(double lambda_min, double lambda_max, double scale, double shift  );


double cheb_kernel_width(double x, double const scale, double const shift, int const M  );
void cheb_toolbox_gibbs_Jackson (int  M, double * mu, int  L);
void cheb_toolbox_gibbs_Fejer (int  M, double * mu, int  L);
void cheb_toolbox_gibbs_Lanczos (int const M, double P, double * mu, int  L);
void cheb_toolbox_gibbs_Lorentz (int const M, double const lamb, double * mu, int  L);

#ifdef CXX_COMPLEX
void cheb_toolbox_TP_coeff_single (double const dt, double const a, double const b,  int * M, std::complex<double> * coeff, double const eps );
void cheb_toolbox_TP_coeff_multi (int const N, double const * const t, double const a, double const b,  int * M, std::complex<double> ** const coeff, double eps );
#endif

int cheb_toolbox_delta_comb_coeffs (double lambda_min, double lambda_max, double scale, double shift, double ** coeff, int N_Comb, int rescal_opt );
int cheb_toolbox_window_coeffs (double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha );
int cheb_toolbox_window_coeffs_fix(double lambda_min, double lambda_max, double scale, double shift, double * coeff, int M );

typedef struct {
    ghost_spmv_flags flags;
    ghost_lidx M;
    ghost_lidx vecncols;
    ghost_gidx globalrows;
    ghost_gidx globalnnz;
    ghost_datatype dt;
    bool naiveFlops;
}
kpm_perf_args_t;

int kpm_perf(double *perf, double time, void *varg);
int cheb_poly_perf(double *perf, double time, void *varg);

#ifdef __cplusplus
}
#endif

#define CHEB_TOOLBOX_H
#endif
