
#include "ghost/config.h"
#include "ghost/types.h"
#include "ghost/sparsemat.h"
#include "ghost/spmv.h"
#include "rayleigh_ritz.h"
#include "ghost/util.h"
#include "ghost/tsmttsm.h"
#include "ghost/tsmm.h"


#include "color_comm_handle.h"

#include <cstdlib>

#ifdef PHYSICS_HAVE_LAPACK
#ifdef GHOST_HAVE_MKL
#include <mkl_lapacke.h>
#else 
#include <lapacke.h>
#endif

#include <complex>
#define I std::complex<double>(0.0,1.0)


    template<typename T, typename T_b>
static lapack_int call_geig_function(int matrix_order, char jobz, char uplo, lapack_int n, T *a, lapack_int lda, T *b, lapack_int ldb, T_b *w)
{
    UNUSED(matrix_order);
    UNUSED(jobz);
    UNUSED(uplo);
    UNUSED(n);
    UNUSED(a);
    UNUSED(lda);
    UNUSED(b);
    UNUSED(ldb);
    UNSUED(w);
    GHOST_ERROR_LOG("This should not be called!");
    return -999;
}

    template<>
lapack_int call_geig_function<double,double>(int matrix_order, char jobz, char uplo, lapack_int n, double *a, lapack_int lda, double *b, lapack_int ldb, double *w)
{
    if ( b )
        return LAPACKE_dsygv( matrix_order, 1, jobz, uplo, n, a, lda, b, ldb, w);
    else
        return LAPACKE_dsyev( matrix_order,    jobz, uplo, n, a, lda,         w);
}

    template<>
lapack_int call_geig_function<float,float>(int matrix_order, char jobz, char uplo, lapack_int n, float *a, lapack_int lda, float *b, lapack_int ldb, float *w)
{
    if ( b )
        return LAPACKE_ssygv( matrix_order, 1, jobz, uplo, n, a, lda, b, ldb, w);
    else
        return LAPACKE_ssyev( matrix_order,    jobz, uplo, n, a, lda,         w);

}

    template<>
lapack_int call_geig_function<std::complex<float>,float>(int matrix_order, char jobz, char uplo, lapack_int n, std::complex<float> *a, lapack_int lda, std::complex<float> *b, lapack_int ldb, float *w)
{
    if ( b )
        return LAPACKE_chegv( matrix_order, 1, jobz, uplo, n, (lapack_complex_float *)a, lda, (lapack_complex_float *)b, ldb, w);
    else
        return LAPACKE_cheev( matrix_order,    jobz, uplo, n, (lapack_complex_float *)a, lda,                                 w);

}

    template<>
lapack_int call_geig_function<std::complex<double>,double>(int matrix_order, char jobz, char uplo, lapack_int n, std::complex<double> *a, lapack_int lda, std::complex<double> *b, lapack_int ldb, double *w)
{
    if ( b )
        return LAPACKE_zhegv( matrix_order, 1, jobz, uplo, n, (lapack_complex_double *)a, lda, (lapack_complex_double *)b, ldb, w);
    else
        return LAPACKE_zheev( matrix_order,    jobz, uplo, n, (lapack_complex_double *)a, lda,                                  w);
}

    template <typename T, typename T_b>
static ghost_error ghost_rayleigh_ritz_tmpl (ghost_sparsemat * mat, void * void_eigs, void * void_res,  ghost_densemat * v_eigs , ghost_densemat * v_res, color_comm_handle cch, ghost_rayleighritz_flags RR_Obtion, ghost_spmv_flags spMVM_Options)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_SOLVER);

    //RR_Obtion = (ghost_rayleighritz_flags) (RR_Obtion & (~GHOST_RAYLEIGHRITZ_KAHAN));

    ghost_error ret = GHOST_SUCCESS;
    T one = 1.0;
    T zero = 0.0;
    ghost_lidx i,j,k,l;
    ghost_lidx n = v_res->traits.ncols;
    ghost_lidx N = n*cch.csize;
    ghost_densemat * view_v_eigs, * view_v_res, * remote_v_res;
    int n_block;
    ghost_datatype DT = v_res->traits.datatype;
    ghost_densemat *x = NULL, *b = NULL;
    ghost_densemat *X = NULL, *B = NULL, *X_check;
    ghost_densemat *view_x = NULL;
    T *  xval = NULL;
    T *  bval = NULL;
    T *  Xval = NULL;
    T *  X_check_val = NULL;
    T *  Bval = NULL;
    ghost_lidx ldb, ldB;
    T *eigs_T = NULL, *res_T = NULL;
    ghost_densemat_traits xtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
    ghost_densemat_traits Xtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
    ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
    spmvtraits.flags = spMVM_Options;

    T_b * eigs = (T_b *)void_eigs;
    T_b * res  = (T_b *)void_res;
    T_b * D;

    ghost_densemat ** v_res_all;
#ifdef GHOST_HAVE_MPI
    ghost_mpi_datatype dt, dt_b;
    ghost_mpi_datatype_get(&dt,DT);
    ghost_mpi_datatype_get(&dt_b,(ghost_datatype)(GHOST_DT_REAL | (DT&(GHOST_DT_FLOAT|GHOST_DT_DOUBLE))));
#endif

    int rank;
    double sum,sum1;
    ghost_rank(&rank,v_eigs->map->mpicomm);

    GHOST_CALL_GOTO(ghost_malloc((void **)&v_res_all,  cch.csize*sizeof(ghost_densemat *)),err,ret);


    v_res_all[ cch.color] = v_res;
    if( RR_Obtion & GHOST_RAYLEIGHRITZ_MEM_SAVE ){
        ghost_densemat_create( &remote_v_res, v_res->map,  v_res->traits);
        ghost_densemat_init_val(remote_v_res,&zero);
    } else {
        for(i=0; i<cch.csize; i++){
            if(i != cch.color) {
                ghost_densemat_create( v_res_all  + i, v_res->map,  v_res->traits);
                ghost_densemat_init_val(v_res_all[ i],&zero);
            }
#ifdef GHOST_HAVE_MPI
            if( cch.csize > 1 ) MPI_Bcast( v_res_all[i]->val, 1, v_res_all[i]->fullmpidt, i, cch.mpicomm);
#endif
        }
    }


    xtraits.ncols = n;
    xtraits.storage = GHOST_DENSEMAT_COLMAJOR;
    xtraits.datatype = DT;
    xtraits.location = GHOST_LOCATION_HOST;
    if (v_eigs->traits.location & GHOST_LOCATION_DEVICE) {
        xtraits.location |= GHOST_LOCATION_DEVICE;
    }
    GHOST_CALL_GOTO(ghost_densemat_create(&x,ghost_map_create_light(n,v_eigs->map->mpicomm),xtraits),err,ret);
    GHOST_CALL_GOTO(ghost_densemat_init_val(x,&zero),err,ret);
    xval = (T *)x->val;


    Xtraits.ncols = N;
    Xtraits.storage = GHOST_DENSEMAT_COLMAJOR;
    Xtraits.datatype = DT;
    Xtraits.location = GHOST_LOCATION_HOST;
    GHOST_CALL_GOTO(ghost_densemat_create(&X,ghost_map_create_light(N,v_eigs->map->mpicomm),Xtraits),err,ret);
    GHOST_CALL_GOTO(ghost_densemat_init_val(X,&zero),err,ret);
    Xval = (T *)X->val;
    GHOST_CALL_GOTO(ghost_densemat_create(&X_check,ghost_map_create_light(N,v_eigs->map->mpicomm),Xtraits),err,ret);
    GHOST_CALL_GOTO(ghost_densemat_init_val(X_check,&zero),err,ret);
    X_check_val = (T *)X_check->val;

    GHOST_CALL_GOTO(ghost_malloc((void **)&eigs_T, N*sizeof(T)),err,ret);
    for(i=0; i<N; i++) eigs_T[i] = 0.;
    if( mat ){
        GHOST_CALL_GOTO(ghost_malloc((void **)&res_T, 3*n*sizeof(T)),err,ret);

        if( RR_Obtion & GHOST_RAYLEIGHRITZ_GENERALIZED ){
            GHOST_CALL_GOTO(ghost_densemat_create(&b,ghost_map_create_light(n,v_eigs->map->mpicomm),xtraits),err,ret);
            GHOST_CALL_GOTO(ghost_densemat_init_val(b,&zero),err,ret);
            bval = (T *)b->val;
            ldb = b->stride;
            GHOST_CALL_GOTO(ghost_densemat_create(&B,ghost_map_create_light(N,v_eigs->map->mpicomm),Xtraits),err,ret);
            GHOST_CALL_GOTO(ghost_densemat_init_val(B,&zero),err,ret);
            Bval = (T *)B->val;
            ldB = B->stride;
        }

        n_block = ghost_autogen_spmmv_next_nvecs( n,mat->traits.C );
        if ( !n_block ) n_block = n;

        for ( i=0; i<n; i+=n_block){
            if( i+n_block > n ) n_block = n - i;
            ghost_densemat_create_and_view_densemat_cols( &view_v_eigs, v_eigs, n_block, i);
            ghost_densemat_create_and_view_densemat_cols( &view_v_res ,  v_res , n_block, i);
            ghost_spmv( view_v_eigs, mat, view_v_res, spmvtraits);
            ghost_densemat_destroy(view_v_eigs);
            ghost_densemat_destroy(view_v_res);
        }

        for ( j=0; j<cch.csize; j++){

#ifdef GHOST_HAVE_MPI
            if( RR_Obtion & GHOST_RAYLEIGHRITZ_MEM_SAVE ) {
                if(j != cch.color) {
                    v_res_all[j] = remote_v_res;
                }
                if( cch.csize > 1 ) MPI_Bcast( v_res_all[j]->val, 1, v_res_all[j]->fullmpidt, j, cch.mpicomm);
            }
#endif

            if( RR_Obtion & GHOST_RAYLEIGHRITZ_KAHAN ){
                GHOST_CALL_GOTO(ghost_tsmttsm( x, v_res_all[j], v_eigs, &one,&zero,GHOST_GEMM_ALL_REDUCE, 1,GHOST_GEMM_KAHAN  ),err,ret);
            }else{
                GHOST_CALL_GOTO(ghost_tsmttsm( x, v_res_all[j], v_eigs, &one,&zero,GHOST_GEMM_ALL_REDUCE, 1,GHOST_GEMM_DEFAULT),err,ret);
            }

            ghost_densemat_download(x);
            for ( l=0; l<n; l++) for ( k=0; k<n; k++) Xval[ X->stride *(cch.color*n+l) + j*n+k ] = xval[ x->stride*l + k ];

            if( RR_Obtion & GHOST_RAYLEIGHRITZ_GENERALIZED ){
                if( RR_Obtion & GHOST_RAYLEIGHRITZ_KAHAN ){
                    GHOST_CALL_GOTO(ghost_tsmttsm( b, v_res_all[j], v_res,&one,&zero,GHOST_GEMM_ALL_REDUCE,1,GHOST_GEMM_KAHAN  ),err,ret);
                }else{
                    GHOST_CALL_GOTO(ghost_tsmttsm( b, v_res_all[j], v_res,&one,&zero,GHOST_GEMM_ALL_REDUCE,1,GHOST_GEMM_DEFAULT),err,ret);
                }
                ghost_densemat_download(b);
                for ( l=0; l<n; l++) for ( k=0; k<n; k++) Bval[ B->stride *(cch.color*n+l) + j*n+k ] = bval[ b->stride*l + k ];
            }

        }


        if( RR_Obtion & GHOST_RAYLEIGHRITZ_GENERALIZED ){
#ifdef GHOST_HAVE_MPI
            if( cch.csize > 1 ) 
                for ( i=0; i<cch.csize; i++)
                    MPI_Bcast( Bval + i*n*B->stride , B->stride*n, dt  , i, cch.mpicomm);
#endif
        }
    }else{
        for ( j=0; j<cch.csize; j++){
#ifdef GHOST_HAVE_MPI
            if( RR_Obtion & GHOST_RAYLEIGHRITZ_MEM_SAVE ) {
                if(j != cch.color) {
                    v_res_all[j] = remote_v_res;
                }
                if( cch.csize > 1 ) MPI_Bcast( v_res_all[j]->val, 1, v_res_all[j]->fullmpidt, j, cch.mpicomm);
            }
#endif

            if( RR_Obtion & GHOST_RAYLEIGHRITZ_KAHAN ){
                GHOST_CALL_GOTO(ghost_tsmttsm( x, v_res_all[j], v_res,&one,&zero,GHOST_GEMM_ALL_REDUCE,1,GHOST_GEMM_KAHAN  ),err,ret);
            }else{
                GHOST_CALL_GOTO(ghost_tsmttsm( x, v_res_all[j], v_res,&one,&zero,GHOST_GEMM_ALL_REDUCE,1,GHOST_GEMM_DEFAULT),err,ret);
            }
            ghost_densemat_download(x);
            for ( l=0; l<n; l++) for ( k=0; k<n; k++) Xval[ X->stride *(cch.color*n+l) + j*n+k ] = xval[ x->stride*l + k ];

        }
        GHOST_CALL_GOTO(ghost_malloc((void **)&D,    N*sizeof(T_b)),err,ret);
        GHOST_CALL_GOTO(ghost_malloc((void **)&eigs, N*sizeof(T_b)),err,ret);
    }

    if( RR_Obtion & GHOST_RAYLEIGHRITZ_DEBUGING ) ghost_dot( eigs_T + n*cch.color, v_res, v_res);
#ifdef GHOST_HAVE_MPI
    if( cch.csize > 1 ) 
        for ( i=0; i<cch.csize; i++){
            MPI_Bcast( Xval + i*n*X->stride , X->stride*n, dt  , i, cch.mpicomm);
            if( RR_Obtion & GHOST_RAYLEIGHRITZ_DEBUGING ) MPI_Bcast( eigs_T + i*n , n, dt  , i, cch.mpicomm);
        }
#endif


    if( !cch.color && !rank && ( RR_Obtion & GHOST_RAYLEIGHRITZ_DEBUGING ) ){
        printf("norm dot:     ");
        for ( k=0; k<N; k++) printf(" %.3e", std::real(eigs_T[ k ])); printf("\n\n");
        printf("norm tsmttsmt:");
        T * pval;
        ghost_lidx ld;
        if( !mat ){
            pval = Xval;
            ld   = X->stride;
        }else{
            pval = Bval;
            ld   = B->stride;
        }
        //for ( l=0; l<N; l++) {for ( k=0; k<N; k++) printf(" %.2e", std::real(pval[ ld *l + k ])); printf("\n");}
        //printf("\n");
        for ( k=0; k<N; k++) printf(" %.3e", std::real(pval[ ld *k + k ])); printf("\n\n");
        printf("\n");
    }

    if( !mat ){
        for (i=0;i<N;i++) {
            if( std::real(Xval[i*X->stride+i]) <= 0. ){
                GHOST_ERROR_LOG("zero vector in vectorblock  (color %d, index %d, value %f", cch.color, i, std::real(Xval[i*X->stride+i])  );
                //ret = GHOST_ERR_NOT_IMPLEMENTED;
            }
            D[i] = (T_b)1./std::sqrt(std::real(Xval[i*X->stride+i]));
            for( j=0;j<N;j++) Xval[i*X->stride+j] *= D[i]*D[j];
        }
    }

    if (call_geig_function<T,T_b>( LAPACK_COL_MAJOR, 'V' , 'U', N, Xval, X->stride, Bval, ldB, eigs)) {
        GHOST_ERROR_LOG("LAPACK eigenvalue function failed!");
        ret = GHOST_ERR_LAPACK;
        goto err;
    }

    if( !mat ){
        for ( i=0;i<N;i++){
            if( eigs[i] <= 0. ){
                GHOST_ERROR_LOG("vector block singular");
                ret = GHOST_ERR_NOT_IMPLEMENTED;
            }
            eigs[i] = (T_b)1./std::sqrt(eigs[i]);
            for( j=0;j<N;j++) {
                Xval[i*X->stride+j] *= D[j]*eigs[i];
            }
        }
    }

    ghost_densemat_upload(X);

#ifdef GHOST_HAVE_MPI
    if ( cch.csize > 1 ){
        MPI_Bcast( Xval, X->stride*N, dt  , 0, mat->context->mpicomm_parent);
        MPI_Bcast( eigs,           N, dt_b, 0, mat->context->mpicomm_parent);
    }else{
        MPI_Bcast( Xval, X->stride*N, dt  , 0, mat->context->mpicomm);
        MPI_Bcast( eigs,           N, dt_b, 0, mat->context->mpicomm);
    }
#endif

    GHOST_CALL_GOTO(ghost_densemat_init_val(v_eigs,&zero),err,ret);

    for ( i=0; i<cch.csize; i++) {

#ifdef GHOST_HAVE_MPI
        if( RR_Obtion & GHOST_RAYLEIGHRITZ_MEM_SAVE ) {
            if(i != cch.color) {
                v_res_all[i] = remote_v_res;
            }
            if( cch.csize > 1 ) MPI_Bcast( v_res_all[i]->val, 1, v_res_all[i]->fullmpidt, i, cch.mpicomm);
        }
#endif

        for ( l=0; l<n; l++) for ( k=0; k<n; k++) xval[ x->stride*l + k ] = Xval[ X->stride *(cch.color*n+l) + i*n+k ];
        ghost_densemat_upload(x);
        GHOST_CALL_GOTO(ghost_tsmm( v_eigs, v_res_all[i], x, &one, &one),err,ret);
    }


    if( mat ) {
        for ( i=0;i<n;i++) eigs_T[i] = (T)(eigs[i+cch.color*n]);
        for(i=0;i<N;i++) res[i] = 0;
        if ( RR_Obtion & GHOST_RAYLEIGHRITZ_RESIDUAL ){
            spmvtraits.flags = spmvtraits.flags|GHOST_SPMV_VSHIFT;
            spmvtraits.flags = spmvtraits.flags|GHOST_SPMV_DOT_YY;
            spmvtraits.flags = (ghost_spmv_flags)(spmvtraits.flags & ~GHOST_SPMV_NOT_REDUCE);

            n_block = ghost_autogen_spmmv_next_nvecs( n,mat->traits.C );
            if ( !n_block ) n_block = n;
            for ( i=0; i<n; i+=n_block){
                if( i+n_block > n ) n_block = n - i;
                ghost_densemat_create_and_view_densemat_cols( &view_v_eigs, v_eigs, n_block, i);
                ghost_densemat_create_and_view_densemat_cols( &view_v_res ,  v_res , n_block, i);
                spmvtraits.gamma = eigs_T + i;
                spmvtraits.dot   = res_T  + i;
                ghost_spmv( view_v_res, mat, view_v_eigs, spmvtraits);
                ghost_densemat_destroy(view_v_eigs);
                ghost_densemat_destroy(view_v_res);
            }

            //T two = 2.0;
            //v_res->norm( v_res, res_T, &two);
            //ghost_dot( res_T, v_res, v_res  );
            for(i=0;i<n;i++) res[cch.color*n + i] = std::sqrt(std::real(res_T[i]));
#ifdef GHOST_HAVE_MPI
            if( cch.csize > 1 ){
                for ( i=0; i<cch.csize; i++) MPI_Bcast( res+i*n, n, dt_b, i, cch.mpicomm);
            }
#endif
        }
    }

    goto out;
err:

out: 
    if( !(RR_Obtion & GHOST_RAYLEIGHRITZ_MEM_SAVE) ){
        for(i=0; i<cch.csize; i++)
            if(i != cch.color) ghost_densemat_destroy( v_res_all[ i]);
    }else
        ghost_densemat_destroy( remote_v_res);

    free(v_res_all);

    ghost_densemat_destroy(B);
    ghost_densemat_destroy(x);
    ghost_densemat_destroy(X);
    ghost_densemat_destroy(X_check);
    if( mat ){
        if( RR_Obtion & GHOST_RAYLEIGHRITZ_GENERALIZED )
            ghost_densemat_destroy(b);

        free(res_T);
    }else{
        free(D);
        free(eigs);
    }
    free(eigs_T);
    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_SOLVER);

    return ret;
}


ghost_error ghost_rayleigh_ritz(ghost_sparsemat * mat, void * eigs, void * res,  ghost_densemat * v_eigs , ghost_densemat * v_res, color_comm_handle cch, ghost_rayleighritz_flags RR_Obtion, ghost_spmv_flags spMVM_Options)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH);
    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH);

    if (v_res->traits.datatype & GHOST_DT_COMPLEX) {
        if (v_res->traits.datatype & GHOST_DT_DOUBLE) {
            return ghost_rayleigh_ritz_tmpl<std::complex<double>, double>( mat, eigs, res,  v_eigs , v_res, cch, RR_Obtion, spMVM_Options);
        } else {
            return ghost_rayleigh_ritz_tmpl<std::complex<float>, float>( mat, eigs, res,  v_eigs , v_res, cch, RR_Obtion, spMVM_Options);
        }
    } else {
        if (v_res->traits.datatype & GHOST_DT_DOUBLE) {
            return ghost_rayleigh_ritz_tmpl<double, double>( mat, eigs, res,  v_eigs , v_res, cch, RR_Obtion, spMVM_Options);
        } else {
            return ghost_rayleigh_ritz_tmpl<float, float>( mat, eigs, res,  v_eigs , v_res, cch, RR_Obtion, spMVM_Options);
        }
    }
}

#else
ghost_error ghost_rayleigh_ritz(ghost_sparsemat * mat, void * eigs, void * res,  ghost_densemat * v_eigs , ghost_densemat * v_res, color_comm_handle cch, ghost_rayleighritz_flags RR_Obtion, ghost_spmv_flags spMVM_Options)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH);

    UNUSED(mat);
    UNUSED(eigs);
    UNUSED(res);
    UNUSED(v_eigs);
    UNUSED(v_res);
    UNUSED(cch);
    UNUSED(RR_Obtion);
    UNUSED(spMVM_Options);
    GHOST_ERROR_LOG("LAPACKE not found!");

    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH);
    return GHOST_ERR_NOT_IMPLEMENTED;
}
#endif

