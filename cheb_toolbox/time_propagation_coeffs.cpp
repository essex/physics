#include <math.h>
//#include <complex.h>

#define CXX_COMPLEX
#include "cheb_toolbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <ghost.h>

#define I std::complex<double>(0.0,1.0)

/*
#define ACC 40.0
#define BIGNO 1.0e10
#define BIGNI 1.0e-10
#define BESSEL_EPS_CUT 1.e-17

int my_bessel_Jn(double * const bess_j, int const N, double const x)   //Returns the Bessel function Jn(x) for any real x and n ≥ 2.
{
	if ( N<0 ) return 1;
	int j,jsum,m,n;
	double bj,bjm,bjp,sum,ans;
	double const ax = fabs(x);
	double const tox=2.0/ax;
	int const end = (int)ax + 1;
	if (N >= 0) bess_j[0] = j0(ax);
	if (N >= 1) bess_j[1] = j1(ax);

	if (ax < 1.0e-14)  for (j = 2; j < N; j++) bess_j[j] = 0.;
	else {
		for (j=2;j<N;j++) { bess_j[j]=(j-1)*tox*bess_j[j-1]-bess_j[j-2];  if ( j > end ) break;}    //Upwards recurrence from J0 and J1.
		for (n = j+1; n < N; n++){
			m=2*((n+(int) sqrt(ACC*n))/2);
			jsum=0;			// jsum will alternate between 0 and 1; when it is 1, we accumulate in sum the even terms in
							//   (5.5.16).
			bjp=ans=sum=0.0;
			bj=1.0;
			for (j=m;j>0;j--) {     // The downward recurrence.
				bjm=j*tox*bj-bjp;
				bjp=bj;   bj=bjm;
				if (fabs(bj) > BIGNO) {      //  Renormalize to prevent overflows.
					bj *= BIGNI;  bjp *= BIGNI;  ans *= BIGNI;  sum *= BIGNI;}
				if (jsum) sum += bj;         //Accumulate the sum.
				jsum=!jsum;                  // Change 0 to 1 or vice versa.
				if (j == n) ans=bjp;}        // Save the unnormalized answer.
			sum=2.0*sum-bj;         // Compute (5.5.16)
			bess_j[n] = ans/sum;  // and use it to normalize the answer.
		if ( fabs(bess_j[n]) < BESSEL_EPS_CUT ) break;}
		for (j=n+1;j<N;j++)  bess_j[j] = 0.;
		}
	if ( x < 0. ) for (j = 1; j < N; j +=2) bess_j[j] *= -1.;
return 0;
}
*/

extern "C" void bessel_jn_array_( double *, int *, double * );

void cheb_toolbox_TP_coeff_single (double const dt, double const a, double const b,  int * M, std::complex<double> * coeff, double const eps ){
  int i,j;
  double * Bessel_Tab = (double *)malloc((*M)*sizeof(double));
  double tmp = dt/a;
  //my_bessel_Jn( Bessel_Tab, *M, tmp);
  bessel_jn_array_( Bessel_Tab, M, &tmp);
  j=0; do j++; while ( ((fabs(Bessel_Tab[j]) > eps) ||  (double)j < tmp ) && (j<*M) );
  if( *M == j )   printf("Warning: Genauigkeit nicht erreicht\n");
  *M=j;
  coeff[0] = std::exp(-I*b*dt)*Bessel_Tab[0];
  for (j = 1; j < *M; j++) coeff[j] = 2.0 * std::exp( -I*( b*dt+M_PI*0.5*j )) * Bessel_Tab[j];
  free( Bessel_Tab );
}


void cheb_toolbox_TP_coeff_multi (int const N, double const * const t, double const a, double const b,  int * M, std::complex<double> ** const coeff, double eps ){
  int i,j, M0 = M[0];
  double * Bessel_Tab = (double *)malloc(M0*sizeof(double));
  double tmp;
  for (i = 0; i < N; i++){
               tmp = t[i]/a;
               //my_bessel_Jn( Bessel_Tab, M0, tmp);
               bessel_jn_array_( Bessel_Tab, &M0, &tmp);
               j=0; do j++; while ( ((fabs(Bessel_Tab[j]) > eps) ||  (double)j < tmp ) && (j<M0) );
               M[i]=j;
               coeff[i] = (std::complex<double> *)malloc(M[i]*sizeof(std::complex<double>));
               coeff[i][0] = std::exp(-I*b*t[i])*Bessel_Tab[0];
               for (j = 1; j < M[i]; j++) coeff[i][j] = 2.0 * std::exp( -I*( b*t[i]+M_PI*0.5*j )) * Bessel_Tab[j];
       }
  free( Bessel_Tab );
}

