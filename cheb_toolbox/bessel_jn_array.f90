subroutine bessel_jn_array( j, N, x)
  implicit none
  integer, intent(in)  :: N
  real(8), intent(in)  :: x
  real(8), intent(out) :: j(N)
  
  j = bessel_jn(0,N-1,x)
  
end subroutine bessel_jn_array
