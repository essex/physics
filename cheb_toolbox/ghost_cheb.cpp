
#include "ghost.h"
#include "svqb.h"
#include "rayleigh_ritz.h"
#include "ghost/instr.h"
#include "ghost/func_util.h"
#include "ghost/datatransfers.h"

#include "color_comm_handle.h"
#define CXX_COMPLEX
#include "cheb_toolbox.h"
#define I std::complex<double>(0.0,1.0)


template <typename p_t, typename d_t> 
static int cheb_kpm_tmpl( ghost_densemat * x, ghost_sparsemat * mat, p_t scale,  p_t shift_d, int M, p_t * mu , ghost_spmv_flags *globalSpmvmOptions, ChebLoop_Options Loop_Options){

    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH);
    int n_vecs = x->traits.ncols;

    ghost_densemat * y[2];
    ghost_densemat * tmp_vec;

    y[0] = x;
    ghost_densemat_create(&y[1], ghost_context_max_map(mat->context), x->traits);

    d_t * mu_tmp  = (d_t *)malloc(sizeof(d_t)*2*M*n_vecs);
    d_t * dot_tmp = (d_t *)malloc(sizeof(d_t)*3*n_vecs);

    d_t beta = 0.;
    d_t zero = 0.;
    d_t shift = shift_d;
    d_t mshift = -shift_d;
    d_t scale_spMVM;
    ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
    spmvtraits.alpha = &scale_spMVM;
    spmvtraits.beta = &beta;
    spmvtraits.gamma = &shift;
    spmvtraits.dot = dot_tmp;

    if ( Loop_Options & ChebLoop_NAIVE_KERNEL ) {
        ghost_densemat_create(&tmp_vec, ghost_context_max_map(mat->context), x->traits);
        ghost_densemat_init_val(tmp_vec,&beta);
    } else {
        *globalSpmvmOptions = (ghost_spmv_flags)(*globalSpmvmOptions | GHOST_SPMV_SHIFT|GHOST_SPMV_SCALE|GHOST_SPMV_AXPBY|GHOST_SPMV_DOT_XX|GHOST_SPMV_DOT_XY);
        if (! (Loop_Options & ChebLoop_REDUCE_OFTEN)) {
            *globalSpmvmOptions = (ghost_spmv_flags)(*globalSpmvmOptions | GHOST_SPMV_NOT_REDUCE);
        }
    }
    spmvtraits.flags = *globalSpmvmOptions;

    int r,i,m;

    beta = -0.5;
    ghost_densemat_init_val(y[1],&beta);

    beta = 0.;
    scale_spMVM = scale;

    for(i=0;i<3*n_vecs;i++) {
        dot_tmp[i]=0.;
    }

    ghost_spmv(y[1], mat, y[0], spmvtraits);

    if ( Loop_Options & ChebLoop_NAIVE_KERNEL ) {
        ghost_axpy(y[1], y[0], &mshift);
        ghost_scale(y[1], &scale_spMVM);
        ghost_dot( mu_tmp + 0       ,  y[0], y[0]);
        ghost_dot( mu_tmp + 1*n_vecs,  y[1], y[0]);
    } else {
        for(r=0;r<n_vecs;r++) mu_tmp[0       +r] = dot_tmp[2*n_vecs + r];
        for(r=0;r<n_vecs;r++) mu_tmp[1*n_vecs+r] = dot_tmp[1*n_vecs + r];
    }

    scale_spMVM = 2.*scale;
    beta = -1.;

    double start;
    ghost_timing_wc(&start);

    for (m=1; m<M; m++) {
        GHOST_INSTR_START("kpm_iteration");
        for(i=0;i<3*n_vecs;i++) {
            dot_tmp[i]=0.;
        }

        if (!(Loop_Options & ChebLoop_NAIVE_KERNEL)) {
            tmp_vec = y[(m-1)&1];
        }

        ghost_spmv(tmp_vec, mat, y[m&1], spmvtraits);

        if (Loop_Options & ChebLoop_NAIVE_KERNEL) {
            ghost_axpy(tmp_vec, y[m&1], &mshift);
            ghost_scale( y[(m-1)&1], &beta);
            ghost_axpy(y[(m-1)&1], tmp_vec, &scale_spMVM);
            ghost_dot( mu_tmp + (2*m  )*n_vecs, y[(m  )&1], y[(m  )&1]);
            ghost_dot( mu_tmp + (2*m+1)*n_vecs, y[(m-1)&1], y[(m  )&1]);

        } else {
            for(r=0;r<n_vecs;r++) mu_tmp[(2*m  )*n_vecs+r] = dot_tmp[2*n_vecs + r];
            for(r=0;r<n_vecs;r++) mu_tmp[(2*m+1)*n_vecs+r] = dot_tmp[1*n_vecs + r];
        }
        GHOST_INSTR_STOP("kpm_iteration");
    }


    //for (m=0; m<2*M*n_vecs; m++) mu[m] = creal(mu_tmp[m]);
    for (m=0; m<2*M*n_vecs; m++) mu[m] = std::real(mu_tmp[m]);
#ifdef GHOST_HAVE_MPI
    if (  !(Loop_Options & ChebLoop_NAIVE_KERNEL) && (*globalSpmvmOptions & GHOST_SPMV_NOT_REDUCE) ){
        ghost_mpi_op op;
        ghost_mpi_datatype dt;
        ghost_mpi_op_sum(&op,(ghost_datatype)(GHOST_DT_DOUBLE|GHOST_DT_REAL));
        ghost_mpi_datatype_get(&dt,(ghost_datatype)(GHOST_DT_DOUBLE|GHOST_DT_REAL));
        MPI_Allreduce( MPI_IN_PLACE, mu, 2*M*n_vecs, dt, op, mat->context->mpicomm );
    }
#endif

    doubleDotProducts_2_ChebMoments( M, n_vecs, mu );

#ifdef GHOST_INSTR_TIMING
    ghost_gidx nnz;
    ghost_gidx nrow;
    
    ghost_sparsemat_nnz(&nnz,mat);
    ghost_sparsemat_nrows(&nrow,mat);
    
    kpm_perf_args_t kpm_iter_perfargs;
    kpm_iter_perfargs.vecncols = x->traits.ncols;
    kpm_iter_perfargs.globalnnz = nnz;
    kpm_iter_perfargs.globalrows = nrow;
    kpm_iter_perfargs.dt = x->traits.datatype;
    kpm_iter_perfargs.flags = *globalSpmvmOptions;
    kpm_iter_perfargs.M=1;
    kpm_iter_perfargs.naiveFlops=true;
    ghost_timing_set_perfFunc(__ghost_functag,"kpm_iteration",kpm_perf,(void *)&kpm_iter_perfargs,sizeof(kpm_iter_perfargs),"naive GF/s");

    kpm_perf_args_t kpm_smart_iter_perfargs = kpm_iter_perfargs;
    kpm_smart_iter_perfargs.naiveFlops=false;
    ghost_timing_set_perfFunc(__ghost_functag,"kpm_iteration",kpm_perf,(void *)&kpm_smart_iter_perfargs,sizeof(kpm_smart_iter_perfargs),"smart GF/s");
#endif

    ghost_densemat_destroy(y[1]);
    free(mu_tmp);
    free(dot_tmp);
    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH);
    return 0;
}


int kpm_perf(double *perf, double time, void *varg)
{
    kpm_perf_args_t arg = *(kpm_perf_args_t *)varg;

    ghost_lidx ncol = arg.vecncols;
    double flops;
    ghost_gidx nnz = arg.globalnnz;
    ghost_gidx nrow = arg.globalrows;
    int flopsPerEntry;

    ghost_spmv_nflops(&flopsPerEntry,arg.dt,arg.dt);

    int flopsPerAdd = 1;
    int flopsPerMul = 1;
    int flopsPerConjDot = 2;
    int flopsPerConjDotSame = 2;

    if (arg.dt & GHOST_DT_COMPLEX) {
        flopsPerAdd = 2;
        flopsPerMul = 6;
        flopsPerConjDot = 8;
        flopsPerConjDotSame = 4;
    }

    if (arg.naiveFlops) {
        flops = (double)arg.M*ncol*(nnz*flopsPerEntry+(3.*flopsPerAdd+4.*flopsPerMul+flopsPerConjDot+flopsPerConjDot)*nrow);
    } else {
        flops = (double)arg.M*ncol*(nnz*flopsPerEntry+(3.*flopsPerAdd+4.*flopsPerMul+flopsPerConjDot+flopsPerConjDotSame)*nrow);
    }

    *perf = flops/1.e9/time;

    return 0;

}



template <typename p_t, typename d_t> 
static int cheb_dos_tmpl( ghost_sparsemat * mat, p_t scale,  p_t shift, int M, int R, p_t * mu, ghost_spmv_flags globalSpmvmOptions, ChebLoop_Options Loop_Options){

    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_SOLVER);
    int *nvecs_list, n, n_vecs;
    ghost_autogen_spmmv_nvecs(&nvecs_list,&n,mat->traits.C);
   
    int i; 
    if (!n) {
        n_vecs = R;
    } else {
        for (i=n-1; i>=0; i--) {
            if (nvecs_list[i] <= R) {
                break;
            }
        }
        n_vecs = nvecs_list[i];
    }
    free(nvecs_list);

    int Iter = R/n_vecs ;
    if( Iter*n_vecs != R ) {
       //printf("due to cfg_densemat_dim set R = %d\n", Iter*n_vecs);
       R = Iter*n_vecs;
    }
    
    if ( Loop_Options & ChebLoop_SINGLEVEC ) {
        Iter   = R;
        n_vecs = 1;
    }
    ghost_densemat_traits vtraits   =   GHOST_DENSEMAT_TRAITS_INITIALIZER;
    vtraits.datatype = mat->traits.datatype;
    vtraits.ncols = n_vecs;
    if (Loop_Options & ChebLoop_SINGLEVEC) {
        vtraits.storage = GHOST_DENSEMAT_COLMAJOR;
    } else {
        vtraits.storage = GHOST_DENSEMAT_ROWMAJOR;
    }

    ghost_densemat * x;
    ghost_densemat_create(&x, ghost_context_max_map(mat->context), vtraits);

    p_t * mu_tmp_r= (p_t *)malloc(sizeof(p_t)*2*M*n_vecs);

    int iter,r,m;
       
    for (iter=0;iter<Iter;iter++){
        GHOST_INSTR_START("kpm_full");
        ghost_densemat_init_rand( x );
        cheb_kpm_tmpl<p_t,d_t>( x, mat, scale, shift, M, mu_tmp_r , &globalSpmvmOptions, Loop_Options);
        for (m=0;m<2*M;m++) for(r=0;r<n_vecs;r++)  mu[m] += mu_tmp_r[m*n_vecs + r];
        GHOST_INSTR_STOP("kpm_full");
    }

#ifdef GHOST_INSTR_TIMING
    ghost_gidx nnz;
    ghost_gidx nrow;
    
    ghost_sparsemat_nnz(&nnz,mat);
    ghost_sparsemat_nrows(&nrow,mat);
    
    kpm_perf_args_t kpm_full_perfargs;
    kpm_full_perfargs.vecncols = x->traits.ncols;
    kpm_full_perfargs.globalnnz = nnz;
    kpm_full_perfargs.globalrows = nrow;
    kpm_full_perfargs.dt = x->traits.datatype;
    kpm_full_perfargs.flags = globalSpmvmOptions;
    kpm_full_perfargs.naiveFlops=false;
    kpm_full_perfargs.M=M;
    ghost_timing_set_perfFunc(__ghost_functag,"kpm_full",kpm_perf,(void *)&kpm_full_perfargs,sizeof(kpm_full_perfargs),GHOST_SPMV_PERF_UNIT);
    
/*    ghost_spmv_perf_args_t spmv_perfargs;
    spmv_perfargs.vecncols = x->traits.ncols;
    spmv_perfargs.globalnnz = nnz;
    spmv_perfargs.globalrows = nrow;
    spmv_perfargs.dt = x->traits.datatype;
    spmv_perfargs.flags = globalSpmvmOptions;
    ghost_timing_set_perfFunc(__ghost_functag,GHOST_SPMV_PERF_TAG,ghost_spmv_perf,(void *)&spmv_perfargs,sizeof(spmv_perfargs),GHOST_SPMV_PERF_UNIT);
*/
    /*char *timingSummary;
    ghost_timing_summarystring(&timingSummary);
    int rank;
    ghost_rank(&rank,mat->context->mpicomm);
        if (rank == 0) {
    printf("\nRank %d\n%s\n",rank,timingSummary);
        }
    free(timingSummary); timingSummary = NULL;*/
#endif

#ifdef GHOST_HAVE_TRACK_DATATRANSFERS
    char *datatransferSummary;
    ghost_datatransfer_summarystring(&datatransferSummary);
    //    if (rank == 0) {
    //        printf("\nRank %d\n%s\n",rank,datatransferSummary);
    //    }
    free(datatransferSummary); datatransferSummary = NULL;
#endif
    ghost_densemat_destroy(x);
    free(mu_tmp_r);
    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_SOLVER);
    return 0;
}




template <typename p_t, typename d_t> 
static int cheb_poly_tmpl( ghost_densemat * x, int M, d_t * coeff , ghost_sparsemat * mat, p_t scale,  p_t shift, p_t * mu , ghost_spmv_flags spMVM_Options){

    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_SOLVER);
  ghost_densemat * y[2];
  ghost_densemat *z, *dst, *src;
  int i,b;
  ghost_densemat_traits vtraits = x->traits;
  int n_vecs = x->traits.ncols;
  int block_loop = 1;
  d_t beta = 0.;
  d_t one  = 1.;
  d_t scale_spMVM;
  d_t * dot_tmp = (d_t *)malloc(sizeof(d_t)*6*n_vecs);
  d_t * mu_tmp  = (d_t *)malloc(sizeof(d_t)*4*(M-1)*n_vecs);
  ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
  spmvtraits.alpha = &scale_spMVM;
  spmvtraits.beta = &beta;
  spmvtraits.gamma = &shift;
  spmvtraits.dot = dot_tmp;
  spmvtraits.delta = &one;
    
  if(  mat->traits.opt_blockvec_width  &&  ( n_vecs >= 2*mat->traits.opt_blockvec_width  ) ){
     n_vecs = 2*mat->traits.opt_blockvec_width;
  }
  
    int *nvecs_list, n;
    ghost_autogen_spmmv_nvecs(&nvecs_list,&n,mat->traits.C);

    if (!n) {
        b = 0;
    } else {
        for (i=n-1; i>=0; i--) {
            if (nvecs_list[i] <= n_vecs) {
                break;
            }
        }
        b = nvecs_list[i];
    }
    free(nvecs_list);
  
  
  if(  b  &&  ( n_vecs > b  ) ){
     n_vecs = b;
     vtraits.ncols = n_vecs;
     block_loop = x->traits.ncols / n_vecs;
     if( n_vecs*block_loop != x->traits.ncols ) block_loop++;
     ghost_densemat_create(&z, ghost_context_max_map(mat->context), vtraits);
     ghost_densemat_init_val(z,&beta);
  } else z = x;
  spmvtraits.z = z;
  
  ghost_densemat_create(&y[0], ghost_context_max_map(mat->context), vtraits);
  ghost_densemat_create(&y[1], ghost_context_max_map(mat->context), vtraits);
  ghost_densemat_init_val(y[0],&beta);
  ghost_densemat_init_val(y[1],&beta);
  
  
  
  spmvtraits.flags = (ghost_spmv_flags)(spMVM_Options 
                      | GHOST_SPMV_SHIFT|GHOST_SPMV_SCALE|GHOST_SPMV_AXPBY
                      | GHOST_SPMV_CHAIN_AXPBY
                      | GHOST_SPMV_DOT_XX|GHOST_SPMV_DOT_XY|GHOST_SPMV_NOT_REDUCE);
  
  for(b=0;b<block_loop;b++){
	  beta = 0.;
	  
	  if( z != x ){
		  if( (b == block_loop-1) && ( vtraits.ncols != x->traits.ncols - b*n_vecs)){
			   ghost_densemat_destroy(z);
			   ghost_densemat_destroy(y[0]);
			   ghost_densemat_destroy(y[1]);
			   vtraits.ncols = x->traits.ncols - b*n_vecs;
			   //printf(" recreate vecs with blocksz=%d \n", vtraits.ncols);
			   ghost_densemat_create(&z,    ghost_context_max_map(mat->context), vtraits);
			   ghost_densemat_create(&y[0], ghost_context_max_map(mat->context), vtraits);
			   ghost_densemat_create(&y[1], ghost_context_max_map(mat->context), vtraits);
			   ghost_densemat_init_val(z,&beta);
			   ghost_densemat_init_val(y[0],&beta);
			   ghost_densemat_init_val(y[1],&beta);
		   }
		  ghost_densemat_create_and_view_densemat_cols( &dst, z, vtraits.ncols,        0);
		  ghost_densemat_create_and_view_densemat_cols( &src, x, vtraits.ncols, b*n_vecs);
		  ghost_densemat_init_densemat( z, src, 0, 0);
		  
	  }
  
  ghost_densemat_init_densemat( y[0], z, 0, 0);
  ghost_scale(z, &coeff[0] );

  beta = 0.;
  scale_spMVM = scale;

  GHOST_INSTR_START("cheb_poly_full");
  for(i=0;i<3*vtraits.ncols;i++) dot_tmp[i]=0.;
  spmvtraits.eta = &coeff[1];
  ghost_spmv(y[1], mat, y[0], spmvtraits);
  for(i=0;i<n_vecs;i++) mu_tmp[0               + i] = dot_tmp[2*vtraits.ncols + i];
  for(i=0;i<n_vecs;i++) mu_tmp[1*vtraits.ncols + i] = dot_tmp[1*vtraits.ncols + i];
  if( !( spmvtraits.flags & GHOST_SPMV_CHAIN_AXPBY ) )
     ghost_axpy( z, y[1], spmvtraits.eta );
  scale_spMVM = 2.*scale;
  beta = -1.;
  int m;
  for (m=2;m<M;m++){
      for(i=0;i<3*vtraits.ncols;i++) dot_tmp[i]=0.;
      spmvtraits.eta = &coeff[m];
      ghost_spmv(y[m&1], mat, y[(m-1)&1], spmvtraits);
      for(i=0;i<n_vecs;i++) mu_tmp[(2*m-2)*vtraits.ncols+i] = dot_tmp[2*vtraits.ncols + i];
      for(i=0;i<n_vecs;i++) mu_tmp[(2*m-1)*vtraits.ncols+i] = dot_tmp[1*vtraits.ncols + i];
      if( !( spmvtraits.flags & GHOST_SPMV_CHAIN_AXPBY ) )
         ghost_axpy( z, y[m&1], spmvtraits.eta );
   }
   GHOST_INSTR_STOP("cheb_poly_full");
   
if( mu != NULL ){
	for (i=0;i<vtraits.ncols;i++){
       for (m=0; m<2*(M-1); m++) mu[m*x->traits.ncols + b*n_vecs + i] = std::real(mu_tmp[m*vtraits.ncols + i]);
       //for (m=0; m<2*(M-1); m++) mu[m*x->traits.ncols + b*n_vecs + i] = creal(mu_tmp[m*vtraits.ncols + i]);
       //for (m=0; m<2*M*n_vecs; m++) mu[m] = std::real(mu_tmp[m]);
         }}
  //if( n_vecs != x->traits.ncols ){
  if( z != x ){
	  ghost_densemat_init_densemat( src, dst, 0, 0);
	  ghost_densemat_destroy(dst);
	  ghost_densemat_destroy(src);
	}
}

if( mu != NULL ){
#ifdef GHOST_HAVE_MPI
        ghost_mpi_op op;
        ghost_mpi_datatype dt;
        ghost_mpi_op_sum(&op,(ghost_datatype)(GHOST_DT_DOUBLE|GHOST_DT_REAL));
        ghost_mpi_datatype_get(&dt,(ghost_datatype)(GHOST_DT_DOUBLE|GHOST_DT_REAL));
        MPI_Allreduce( MPI_IN_PLACE, mu, 2*(M-1)*x->traits.ncols, dt, op, mat->context->mpicomm);
#endif
    doubleDotProducts_2_ChebMoments( (M-1), x->traits.ncols, mu );
  }
  

#ifdef GHOST_INSTR_TIMING
    ghost_gidx nnz;
    ghost_gidx nrow;
    
    ghost_sparsemat_nnz(&nnz,mat);
    ghost_sparsemat_nrows(&nrow,mat);
    
    kpm_perf_args_t kpm_iter_perfargs;
    kpm_iter_perfargs.vecncols = x->traits.ncols;
    kpm_iter_perfargs.globalnnz = nnz;
    kpm_iter_perfargs.globalrows = nrow;
    kpm_iter_perfargs.dt = x->traits.datatype;
    kpm_iter_perfargs.flags = spMVM_Options;
    kpm_iter_perfargs.M=M-1;
    kpm_iter_perfargs.naiveFlops = 0;
    
    ghost_timing_set_perfFunc(__ghost_functag,"cheb_poly_full",cheb_poly_perf,(void *)&kpm_iter_perfargs,sizeof(kpm_iter_perfargs),GHOST_SPMV_PERF_UNIT);
    
/*    ghost_spmv_perf_args_t spmv_perfargs;
    spmv_perfargs.vecncols = x->traits.ncols;
    spmv_perfargs.globalnnz = nnz;
    spmv_perfargs.globalrows = nrow;
    spmv_perfargs.dt = x->traits.datatype;
    spmv_perfargs.flags = spMVM_Options;
    ghost_timing_set_perfFunc(__ghost_functag,GHOST_SPMV_PERF_TAG,ghost_spmv_perf,(void *)&spmv_perfargs,sizeof(spmv_perfargs),GHOST_SPMV_PERF_UNIT);
*/
    /*char *timingSummary;
    ghost_timing_summarystring(&timingSummary);
    int rank;
    ghost_rank(&rank,mat->context->mpicomm);
        if (rank == 0) {
    printf("\nRank %d\n%s\n",rank,timingSummary);
        }
    free(timingSummary); timingSummary = NULL;*/
#endif


  //if( n_vecs != x->traits.ncols ){
  if( z != x ){
	   ghost_densemat_destroy(z);
  }
  free(dot_tmp);
  free(mu_tmp);
  ghost_densemat_destroy(y[0]);
  ghost_densemat_destroy(y[1]);
    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_SOLVER);
  return 0;

}


int cheb_poly_perf(double *perf, double time, void *varg)
{
    kpm_perf_args_t arg = *(kpm_perf_args_t *)varg;

    ghost_lidx ncol = arg.vecncols;
    double flops;
    ghost_gidx nnz = arg.globalnnz;
    ghost_gidx nrow = arg.globalrows;
    int flopsPerEntry;

    ghost_spmv_nflops(&flopsPerEntry,arg.dt,arg.dt);

    int flopsPerAdd = 1;
    int flopsPerMul = 1;
    int flopsPerConjDot = 2;
    int flopsPerConjDotSame = 2;

    if (arg.dt & GHOST_DT_COMPLEX) {
        flopsPerAdd = 2;
        flopsPerMul = 6;
        flopsPerConjDot = 8;
        flopsPerConjDotSame = 4;
    }

    if (arg.naiveFlops) {
        flops = (double)arg.M*ncol*(nnz*flopsPerEntry+(3.*flopsPerAdd+5.*flopsPerMul+flopsPerConjDot+flopsPerConjDot)*nrow);
    } else {
        flops = (double)arg.M*ncol*(nnz*flopsPerEntry+(3.*flopsPerAdd+5.*flopsPerMul+flopsPerConjDot+flopsPerConjDotSame)*nrow);
    }

    *perf = flops/1.e9/time;

    return 0;

}

int double_compar(  const void *a, const void *b ){ 
	if ( *(double *)a > *(double *)b ) return 1;
	return -1;
	}

int int_compar(  const void *a, const void *b ){ 
	// using: qsort ( val, n, sizeof(int) , int_compar);
	if ( *(int *)a > *(int *)b ) return 1;
	return -1;
	}

void double_qsort(int * index, double * vals ,  int n ){
	
	size_t size_d = sizeof(double);
	size_t size_i = sizeof(int);
	size_t size   = size_d + size_i;
	char * base   = (char *)malloc( n*size );
    if (!base) {
        printf("abort -> malloc failed!\n");
        exit(0);
    }
	int i;
	for(i=0;i<n;i++) {
		memcpy( base + i*size,          vals  + i , size_d);
		memcpy( base + i*size + size_d, index + i , size_i);}
	
	qsort ( base, n, size , double_compar);
	
	for(i=0;i<n;i++) {
		memcpy( vals  + i, base + i*size         , size_d);
		memcpy( index + i, base + i*size + size_d, size_i);}
	
	free(base);
}




ghost_lidx sort_ritz_vec( ghost_lidx * list_ritz_num, ghost_lidx ** list_ritz,
			int N, double * eig,  double * res, double lambda_min, double lambda_max, double eps){
	
	int i,j;
	double * res_g[4];
	
	for(j=0;j<4;j++){
		list_ritz_num[j]=0;
		res_g[j] = (double *)malloc(sizeof(double)*N);
	}
	
	ghost_lidx break_cond = 0;
	
	for(i=0;i<N;i++){
		if( (res[i]<eps) && ( lambda_min>eig[i] ) )  break_cond |= 1;
		if( (res[i]<eps) && ( lambda_max<eig[i] ) )  break_cond |= 2;
		
		
		if( (lambda_min-eps>eig[i]) ){
				list_ritz[2][list_ritz_num[2]] =     i ;
				res_g[2][list_ritz_num[2]] = res[i];
				list_ritz_num[2]++;
			}
		else if(eig[i]>lambda_max+eps){
				list_ritz[3][list_ritz_num[3]] =     i ;
				res_g[3][list_ritz_num[3]] = res[i];
				list_ritz_num[3]++;
			}
		else //( (lambda_min-eps<eig[i]) && (eig[i]<lambda_max+eps) )
			{
				
				list_ritz[0][list_ritz_num[0]] =     i ;
				res_g[0][list_ritz_num[0]] = res[i];
				list_ritz_num[0]++;
				if( res[i]<  0.5*(lambda_max-lambda_min) ){
					list_ritz[1][list_ritz_num[1]] =     i ;
					res_g[1][list_ritz_num[1]] = res[i];
					list_ritz_num[1]++;
				}
			}
	}
	
	for(j=0;j<4;j++){
		double_qsort(list_ritz[j], res_g[j],  list_ritz_num[j] );
		free(res_g[j]);
	}
	
	return break_cond;
}
static void copy_vecs ( ghost_densemat * dst, ghost_densemat * src, ghost_lidx nvecs , ghost_lidx *dst_idxs , ghost_lidx *src_idxs )
{
	ghost_densemat * dst_view, * src_view;
	
	ghost_densemat_create_and_view_densemat_cols_scattered( &dst_view, dst, nvecs, dst_idxs);
	ghost_densemat_create_and_view_densemat_cols_scattered( &src_view, src, nvecs, src_idxs);
	
	ghost_densemat_init_densemat( dst_view, src_view, 0, 0);
	
	ghost_densemat_destroy(dst_view);
	ghost_densemat_destroy(src_view);
}

template <typename p_t, typename d_t> 
static int cheb_fd_window_tmpl(ghost_sparsemat * mat, cheb_fd_handle handle  ){
	
	int rank,i,m,spMVM_counter=0;
	ghost_rank(&rank,mat->context->mpicomm);
	FILE * file_timing;
	double scale =  2./(handle.eig_top - handle.eig_down);
	double shift = 0.5*(handle.eig_top + handle.eig_down);
	
	double * window_coeff_d;
	char   file_name[256];
	//int M = (int)(ploy_factor*cheb_toolbox_get_window_poly_degree_Ja_degree(lambda_min ,lambda_max ,scale , shift ));
	int M = (int)(handle.ploy_factor*cheb_toolbox_get_window_poly_degree(handle.lambda_min ,handle.lambda_max ,scale , shift ));
	double delta_l_border = handle.lambda_min - handle.border_shift*cheb_kernel_width( handle.lambda_min , scale, shift, M);
	double delta_r_border = handle.lambda_max + handle.border_shift*cheb_kernel_width( handle.lambda_max , scale, shift, M);
	
	window_coeff_d = (double *)malloc(sizeof(double)*M);
	cheb_toolbox_window_coeffs_fix( delta_l_border, delta_r_border, scale, shift, window_coeff_d, M);
	
	/*
	if(!rank) {
		sprintf(file_name, "%s_timing.dat", file_prefix);
		file_timing = fopen(file_name, "w");
		printf("#########################################################\n");
		printf("  eigrange=[%g,%g]\n   rescaling parameters\n     scale = %g\n     shift = %g\n\n",eig_down,eig_top,scale,shift);
		printf("  searchrange=[%g,%g]\n   rescaled=[%g,%g] \n\n",lambda_min,lambda_max, scale*(lambda_min-shift), scale*(lambda_max-shift) );
		}
	*/
	
	if( !handle.color_comm.color )
	    printf( " rang %d, Matrix size:\n      local:  %.3f GB\n      remote %.3f GB\n      global: %.3f GB\n",
	    handle.color_comm.prank, ghost_sparsemat_bytesize(mat->localPart)*1./(1024*1024*1024), ghost_sparsemat_bytesize(mat->remotePart)*1./(1024*1024*1024), ghost_sparsemat_bytesize(mat)*1./(1024*1024*1024));
	
	
	
	if( !handle.color_comm.prank ){
		printf("   Poly Degree = %d   ( ploy_factor = %g )\n", M, handle.ploy_factor );
		printf("    filterrange=[%g,%g]\n   rescaled=[%g,%g] \n\n",delta_l_border,delta_r_border, scale*(delta_l_border-shift), scale*(delta_r_border-shift) );
		}
	
	if ( !strcmp( handle.kernel, "Ja" ) ){
		cheb_toolbox_gibbs_Jackson ( M, window_coeff_d, 1);
		if(!rank && !handle.color_comm.color)   printf("   using Jackson Kernel\n");
		}
	else if ( !strcmp( handle.kernel, "Fe" ) ){
		cheb_toolbox_gibbs_Fejer ( M, window_coeff_d, 1);
		if(!rank && !handle.color_comm.color)   printf("   using Fejer Kernel\n");
		}
	else if ( !strcmp( handle.kernel, "La" ) ){
		cheb_toolbox_gibbs_Lanczos ( M, handle.kernel_val[0], window_coeff_d, 1);
		if(!rank && !handle.color_comm.color)   printf("   using Lanczos-%g Kernel\n", handle.kernel_val[0]);
		}
	else if ( !strcmp( handle.kernel, "Lo" ) ){
		cheb_toolbox_gibbs_Lorentz ( M, handle.kernel_val[0], window_coeff_d, 1);
		if(!rank && !handle.color_comm.color)   printf("   using Lorentz-%g Kernel\n", handle.kernel_val[0]);
		}
	
	if( !handle.color_comm.prank ){
		printf("   searchspace = %d \n", handle.N_space );
		printf("     cheb_poly sub_block = %d \n\n", mat->traits.opt_blockvec_width  );
		printf("   output_data with prefix: %s\n\n", handle.file_prefix);
		printf("#############################################\n\n");
		}
	
	int n_space = handle.N_space / handle.color_comm.csize;
	
	if( handle.N_space != n_space * handle.color_comm.csize ){
		if( !handle.color_comm.prank ) printf( "abort: searchspace do not supportded for this konfiguration\n");
		free(window_coeff_d);
		return 1;
	}
	
	ghost_rayleighritz_flags sqvb_flags = GHOST_RAYLEIGHRITZ_DEFAULT;
	//sqvb_flags = (ghost_rayleighritz_flags)(sqvb_flags | GHOST_RAYLEIGHRITZ_DEBUGING );
	//sqvb_flags = (ghost_rayleighritz_flags)(sqvb_flags | GHOST_RAYLEIGHRITZ_KAHAN    );
	sqvb_flags = (ghost_rayleighritz_flags)(sqvb_flags | GHOST_RAYLEIGHRITZ_MEM_SAVE );
	
	ghost_rayleighritz_flags grr_flags = (ghost_rayleighritz_flags)(sqvb_flags | GHOST_RAYLEIGHRITZ_GENERALIZED|GHOST_RAYLEIGHRITZ_RESIDUAL);
	
	d_t * window_coeff = (d_t *)malloc(sizeof(d_t)*M);
	for(i=0;i<M;i++) window_coeff[i] = (d_t)(window_coeff_d[i]);
	
	double * eigs = (double *)malloc(handle.N_space*sizeof(double));
	double * res  = (double *)malloc(handle.N_space*sizeof(double));
	//double complex two = 2.;
	d_t * InvPN = (d_t *)malloc(sizeof(d_t)*handle.N_space);
	
	ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
	vtraits.datatype = mat->traits.datatype;
	vtraits.storage = GHOST_DENSEMAT_ROWMAJOR;
	vtraits.ncols = n_space;
	
	
	ghost_lidx * list_ritz[4];
	list_ritz[0] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[1] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[2] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[3] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	ghost_lidx list_ritz_num[4];
	list_ritz_num[0] = 0;
	list_ritz_num[1] = handle.N_space;
	list_ritz_num[2] = 0;
	list_ritz_num[3] = 0;
	for (m=0;m<handle.N_space;m++) list_ritz[1][m]=m;
	
	double * mu_vecs  = (double*)malloc(sizeof(double)*2*(M-1)*n_space);
	
	ghost_densemat *v, *w, *t;
	
	ghost_densemat_create(&v, ghost_context_max_map(mat->context), vtraits);
	ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
	d_t zero = 0.0;
	ghost_densemat_init_val(w,&zero);
	
	if( !handle.color_comm.color )
		//printf( "  rang %d, DenseMatrix(%d) size %.3f (%g %%) GB\n",handle.color_comm.prank, n_space,  w->traits.ncolspadded*w->traits.nrowshalopadded*w->elSize*1.e-9,  w->traits.ncolspadded*w->traits.nrowshalopadded*100./(w->traits.nrows*w->traits.ncols)   );
	
	//ghost_rand_seed(GHOST_RAND_SEED_TIME,5748+4685*handle.color_comm.color);
	ghost_rand_seed(GHOST_RAND_SEED_TIME,1424);
	//ghost_rand_seed( GHOST_RAND_SEED_NONE, 0);
	
	ghost_densemat_init_rand( v );
	
	if( !handle.color_comm.prank ) printf("Status:  Pre SVQB ..\n");
	ghost_rayleigh_ritz( NULL, NULL, NULL, w, v, handle.color_comm, sqvb_flags, GHOST_SPMV_DEFAULT);
	t=v; v=w; w=t;
	
	
	if( !handle.color_comm.prank ) printf("Status: Iteration ..\n");
	ghost_lidx break_cond = 0;
	int mm;
	for (mm=0;mm<handle.max_Iter;mm++){
		
		if (handle.save_memory_mode) ghost_densemat_destroy(w);
			cheb_poly_tmpl( v, M, window_coeff ,  mat, scale,   shift, mu_vecs, handle.globalSpmvmOptions);
			spMVM_counter += (M-1)*vtraits.ncols;
		if (handle.save_memory_mode) {  ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
		                         ghost_densemat_init_val(w,&zero);  }
		
		//if(!rank && !handle.color_comm.color){ 
		   /* int N_class=4;
	        
		    int M2=2*(M-1);
	        double * mu     = (double*)malloc(sizeof(double)*M2);
	        double * spc = (double *)malloc(sizeof(double)*M2*N_class);
	        
	        int n,b;
	        for (n=0;n<N_class;n++){
	          for(m=0;m<M2;m++) mu[m]=0.;
	          
	          for(m=0;m<M2;m++)for(b=0;b<list_ritz_num[n];b++)
	             if( (handle.color_comm.color   *vtraits.ncols  <= list_ritz[n][b]) &&
	                 (handle.color_comm.color+1)*vtraits.ncols  >  list_ritz[n][b]  ) 
	                    mu[m] += mu_vecs[m*vtraits.ncols+(list_ritz[n][b]-handle.color_comm.color*vtraits.ncols) ];
	          
	          cheb_toolbox_gibbs_Jackson (M2, mu, 1);
	          cheb_toolbox_kpm_1D_reconstruction_line (M2, mu, M2, spc + n*M2 );
		  }
		  
		  sprintf(file_name, "%s_Class_dense_%02d.dat", handle.file_prefix, mm);
		  FILE * out = fopen(file_name, "w");
	      double E = 1./scale; double scal_spc = 0.5/E;
          for(m=0;m<M2;m++)    {      fprintf(out,"%g", E*(2.*(m+0.5)/M2-1.) + shift);
			 for (n=0;n<N_class;n++)  fprintf(out,"\t%g", scal_spc*M2*spc[n*M2 + m]); 
						              fprintf(out,"\n");
	         }
	         
	        free(spc);fclose(out); */
	        /*
	        spc = (double *)malloc(sizeof(double)*M2*vtraits.ncols);
	        for (n=0;n<vtraits.ncols;n++){
	          for(m=0;m<M2;m++) mu[m] = mu_vecs[m*vtraits.ncols+n];
	          cheb_toolbox_gibbs_Jackson (M2, mu, 1);
	          cheb_toolbox_kpm_1D_reconstruction_line (M2, mu, M2, spc + n*M2 );
	         }        
	        
	        sprintf(file_name, "%s_All_dense_%02d.dat", file_prefix, mm);
	        out = fopen(file_name, "w");
	        for(m=0;m<M2;m++)    {          fprintf(out,"%g", E*(2.*(m+0.5)/M2-1.) + shift);
			 for (n=0;n<vtraits.ncols;n++)  fprintf(out,"\t%g", scal_spc*M2*spc[n*M2 + m]); 
                                            fprintf(out,"\n");
	         }
	        fclose(out);
	        free(spc);free(mu);
            */
		//}
		
		
		if( (break_cond & 1) && (break_cond & 2) ) break_cond |=4;
		/*if( (break_cond & 1) && (break_cond & 2) && handle.final_cut_mode ){
			ghost_lidx svd_offset;
			
			ghost_svd_deflation( &svd_offset, w, v, 0.1f);
			
			ghost_lidx * copy_ints = (ghost_lidx*)malloc(vtraits.ncols*handle.color_comm.csize*sizeof(ghost_lidx));
			for ( i = 0; i<vtraits.ncols*handle.color_comm.csize; i++ ) copy_ints[i] = i;
			
			vtraits.ncols -= svd_offset;
			
			ghost_densemat_destroy(v);
			ghost_densemat_create(&v, mat->context, vtraits);
			ghost_densemat_init_val(v,&zero);
			
			copy_vecs ( v, w, vtraits.ncols, copy_ints, copy_ints+svd_offset);
			
			ghost_densemat_destroy(w);
			ghost_densemat_create(&w, mat->context, vtraits);
			ghost_densemat_init_val(w,&zero);
			
			free(copy_ints);
			
			t=v; v=w; w=t;
			
		}else{*/	
			t=v; v=w; w=t;
		//	 }
		ghost_rayleigh_ritz( mat, eigs, res, v,  w,  handle.color_comm, grr_flags, handle.globalSpmvmOptions); 
		
		spMVM_counter += 2*vtraits.ncols;
		
		
		//for (i=0;i<vtraits.ncols*handle.color_comm.csize;i++) InvPN[i] = 0.;
		//v->norm(v,InvPN,&two);
		
		if( !handle.color_comm.prank ){
			//for (i=0;i<vtraits.ncols*handle.color_comm.csize;i++) printf("Num_%03d  %+.14e +/- %g  IPN: %g\n", i, eigs[i],res[i],std::real(InvPN[i]));
			for (i=0;i<handle.N_space;i++) printf("Num_%03d  %+.14e +/- %g \n", i, eigs[i],res[i] );
			printf("\n");
		}
		
		
		break_cond |= sort_ritz_vec( list_ritz_num, list_ritz, handle.N_space, eigs,  res, handle.lambda_min, handle.lambda_max, handle.eps);
#ifdef GHOST_HAVE_MPI 
    MPI_Bcast( &break_cond, 1, ghost_mpi_dt_lidx , 0, handle.color_comm.mpicomm_parent);
#endif
		if( !handle.color_comm.prank ){ printf("ITER  %d\n",mm);
			     printf("  %d  %d  %d  %d\n", list_ritz_num[0], list_ritz_num[1], list_ritz_num[2], list_ritz_num[3]  );
			     
			     for(m=0;m<4;m++){
			         printf("L%d: ",m);
			         if(list_ritz_num[m]) printf(" res = [%g : %g]", res[list_ritz[m][0]] , res[list_ritz[m][list_ritz_num[m]-1]] );
			         printf("\n  ");
			         for(i=0;i<list_ritz_num[m];i++) printf(" %d", list_ritz[m][i]);
			         printf("\n");
			      }
			
			double res_min=1., res_max=1., res_min_lr=1., res_min_l=1., res_min_r=1.;
			if(list_ritz_num[1]){ res_min = res[list_ritz[1][0]];
			                      res_max = res[list_ritz[1][list_ritz_num[1]-1]];}
			if(list_ritz_num[2])  res_min_l = res[list_ritz[2][0]];
			if(list_ritz_num[3])  res_min_r = res[list_ritz[3][0]];
			if ( res_min_l > res_min_r) res_min_lr = res_min_l; else res_min_lr = res_min_r;
			
			//if(file_res_counter)  fprintf(file_res_counter,"%d\t%d\t%ld\t%g\t%g\t%g\t%g\t%g\n", vtraits.ncols, M-1, spMVM_counter, res_min , res_max, res_min_lr, res_min_l, res_min_r);
		}
		
		/*
		if( (break_cond & 1) && (break_cond & 2) && handle.final_cut_mode ){
			
			vtraits.ncols = list_ritz_num[0];
			ghost_lidx * copy_ints = (ghost_lidx*)malloc(vtraits.ncols*sizeof(ghost_lidx));

			ghost_densemat_destroy(w);
			ghost_densemat_create(&w, mat->context, vtraits);
			ghost_densemat_init_val(w,&zero);
			
			qsort ( list_ritz[0], vtraits.ncols, sizeof(int) , int_compar);
			for ( i = 0; i<vtraits.ncols; i++ ) copy_ints[i] = i;
			copy_vecs ( w, v, vtraits.ncols, copy_ints, list_ritz[0]);
			
			ghost_densemat_destroy(v);
			ghost_densemat_create(&v, mat->context, vtraits);
			ghost_densemat_init_val(v,&zero);
			
			free(copy_ints);
			
			t=v; v=w; w=t;
		}
		
		if( (break_cond & 1) && (break_cond & 2) && !handle.final_cut_mode ) break;*/
		if( break_cond & 4 ) break;
	}
	
	//qsort ( list_ritz[1], list_ritz_num[1], sizeof(int) , int_compar);
	
	
	free(eigs);
	free(res);
	free(mu_vecs);
	free(InvPN);
	
	free(window_coeff);
	free(window_coeff_d);
	free(list_ritz[0]);
	free(list_ritz[1]);
	free(list_ritz[2]);
	free(list_ritz[3]);
	ghost_densemat_destroy(w);
	ghost_densemat_destroy(v);
	return 0;
}


template <typename p_t, typename d_t> 
static int cheb_fd_delta_tmpl(ghost_sparsemat * mat, cheb_fd_handle handle  ){
	
	int rank,i,m,spMVM_counter=0;
	ghost_rank(&rank,mat->context->mpicomm);
	FILE * file_timing;
	double scale =  2./(handle.eig_top - handle.eig_down);
	double shift = 0.5*(handle.eig_top + handle.eig_down);
	
	double * window_coeff_d;
	char   file_name[256];
	//int M = (int)(handle.ploy_factor*cheb_toolbox_get_window_poly_degree(handle.lambda_min ,handle.lambda_max ,scale , shift ));
	//double delta_l_border = handle.lambda_min - handle.border_shift*cheb_kernel_width( handle.lambda_min , scale, shift, M);
	//double delta_r_border = handle.lambda_max + handle.border_shift*cheb_kernel_width( handle.lambda_max , scale, shift, M);
	
	//window_coeff_d = (double *)malloc(sizeof(double)*M);
	//cheb_toolbox_window_coeffs_fix( delta_l_border, delta_r_border, scale, shift, window_coeff_d, M);
	int M = cheb_toolbox_delta_comb_coeffs(handle.lambda_min, handle.lambda_max, scale,  shift, &window_coeff_d, 1, 1 );
	
	/*
	if(!rank) {
		sprintf(file_name, "%s_timing.dat", file_prefix);
		file_timing = fopen(file_name, "w");
		printf("#########################################################\n");
		printf("  eigrange=[%g,%g]\n   rescaling parameters\n     scale = %g\n     shift = %g\n\n",eig_down,eig_top,scale,shift);
		printf("  searchrange=[%g,%g]\n   rescaled=[%g,%g] \n\n",lambda_min,lambda_max, scale*(lambda_min-shift), scale*(lambda_max-shift) );
		}
	*/
	
	if(!rank){
		printf("   Poly Degree = %d   ( ploy_factor = %g )\n", M, handle.ploy_factor );
		//printf("    filterrange=[%g,%g]\n   rescaled=[%g,%g] \n\n",delta_l_border,delta_r_border, scale*(delta_l_border-shift), scale*(delta_r_border-shift) );
		printf("    filterrange=[%g,%g]\n   rescaled=[%g,%g] \n\n",handle.lambda_min,handle.lambda_max, scale*(handle.lambda_min-shift), scale*(handle.lambda_max-shift) );
		}
	
	if ( !strcmp( handle.kernel, "Ja" ) ){
		cheb_toolbox_gibbs_Jackson ( M, window_coeff_d, 1);
		if(!rank)   printf("   using Jackson Kernel\n");
		}
	else if ( !strcmp( handle.kernel, "Fe" ) ){
		cheb_toolbox_gibbs_Fejer ( M, window_coeff_d, 1);
		if(!rank)   printf("   using Fejer Kernel\n");
		}
	else if ( !strcmp( handle.kernel, "La" ) ){
		cheb_toolbox_gibbs_Lanczos ( M, handle.kernel_val[0], window_coeff_d, 1);
		if(!rank)   printf("   using Lanczos-%g Kernel\n", handle.kernel_val[0]);
		}
	else if ( !strcmp( handle.kernel, "Lo" ) ){
		cheb_toolbox_gibbs_Lorentz ( M, handle.kernel_val[0], window_coeff_d, 1);
		if(!rank)   printf("   using Lorentz-%g Kernel\n", handle.kernel_val[0]);
		}
	
	if(!rank){
		printf("   searchspace = %d \n", handle.N_space );
		printf("     cheb_poly sub_block = %d \n\n", mat->traits.opt_blockvec_width  );
		printf("   output_data with prefix: %s\n\n", handle.file_prefix);
		printf("#############################################\n\n");
		}	
	
	
	d_t * window_coeff = (d_t *)malloc(sizeof(d_t)*M);
	for(i=0;i<M;i++) window_coeff[i] = (d_t)(window_coeff_d[i]);
	
	double * eigs = (double *)malloc(handle.N_space*sizeof(double));
	double * res  = (double *)malloc(handle.N_space*sizeof(double));
	//double complex two = 2.;
	d_t * InvPN = (d_t *)malloc(sizeof(d_t)*handle.N_space);
	
	ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
	vtraits.datatype = mat->traits.datatype;
	vtraits.storage = GHOST_DENSEMAT_ROWMAJOR;
	vtraits.ncols = handle.N_space;
	
	ghost_densemat *v, *w, *t;
	
	ghost_densemat_create(&v, ghost_context_max_map(mat->context), vtraits);
	ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
	
	
	ghost_lidx * list_ritz[4];
	list_ritz[0] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[1] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[2] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	list_ritz[3] = (ghost_lidx*)malloc(sizeof(ghost_lidx)*handle.N_space);
	ghost_lidx list_ritz_num[4];
	list_ritz_num[0] = 0;
	list_ritz_num[1] = handle.N_space;
	list_ritz_num[2] = 0;
	list_ritz_num[3] = 0;
	for (m=0;m<handle.N_space;m++) list_ritz[1][m]=m;
	
    double * mu_vecs  = (double*)malloc(sizeof(double)*2*(M-1)*handle.N_space);
	
	d_t zero = 0.0;
	ghost_densemat_init_val(w,&zero);
	ghost_densemat_init_rand( v );
	if(!rank) printf("Status:  Pre SVQB ..\n");
	
	ghost_rayleigh_ritz( NULL, NULL, NULL, w, v,  handle.color_comm, GHOST_RAYLEIGHRITZ_DEFAULT, GHOST_SPMV_DEFAULT);
	
	t=v; v=w; w=t;
	
	
	if(!rank) printf("Status: Iteration ..\n");
	ghost_lidx break_cond = 0;
	int mm;
	for (mm=0;mm<handle.max_Iter;mm++){
		
		if (handle.save_memory_mode) ghost_densemat_destroy(w);
			cheb_poly_tmpl( v, M, window_coeff ,  mat, scale,   shift, mu_vecs, handle.globalSpmvmOptions);
			spMVM_counter += (M-1)*vtraits.ncols;
		if (handle.save_memory_mode) {  ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
		                         ghost_densemat_init_val(w,&zero);  }
		if(!rank){ 
		    int N_class=4;
	        
		    int M2=2*(M-1);
	        double * mu     = (double*)malloc(sizeof(double)*M2);
	        double * spc = (double *)malloc(sizeof(double)*M2*N_class);
	        
	        int n,b;
	        for (n=0;n<N_class;n++){
	          for(m=0;m<M2;m++) mu[m]=0.;
	          
	          for(m=0;m<M2;m++)for(b=0;b<list_ritz_num[n];b++) mu[m] += mu_vecs[m*vtraits.ncols+list_ritz[n][b]];
	          
	          cheb_toolbox_gibbs_Jackson (M2, mu, 1);
	          cheb_toolbox_kpm_1D_reconstruction_line (M2, mu, M2, spc + n*M2 );
		  }
		  
		  sprintf(file_name, "%s_Class_dense_%02d.dat", handle.file_prefix, mm);
		  FILE * out = fopen(file_name, "w");
	      double E = 1./scale; double scal_spc = 0.5/E;
          for(m=0;m<M2;m++)    {      fprintf(out,"%g", E*(2.*(m+0.5)/M2-1.) + shift);
			 for (n=0;n<N_class;n++)  fprintf(out,"\t%g", scal_spc*M2*spc[n*M2 + m]); 
						              fprintf(out,"\n");
	         }
	         
	        free(spc);fclose(out);
	        /*
	        spc = (double *)malloc(sizeof(double)*M2*vtraits.ncols);
	        for (n=0;n<vtraits.ncols;n++){
	          for(m=0;m<M2;m++) mu[m] = mu_vecs[m*vtraits.ncols+n];
	          cheb_toolbox_gibbs_Jackson (M2, mu, 1);
	          cheb_toolbox_kpm_1D_reconstruction_line (M2, mu, M2, spc + n*M2 );
	         }        
	        
	        sprintf(file_name, "%s_All_dense_%02d.dat", file_prefix, mm);
	        out = fopen(file_name, "w");
	        for(m=0;m<M2;m++)    {          fprintf(out,"%g", E*(2.*(m+0.5)/M2-1.) + shift);
			 for (n=0;n<vtraits.ncols;n++)  fprintf(out,"\t%g", scal_spc*M2*spc[n*M2 + m]); 
                                            fprintf(out,"\n");
	         }
	        fclose(out);
	        free(spc);free(mu);
            */
		}
		
		
		if( (break_cond & 1) && (break_cond & 2) ) break_cond |=4;
		if( (break_cond & 1) && (break_cond & 2) && handle.final_cut_mode ){
			ghost_lidx svd_offset;
			
			ghost_svd_deflation( &svd_offset, w, v, 0.1f);
			
			ghost_lidx * copy_ints = (ghost_lidx*)malloc(vtraits.ncols*sizeof(ghost_lidx));
			for ( i = 0; i<vtraits.ncols; i++ ) copy_ints[i] = i;
			
			vtraits.ncols -= svd_offset;
			
			ghost_densemat_destroy(v);
			ghost_densemat_create(&v, ghost_context_max_map(mat->context), vtraits);
			ghost_densemat_init_val(v,&zero);
			
			copy_vecs ( v, w, vtraits.ncols, copy_ints, copy_ints+svd_offset);
			
			ghost_densemat_destroy(w);
			ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
			ghost_densemat_init_val(w,&zero);
			
			free(copy_ints);
			
			t=v; v=w; w=t;
			
		}else{	
			t=v; v=w; w=t;
			 }
		ghost_rayleigh_ritz( mat, eigs, res, v,  w,  handle.color_comm, (ghost_rayleighritz_flags)(GHOST_RAYLEIGHRITZ_GENERALIZED|GHOST_RAYLEIGHRITZ_RESIDUAL), handle.globalSpmvmOptions); 
		spMVM_counter += 2*vtraits.ncols;
		
		
		for (i=0;i<vtraits.ncols;i++) InvPN[i] = 0.;
		//v->norm(v,InvPN,&two);
		
		if(!rank ){
			for (i=0;i<vtraits.ncols*handle.color_comm.csize;i++) printf("Num_%03d  %+.14e +/- %g  IPN: %g\n", i, eigs[i],res[i],std::real(InvPN[i]));
			printf("\n");
		}
		
		
		break_cond |= sort_ritz_vec( list_ritz_num, list_ritz, vtraits.ncols*handle.color_comm.csize, eigs,  res, handle.lambda_min, handle.lambda_max, handle.eps);
#ifdef GHOST_HAVE_MPI 
    MPI_Bcast( &break_cond, 1, ghost_mpi_dt_lidx , 0, MPI_COMM_WORLD);
#endif
		if(!rank){ printf("ITER  %d\n",mm);
			     printf("  %d  %d  %d  %d\n", list_ritz_num[0], list_ritz_num[1], list_ritz_num[2], list_ritz_num[3]  );
			     
			     for(m=0;m<4;m++){
			         printf("L%d: ",m);
			         if(list_ritz_num[m]) printf(" res = [%g : %g]", res[list_ritz[m][0]] , res[list_ritz[m][list_ritz_num[m]-1]] );
			         printf("\n  ");
			         for(i=0;i<list_ritz_num[m];i++) printf(" %d", list_ritz[m][i]);
			         printf("\n");
			      }
			
			double res_min=1., res_max=1., res_min_lr=1., res_min_l=1., res_min_r=1.;
			if(list_ritz_num[1]){ res_min = res[list_ritz[1][0]];
			                      res_max = res[list_ritz[1][list_ritz_num[1]-1]];}
			if(list_ritz_num[2])  res_min_l = res[list_ritz[2][0]];
			if(list_ritz_num[3])  res_min_r = res[list_ritz[3][0]];
			if ( res_min_l > res_min_r) res_min_lr = res_min_l; else res_min_lr = res_min_r;
			
			//if(file_res_counter)  fprintf(file_res_counter,"%d\t%d\t%ld\t%g\t%g\t%g\t%g\t%g\n", vtraits.ncols, M-1, spMVM_counter, res_min , res_max, res_min_lr, res_min_l, res_min_r);
		}
		
		
		if( (break_cond & 1) && (break_cond & 2) && handle.final_cut_mode ){
			
			vtraits.ncols = list_ritz_num[0];
			ghost_lidx * copy_ints = (ghost_lidx*)malloc(vtraits.ncols*sizeof(ghost_lidx));

			ghost_densemat_destroy(w);
			ghost_densemat_create(&w, ghost_context_max_map(mat->context), vtraits);
			ghost_densemat_init_val(w,&zero);
			
			qsort ( list_ritz[0], vtraits.ncols, sizeof(int) , int_compar);
			for ( i = 0; i<vtraits.ncols; i++ ) copy_ints[i] = i;
			copy_vecs ( w, v, vtraits.ncols, copy_ints, list_ritz[0]);
			
			ghost_densemat_destroy(v);
			ghost_densemat_create(&v, ghost_context_max_map(mat->context), vtraits);
			ghost_densemat_init_val(v,&zero);
			
			free(copy_ints);
			
			t=v; v=w; w=t;
		}
		
		if( (break_cond & 1) && (break_cond & 2) && !handle.final_cut_mode ) break;
		if( break_cond & 4 ) break;
	}
	
	
	//qsort ( list_ritz[1], list_ritz_num[1], sizeof(int) , int_compar);
	
	
	free(eigs);
	free(res);
	free(mu_vecs);
	free(InvPN);
	
	free(window_coeff);
	free(list_ritz[0]);
	free(list_ritz[1]);
	free(list_ritz[2]);
	free(list_ritz[3]);
	ghost_densemat_destroy(w);
	ghost_densemat_destroy(v);
	return 0;
}

extern "C" int cheb_kpm_z( ghost_densemat * x, ghost_sparsemat * mat, double scale,  double shift_d, int M, double * mu , ghost_spmv_flags *globalSpmvmOptions, ChebLoop_Options Loop_Options)
{ return cheb_kpm_tmpl< double, std::complex<double> >( x, mat, scale, shift_d, M, mu , globalSpmvmOptions, Loop_Options);}

extern "C" int cheb_kpm_d( ghost_densemat * x, ghost_sparsemat * mat, double scale,  double shift_d, int M, double * mu , ghost_spmv_flags *globalSpmvmOptions, ChebLoop_Options Loop_Options)
{ return cheb_kpm_tmpl< double, double >( x, mat, scale, shift_d, M, mu , globalSpmvmOptions, Loop_Options);}

extern "C" int cheb_dos_z( ghost_sparsemat * mat, double scale,  double shift, int M, int R, double * mu, ghost_spmv_flags globalSpmvmOptions, ChebLoop_Options Loop_Options)
{ return cheb_dos_tmpl< double, std::complex<double> >( mat, scale, shift, M, R, mu, globalSpmvmOptions, Loop_Options);}
    

extern "C" int cheb_dos_d( ghost_sparsemat * mat, double scale,  double shift, int M, int R, double * mu, ghost_spmv_flags globalSpmvmOptions, ChebLoop_Options Loop_Options)
{ return cheb_dos_tmpl< double, double >( mat, scale, shift, M, R, mu, globalSpmvmOptions, Loop_Options);}


extern "C" int cheb_poly_d( ghost_densemat * x, int M, double * coeff , ghost_sparsemat * mat, double scale,  double shift,  double * mu, ghost_spmv_flags spMVM_Options) 
{ return cheb_poly_tmpl< double, double >( x, M, coeff, mat, scale, shift, mu, spMVM_Options); }

extern "C" int cheb_poly_z( ghost_densemat * x, int M, std::complex<double> * coeff , ghost_sparsemat * mat, double scale,  double shift, double * mu, ghost_spmv_flags spMVM_Options) 
{ return cheb_poly_tmpl< double, std::complex<double> >( x, M, coeff, mat, scale, shift, mu, spMVM_Options); }

extern "C" int cheb_fd_window_d(ghost_sparsemat * mat, cheb_fd_handle handle )
{ return cheb_fd_window_tmpl< double, double >( mat, handle); }

extern "C" int cheb_fd_window_z(ghost_sparsemat * mat, cheb_fd_handle handle )
{ return cheb_fd_window_tmpl< double, std::complex<double> >( mat, handle); }

int cheb_mpoly_d(ghost_densemat ** xx, int N, ghost_densemat * x, int M, double * coeff , ghost_sparsemat * mat, double scale,  double shift,  ghost_spmv_flags spMVM_Options){
  
  // todo check DT
  
  ghost_densemat * y[2];
  ghost_densemat_create(&y[0], ghost_context_max_map(mat->context), x->traits);
  ghost_densemat_create(&y[1], ghost_context_max_map(mat->context), x->traits);
  double beta;
  double scale_spMVM;
  int n;
  ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
  spmvtraits.flags = spMVM_Options|GHOST_SPMV_SHIFT|GHOST_SPMV_SCALE|GHOST_SPMV_AXPBY;
  spmvtraits.alpha = &scale_spMVM;
  spmvtraits.beta = &beta;
  spmvtraits.gamma = &shift;
  
  ghost_densemat_init_densemat( y[0], x, 0, 0);

  for(n=0;n<N;n++) {
	   ghost_densemat_init_densemat( xx[n], x, 0, 0);
	   ghost_scale(xx[n], &coeff[n] );
   }

  beta = 0.;
  scale_spMVM = scale;
  ghost_densemat_init_val(y[1],&beta);
  
  ghost_spmv(y[1], mat, y[0], spmvtraits);
  for(n=0;n<N;n++)   ghost_axpy(xx[n], y[1], &coeff[N+n]);

  scale_spMVM = 2.*scale;
  beta = -1.;
  int m;
  for (m=2;m<M;m++){
      ghost_spmv(y[m&1], mat, y[(m-1)&1], spmvtraits);
      for(n=0;n<N;n++)         ghost_axpy(xx[n], y[m&1], &coeff[N*m+n]);
   }
   
  ghost_densemat_destroy(y[0]);
  ghost_densemat_destroy(y[1]);
  
  return 0;
}



int cheb_tp_z( ghost_densemat * x,                       double dt , ghost_sparsemat * mat, double scale,  double shift,  ghost_spmv_flags spMVM_Options, double eps){

    int M = (int)(2*dt/scale) + 100;
    std::complex<double> * coeff = (std::complex<double> *)malloc(sizeof(std::complex<double>)*M);
    cheb_toolbox_TP_coeff_single ( dt, scale, shift,  &M, coeff, eps );

    cheb_poly_z(  x, M, coeff , mat, scale, shift, NULL, spMVM_Options);

    free(coeff);

    return 0;
}
