#ifndef COLOR_COMM_HANDLE_H

#include <ghost.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct{
	int    psize;
	int    prank;
	int    csize;
	int    color;
	ghost_mpi_comm mpicomm;
	ghost_mpi_comm mpicomm_parent;
}color_comm_handle;


int init_color_comm_handle(color_comm_handle *cc,  ghost_context *ctx);

int color_comm_test( ghost_sparsemat * mat, int block_size, color_comm_handle cch, ghost_spmv_flags spMVM_Options);
int color_comm_test_2( ghost_sparsemat * mat, int block_size, color_comm_handle cch, ghost_spmv_flags spMVM_Options);

#ifdef __cplusplus
}
#endif

#define COLOR_COMM_HANDLE_H
#endif
