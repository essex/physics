#include "color_comm_handle.h"

int init_color_comm_handle(color_comm_handle *cc,  ghost_context *ctx){

#ifdef GHOST_HAVE_MPI
	if ( ctx->mpicomm_parent == MPI_COMM_NULL ){
		cc->mpicomm = MPI_COMM_NULL;
		cc->mpicomm_parent = ctx->mpicomm;
		MPI_Comm_size( ctx->mpicomm,        &(cc->psize) );
		MPI_Comm_rank( ctx->mpicomm,        &(cc->prank) );
#endif
		cc->csize = 1;
		cc->color = 0;
		cc->psize = 1;
		cc->prank = 0;
		
		
#ifdef GHOST_HAVE_MPI
	} else {
		cc->mpicomm_parent = ctx->mpicomm_parent;
		int psize, csize, prank, crank, rank, size;
		MPI_Comm_size( ctx->mpicomm_parent, &psize );
		MPI_Comm_size( ctx->mpicomm,        &csize );
		MPI_Comm_rank( ctx->mpicomm_parent, &prank );
		MPI_Comm_rank( ctx->mpicomm,        &crank );
		
		MPI_Comm_split( ctx->mpicomm_parent, crank, 0, &(cc->mpicomm) );
		
		MPI_Comm_rank( cc->mpicomm,        &rank );
		MPI_Comm_size( cc->mpicomm,        &size );
		cc->color = rank;
		cc->csize = size;
		cc->psize = psize;
		cc->prank = prank;
		GHOST_INFO_LOG("parent %2d/%d -> (Color %2d/%d  --  Ctx %2d/%d )",  prank, psize, rank, size, crank, csize )
		
		if( ctx->row_map->glb_perm || ctx->row_map->loc_perm ){
		GHOST_WARNING_LOG(" matrix permutation is not suported by multi layer communicationen" )
		}

	}
#endif


	return 0;
}

int color_comm_test( ghost_sparsemat * mat, int block_size, color_comm_handle cch, ghost_spmv_flags spMVM_Options){
	
	
	ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
	vtraits.datatype = mat->traits.datatype;
	vtraits.storage = GHOST_DENSEMAT_ROWMAJOR;
	vtraits.ncols = block_size;
	
	ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
	spmvtraits.flags = spMVM_Options;
	
	#ifdef GHOST_HAVE_MPI
		ghost_mpi_datatype dt;
		ghost_mpi_datatype_get(&dt,vtraits.datatype);
	#endif
	
	int rank;
	ghost_rank(&rank,mat->context->mpicomm);
	
	ghost_densemat *v, *w, *y;
	
	ghost_densemat_create(&v,ghost_context_max_map( mat->context), vtraits);
	ghost_densemat_create(&w,ghost_context_max_map( mat->context), vtraits);
	ghost_densemat_create(&y,ghost_context_max_map( mat->context), vtraits);
	
	float          szero = 0., smone = -1., stwo = 2.;
	double         dzero = 0., dmone = -1., dtwo = 2.;
	float  complex czero = 0., cmone = -1., ctwo = 2.;
	double complex zzero = 0., zmone = -1., ztwo = 2.;
	
	void * zero, * mone, * two;
	
	if (vtraits.datatype & GHOST_DT_COMPLEX) {
        if (vtraits.datatype & GHOST_DT_DOUBLE) {
            zero = &zzero;
            mone = &zmone;
            two  = &ztwo;
        } else {
            zero = &czero;
            mone = &cmone;
            two  = &ctwo;
        }
    } else {
        if (vtraits.datatype & GHOST_DT_DOUBLE) {
            zero = &dzero;
            mone = &dmone;
            two  = &dtwo;
        } else {
            zero = &szero;
            mone = &smone;
            two  = &stwo;
        }
    }
	
	
	ghost_densemat_init_rand( v );
	ghost_densemat_init_val(w,&zero);
	ghost_densemat_init_val(y,&zero);
	
	#ifdef GHOST_HAVE_MPI
    if( cch.csize > 1 ){
		//MPI_Bcast( v->val, v->traits.nrowshalopadded*v->traits.ncolspadded, dt, 0, cch.mpicomm);
		MPI_Bcast( v->val, 1, v->fullmpidt, 0, cch.mpicomm);
	          // nrowshalopadded * ncolspadded
		  }
	#endif
	
	
	ghost_spmv( w, mat, v, spmvtraits);
	
	int np_color = cch.color + 1;
	int nm_color = cch.color - 1;
	if ( np_color == cch.csize ) np_color = 0;
	if ( nm_color == -1 )        nm_color = cch.csize - 1;
	
	MPI_Status mpi_status;
	#ifdef GHOST_HAVE_MPI
	if( cch.csize > 1 ){
	MPI_Sendrecv( w->val, 1, w->fullmpidt, np_color, 0,
                  y->val, 1, y->fullmpidt, nm_color, 0,  cch.mpicomm, &mpi_status);
	}else{
		ghost_densemat_init_densemat(y,w,0,0);
	}
	#endif
	
	
	ghost_axpy( w, y , mone);
	
	double * norm = malloc(sizeof(double)*block_size);
	
	ghost_dot( norm, w, w);
	
	if( !rank ){
		int i;
		
		printf( "color %d:", cch.color );
		for(i=0;i<block_size;i++) printf( "  %.4e", norm[i] );
		printf( "\n" );
	}
	
	free(norm);
	ghost_densemat_destroy(v);
	ghost_densemat_destroy(w);
	ghost_densemat_destroy(y);
	
	return 0;
}


int color_comm_test_2( ghost_sparsemat * mat, int block_size, color_comm_handle cch, ghost_spmv_flags spMVM_Options){
	
	
	ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
	vtraits.datatype = mat->traits.datatype;
	vtraits.storage = GHOST_DENSEMAT_ROWMAJOR;
	vtraits.ncols = block_size;
	
	ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
	spmvtraits.flags = spMVM_Options;
	
	#ifdef GHOST_HAVE_MPI
		ghost_mpi_datatype dt;
		ghost_mpi_datatype_get(&dt,vtraits.datatype);
	#endif
	
	int rank;
	ghost_rank(&rank,mat->context->mpicomm);
	
	ghost_densemat *v, *w, *y;
	
	ghost_densemat_create(&v,ghost_context_max_map( mat->context), vtraits);
	ghost_densemat_create(&w,ghost_context_max_map( mat->context), vtraits);
	ghost_densemat_create(&y,ghost_context_max_map( mat->context), vtraits);
	
	float          szero = 0., smone = -1., stwo = 2.;
	double         dzero = 0., dmone = -1., dtwo = 2.;
	float  complex czero = 0., cmone = -1., ctwo = 2.;
	double complex zzero = 0., zmone = -1., ztwo = 2.;
	
	void * zero, * mone, * two;
	
	if (vtraits.datatype & GHOST_DT_COMPLEX) {
        if (vtraits.datatype & GHOST_DT_DOUBLE) {
            zero = &zzero;
            mone = &zmone;
            two  = &ztwo;
        } else {
            zero = &czero;
            mone = &cmone;
            two  = &ctwo;
        }
    } else {
        if (vtraits.datatype & GHOST_DT_DOUBLE) {
            zero = &dzero;
            mone = &dmone;
            two  = &dtwo;
        } else {
            zero = &szero;
            mone = &smone;
            two  = &stwo;
        }
    }
	
	
	
	#ifdef GHOST_HAVE_MPI
	if ( cch.csize > 1) {
		if ( !cch.color ){
			
			ghost_densemat_traits vtraits_tmp = vtraits;
			vtraits_tmp.ncols *= cch.csize;
			
			
			ghost_densemat *v_tmp, *v_tmp_view, *w_tmp;
			ghost_densemat_create(&v_tmp,ghost_context_max_map( mat->context), vtraits_tmp);
			
			ghost_densemat_init_rand( v_tmp );
			
			double * norm = malloc(sizeof(double)*vtraits_tmp.ncols);
			for(int i = 0; i < vtraits_tmp.ncols; i++) norm[i] = 0.;
			ghost_dot( norm, v_tmp, v_tmp);
			if(!rank) {
				printf( "All: ");
				for(int i = 0; i < vtraits_tmp.ncols; i++) printf( " %.4f", norm[i] );
				printf( "\n\n");
			}
			free(norm);
			
			for(int i=1; i<cch.csize; i++){
				ghost_densemat_create_and_view_densemat_cols( &v_tmp_view,v_tmp, vtraits.ncols, i*vtraits.ncols);
				ghost_densemat_init_densemat( v, v_tmp_view, 0, 0);
				MPI_Send( v->val, 1, v->fullmpidt, i, 0, cch.mpicomm);
				ghost_densemat_destroy(v_tmp_view);
			}
			ghost_densemat_create_and_view_densemat_cols( &v_tmp_view,v_tmp, vtraits.ncols, 0);
			ghost_densemat_init_densemat( v, v_tmp_view, 0, 0);
			ghost_densemat_destroy(v_tmp_view);
			
			ghost_densemat_destroy(v_tmp);
		}
		else{
			ghost_densemat_init_val(v,&zero);
			MPI_Status status;
			MPI_Recv( v->val, 1, v->fullmpidt, 0, 0, cch.mpicomm, &status);
		}
	}else 
	#endif 
		ghost_densemat_init_rand( v );
	
	double * norm = malloc(sizeof(double)*vtraits.ncols*cch.csize);
	for(int i = 0; i < vtraits.ncols*cch.csize; i++) norm[i] = 0.;
	ghost_dot( norm + vtraits.ncols*cch.color, v, v);
	
	#ifdef GHOST_HAVE_MPI
	if( cch.csize > 1 ){
		for(int i=0; i<cch.csize; i++ )
		MPI_Bcast( norm + i*vtraits.ncols, vtraits.ncols, MPI_DOUBLE, i, cch.mpicomm);
		  }
	#endif
	if( !rank && !cch.color ) {
				printf( "remote: ");
				for(int i = 0; i < vtraits.ncols*cch.csize; i++) printf( " %.4f", norm[i] );
				printf( "\n\n");
			}
	
	free(norm);
	ghost_densemat_destroy(v);
	
	return 0;
}
