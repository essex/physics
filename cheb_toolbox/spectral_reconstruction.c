#include <math.h>
#include <fftw3.h>

#include "cheb_toolbox.h"

int doubleDotProducts_2_ChebMoments( int M, int N, double * mu ){
	int n,m;
	double tmp;
	for(n=0;n<N;n++){
		tmp = 1./mu[0 + n];
		mu[  n]  = 1.;
		mu[N+n] *= tmp;
		for (m=2;m<=M;m++){ mu[(2*m-2)*N+n] = 2.*mu[(2*m-2)*N+n]*tmp - mu[  n];
			mu[(2*m-1)*N+n] = 2.*mu[(2*m-1)*N+n]*tmp - mu[N+n];  }
	}
	return 0;
}

int floatDotProducts_2_ChebMoments( int M, int N, float * mu ){
	int n,m;
	float tmp;
	for(n=0;n<N;n++){
		tmp = 1./mu[0 + n];
		mu[  n]  = 1.;
		mu[N+n] *= tmp;
		for (m=2;m<=M;m++){ mu[(2*m-2)*N+n] = 2.*mu[(2*m-2)*N+n]*tmp - mu[  n];
			mu[(2*m-1)*N+n] = 2.*mu[(2*m-1)*N+n]*tmp - mu[N+n];  }
	}
	return 0;
}

int cheb_toolbox_kpm_1D_reconstruction_line (int const M, double * const mu, int const N, double * const dos )
{
    //        mu[i] = g[i]*mu[i]
    //
    //                DCT ( mu[i] )_om             DCT-III  i =                    { 0      , 1,  ..  M-1    }
    // dos[i]  = -------------------------             ->  om = cos( PI*(i+0.5)/M) { 0.99.. ,  ..... -0.99.. }
    //             PI * sqrt( 1 - om^2 )
    static int M_old=0;
    static int init=1;
    static double  *a;
    static fftw_plan  dct_mu2f;
    if ( (init == 0) && (  M_old != M  )   ){
        free(a); fftw_destroy_plan( dct_mu2f ); M_old = 0; init=1;
        if ( N == 0 ) return -1;
    }
    if ( init == 1 ){
        M_old = M; init = 0;
        a = (double *) malloc ( 4*M*sizeof(double));
        dct_mu2f = fftw_plan_r2r_1d( 2*M , a, a + 2*M,  FFTW_REDFT01 , FFTW_ESTIMATE);}
        //  REDFT01 (DCT-III):
        //                       M-1  /                                \ .
        //    f[i] = mu[0] + 2 * SUM  | mu[j] * cos( PI*j*(i+0.5) /M ) | .
        //                       j=0  \                                / .

        int i,j,l;
        for (i = 0; i < M;     i++)      a[i] = mu[i];
        for (i = M; i < 2*M; i++)  a[i] = 0.0;
        fftw_execute( dct_mu2f );


        for(i = 0; i< N; i++) dos[i] = 0.0;
        double const dE = 2.0/N;
        double * E  = (double *)malloc((2*M+2)*sizeof(double));
        double * Ec = (double *)malloc((2*M+2)*sizeof(double));
        for( i = 0; i<2*M+1; i++) E[i] = - cos(i*M_PI/(2*M)); E[2*M+1]= 2.0;
        for( i = 0; i<2*M+2; i++) Ec[i] = E[i];

        int x0=0,x=1;
        for ( j = 0; j < N; j++){
            while ( (-1.0+(j+1)*dE) > E[x] )  x++;
            E[x0  ]= -1.0+ j   *dE;
            E[x   ]= -1.0+(j+1)*dE;
            for ( l = x0; l < x; l++) dos[j] += a[4*M-1-l]*(E[l+1]-E[l])/(Ec[l+1]-Ec[l]);
            dos[j] /= 2*M;
            E[x0] = Ec[x0];
            E[x ] = Ec[x ];
            x0=x-1;
        }
        free(Ec);
        free(E);

        return 0;
}

int cheb_toolbox_kpm_print_density(char * file_name, int M, int N, double * mu , double scale, double shift, double * info, void gibbs (int const , double * , int const ) ){
    int m,n;
    FILE * out = fopen(file_name,"w");
    double * spc = (double *)malloc(sizeof(double)*M);
    double E = 1./scale;
    double scal_spc = 0.5/E;
    
    for ( n=0;n<N;n++ ){
        if ( gibbs != NULL )
             gibbs(M, mu + n*M, 1);
        cheb_toolbox_kpm_1D_reconstruction_line (M, mu + n*M, M, spc );
        if (N == 1)
            for(m=0;m<M;m++)  fprintf(out,"%g\t%g\n", E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
        else if ( info == NULL )
          {
            for(m=0;m<M;m++)  fprintf(out,"%d\t%g\t%g\n", n, E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
            fprintf(out,"\n");
          }
        else
          {
            for(m=0;m<M;m++)  fprintf(out,"%g\t%g\t%g\n", info[n], E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
            fprintf(out,"\n");
          }

    }
    
    free(spc);
    fclose(out);
    
    return 0;
}

int cheb_toolbox_kpm_eval_density_at_engegie(double * dense, double omega, int M, int N, double * mu , double scale, double shift, void gibbs (int const , double * , int const ) ){
    int m,n;
    double * g = (double *)malloc(sizeof(double)*M);
    for(m=0;m<M;m++) g[m]=1.;
    gibbs(M, g, 1);
    
    omega = scale*(omega-shift);
    scale = scale/(M_PI*sqrt(1-omega*omega));
    
    double P[2];
    P[0] = 1.;
    P[1] = omega;
    
    for(n=0;n<N;n++) dense[n]  =   scale*g[0]*mu[0+n*M]*P[0];
    for(n=0;n<N;n++) dense[n] += 2*scale*g[1]*mu[1+n*M]*P[1];
    
    for(m=2;m<M;m++)
    {
        P[m&1] = 2*omega*P[(m-1)&1] - P[(m-2)&1];
        for(n=0;n<N;n++) dense[n] += 2*scale*g[m]*mu[m+n*M]*P[m&1];
    }
    free(g);
    
    return 0;
}
