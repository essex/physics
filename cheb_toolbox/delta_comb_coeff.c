#include "cheb_toolbox.h"
#include <math.h>
#include <stdlib.h>

double cheb_toolbox_get_window_poly_degree(double lambda_min, double lambda_max, double scale, double shift ){
	
	if( scale < 0. )  return 0;
	
	double x;
	if( lambda_min > lambda_max ) {x = lambda_min; lambda_min = lambda_max; lambda_max = x;}
	if( (fabs(scale*(lambda_min-shift)) > 1.) || (fabs(scale*(lambda_max-shift)) > 1.) ) return 0;
	double phi_min = acos(scale*(lambda_min-shift));
	double phi_max = acos(scale*(lambda_max-shift));
	
	
	
	return M_PI/(phi_min - phi_max);
}

double cheb_toolbox_get_window_poly_degree_Ja_degree(double lambda_min, double lambda_max, double scale, double shift  ){
	double x = scale*( 0.5*(lambda_max+lambda_min) -shift);
	double w = scale*fabs(lambda_min-lambda_max);
	double M = M_PI/(acos(x-0.5*w)-acos(x+0.5*w));
	double q,phi, cos_term, x_term, s, ds, sqrt_2 = sqrt(2.) ;
	int iter = 0;

	do{
		q = 1./(M+1.);
		phi = 2.*M_PI*q;
		cos_term = 1-cos(phi);
		x_term   = M-x*x*(M-1.);
		s = sqrt(( x_term * cos_term )*0.5*q);
		ds = (  ((1-x*x) * cos_term                      )
		       -(2.*M_PI * x_term * sin(phi))*q*q
		       -( x_term * cos_term                      )*q
		       )/( 2 * (M+1) * sqrt(2) * s );
		s -= w;
		M -= s/ds;
		iter++;
	}while( (fabs(s)> w*0.01) && (iter < 1000) );
return M;
}



int cheb_toolbox_delta_comb_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, int N_Comb, int rescal_opt ){
	
	if( scale < 0. )  return 0;
	
	double x;
	if( lambda_min > lambda_max ) {x = lambda_min; lambda_min = lambda_max; lambda_max = x;}
	if( (fabs(scale*(lambda_min-shift)) > 1.) || (fabs(scale*(lambda_max-shift)) > 1.) ) return 0;
	int m,n;
	double phi_min = acos(scale*(lambda_min-shift));
	double phi_max = acos(scale*(lambda_max-shift));

	int M =  (int)( (N_Comb)*2.*M_PI/(phi_min - phi_max));
	
	if(M>0){   *coeff = (double *)malloc( M * N_Comb* sizeof(double));
	
	  for (n=0;n<N_Comb;n++)  {
			double phi = phi_max + (n+0.5) * M_PI/M;
			double tmp=1.;
			if(rescal_opt) tmp = 1./(sin(phi)*M);
					(*coeff)[           n] = 1.           *tmp;
	    for (m = 1; m < M; m++)	(*coeff)[N_Comb*m + n] = 2.*cos(m*phi)*tmp;}
	
	}
	return M;
}


int cheb_toolbox_window_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha ){
	
	if( scale < 0. )  return 0;
	
	double x;
	if( lambda_min > lambda_max ) {x = lambda_min; lambda_min = lambda_max; lambda_max = x;}
	if( (fabs(scale*(lambda_min-shift)) > 1.) || (fabs(scale*(lambda_max-shift)) > 1.) ) return 0;
	int m;
	double phi_min = acos(scale*(lambda_min-shift));
	double phi_max = acos(scale*(lambda_max-shift));

	int M =  (int)( alpha*M_PI/(phi_min - phi_max));
	
	if(M>0){   *coeff = (double *)malloc( M * sizeof(double));
	
	(*coeff)[0] = (phi_min - phi_max)/M_PI;
	for (m = 1; m < M; m++)	(*coeff)[m] = 2.*(sin(m*phi_min)-sin(m*phi_max))/(m*M_PI);
	
	}
	return M;
}

int cheb_toolbox_window_coeffs_fix(double lambda_min, double lambda_max, double scale, double shift, double * coeff, int M ){
	
	if( scale < 0. )  return 0;
	
	double x;
	if( lambda_min > lambda_max ) {x = lambda_min; lambda_min = lambda_max; lambda_max = x;}
	if( (fabs(scale*(lambda_min-shift)) > 1.) || (fabs(scale*(lambda_max-shift)) > 1.) ) return 0;
	int m;
	double phi_min = acos(scale*(lambda_min-shift));
	double phi_max = acos(scale*(lambda_max-shift));
	
	coeff[0] = (phi_min - phi_max)/M_PI;
	for (m = 1; m < M; m++)	coeff[m] = 2.*(sin(m*phi_min)-sin(m*phi_max))/(m*M_PI);
	
	return 0;
}

