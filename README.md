---
# Disclaimer #
---

This is a pre-alpha release, so **please do not expect anything to work!** Just kidding... But some things may, and probably will, be broken. 
Nevertheless, please report any bugs, issues and feature requests to the [issue tracker](https://bitbucket.org/essex/physics/issues).

---
# Installation #
---

First of all, clone the git repository:

`git clone git@bitbucket.org:essex/physics.git && cd physics/`

It is preferrable to perform an out-of-source build, i.e., create a build directory first:

`mkdir build && cd build/`

To do a quick build with the default settings:

`cmake .. -DCMAKE_INSTALL_PREFIX=<where-to-install> -DGHOST_DIR=<ghost-prefix>/lib/ghost`

You can toggle shared/static libs with `-DBUILD_SHARED_LIBS=ON/OFF` (default: static).
Once the Makefile is present you can type

`make install`

For interactive specification of build options and variables, use ccmake to configure and generate a Makefile:

`ccmake ..`


