#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <assert.h>

#include "bapp_structures.h"



#ifdef GHOST_IDX64_LOCAL
#define repint_t uint64_t
#define repint_length 63
#else
#define repint_t uint32_t
#define repint_length 31
#endif

// bit at position i=0,1,...
// gives 0 or 1
#define bitati(x,i) ( ((x) >> (i)) & 1 )

int bitcount(repint_t i){
  int count = 0;
  //while (count < i){	count++;
  //			i = i & (i-1);  }
  
  while (i) {
    count += i & 1;
    i = i >> 1;
  }
  return count;
}


ghost_gidx binomial(int n, int k){
	
  if((k> n)||(k< 0)) return 0;
  if((k==n)||(k==0)) return 1;
  
  if( 2*k > n ) k=n-k;
  int i;
  ghost_gidx B=1;
  for (i=1;i<=k;i++) B = (B*((n-k)+i))/i;
  
  return B;
}

ghost_gidx power_of_2(ghost_gidx n) {
  // return bitshift_l( 1 , n );
  return 1 << n;
}

void print_bp(int n,repint_t bp) {

  int i;
  repint_t mask = power_of_2(n-1);
  for (i=0;i<n;i++) {
    if (bp & mask) {
      printf("+");
    }
    else {
      printf("-");
    }
    mask=mask >> 1;
  }
  printf("\n");

}


int obtain_hardparticles(hardparticles_dof * dof, ghost_gidx idx, void* rep) {
  assert(idx >=0 && idx < dof->nstates);

  // binary search
  ghost_gidx min_il = 0;
  ghost_gidx max_il = power_of_2(dof->nleft);
  repint_t il = 0;
  int i;
  for(i = 0; i < dof->n+1; i++) {
    il = (min_il + max_il)/2;
    if( idx >= dof->lutlead[il+1] )
      min_il = il+1;
    else if( max_il != min_il )
      max_il = il;
    else
      break;
  }

  repint_t bpleft, bpright,bp;

  bpleft=il;
  bpright=dof->lutrev[ idx - dof->lutlead[il] + dof->revcnt[dof->np-bitcount(il)]];

  bp=(bpleft << ((repint_t) dof->nright)) + bpright;
  
  *((repint_t*) rep) = bp;

  return 0;
}

int lookup_hardparticles(hardparticles_dof * dof, void* rep, ghost_gidx * idx) {

  repint_t bp= *((repint_t*) rep);
  
  assert(bitcount(bp) == dof->np);

  // split bit_pattern
  repint_t lbp = bp >>  dof->nright;
  repint_t tbp = bp & (power_of_2(dof->nright)-1);
  
  *idx = dof->lutlead[lbp] + dof->luttrail[tbp] -1;

  return 0;
}



// put N hard particles on L sites
// representation via bit patterns
int make_hardparticles_dof(int L, int N, qspace * ss, int pos) {

  ss->whichdof[pos]=hardparticles;

  hardparticles_dof *mydof;

  if (L>repint_length) {
    printf("Error in %s: bit patterns defined only for L <= %d\n",__func__,repint_length);
    abort();
  }

  mydof=malloc(sizeof(hardparticles_dof));

  mydof->n=L;
  mydof->np=N;

  mydof->nstates = binomial(L,N);

  mydof->nright = L/2;
  mydof->nleft = L - mydof->nright;

 mydof->lutlead  = malloc( sizeof(ghost_gidx)* (power_of_2(mydof->nleft)+1));
  mydof->luttrail = malloc( sizeof(ghost_gidx)*  power_of_2(mydof->nright)   );
  mydof->lutrev   = malloc( sizeof(ghost_gidx)* (power_of_2(L/2)+1));
  mydof->cnt      = malloc( sizeof(ghost_gidx)* (L+1) );
  mydof->revcnt   = malloc( sizeof(ghost_gidx)* (L+1) );

  // construct lookup tables [---
  ghost_gidx i;
  for(i=0;i<=L;i++)    mydof->cnt[i] = 0;
  for(i=0;i<=L;i++) mydof->revcnt[i] = 0;
  
  //arrange right bit patterns according to bit count
  for(i=0;i<power_of_2(mydof->nright);i++){
    ghost_gidx tmp = bitcount(i);
    mydof->cnt[tmp]++;
    mydof->luttrail[i] = mydof->cnt[tmp];
  }
		
  mydof->revcnt[0] = 0;
  for(i=1;i<=L;i++)  mydof->revcnt[i] = mydof->revcnt[i-1] + mydof->cnt[i-1];
  
  for(i=0;i<power_of_2(mydof->nright);i++){
    ghost_gidx tmp = bitcount(i);
    mydof->lutrev[mydof->revcnt[tmp]]=i;
    mydof->revcnt[tmp]++;
  }
  
  //recreate revcnt
  mydof->revcnt[0] = 0;
  for(i=1;i<=L;i++)  mydof->revcnt[i] = mydof->revcnt[i-1] + mydof->cnt[i-1];
  
  mydof->lutlead[0]=0;
  for(i=0;i<power_of_2(mydof->nleft);i++){
    ghost_gidx tmp;
    tmp = N-bitcount(i);
    if ( (tmp>=0) && (tmp<=N) ) {
      mydof->lutlead[i+1] = mydof->lutlead[i]+mydof->cnt[tmp];
    }
    else {
      mydof->lutlead[i+1] = mydof->lutlead[i];
    }
  }

  ss->dofs[pos]=mydof;

  ss->dofsize[pos].repsize=sizeof(repint_t);
  ss->dofsize[pos].nstates=mydof->nstates;


  return 0;

}



/* +++++++++++++++++++++++++++
    manipulation of states
+++++++++++++++++++++++++++ */


/* general */

// c^+_i c_i, b^+_i b_i, P^up_i = (J^z_i+0.5) */
static inline double _onethere(int i, repint_t *x) {
  //  if (&x & (repint_t) power_of_2(i-1)) {
  //  if ( (&x >> (i-1)) & 1) {
  if ( bitati(*x,i) ) {
    return 1.0;
  }
  else {
    return 0.0;
  }
}

// n_i n_j
static inline double _boththere(int i, int j, repint_t *x) {
  //  if ( ((&x >> (i-1)) & 1) && ((&x >> (j-1)) & 1) ) {
  if ( bitati(*x,i) && bitati(*x,j) ) {
    return 1.0;
  }
  else {
    return 0.0;
  }
}

/* fermions */

// x -> scalar * c^+_i c_j x
static inline double _hardfermion_cdicj(int i, int j, repint_t *x) {

  if (bitati(*x,i) || (! bitati(*x,j))) {
    return 0.0;
  }
  
  // fermionic sign
  int s1,s2;
  
  s1=bitcount(*x && (power_of_2(i)-1) );
  s2=bitcount(*x && (power_of_2(j)-1) );

  //flip bits
  *x = (*x ^ (1 << i));
  *x = (*x ^ (1 << j));

  if ((s1*s2)%2 == 0)
    {
      return 1.0;
    }
  else
    {
      return -1.0;
    }
}


// x -> scalar * (c^+_i c_j + c^+_j c_i) x
static inline double _hardfermion_hop(int i, int j, repint_t *x) {

  if (i==j) {
    if (bitati(*x,i)) {
      return 2.0;
    }
    else
      {
	return 0.0;
      }
  }

  /*
  if (bitati(*x,i) && bitati(*x,j)) return 0.0;
  if ((!bitati(*x,i)) && (!bitati(*x,j))) return 0.0;
  */

  // both (un-) occupied
  if (bitati(*x,i) == bitati(*x,j)) return 0.0;

  int il,ir;
  if (i<j) {
    il=i;
    ir=j;
  }
  else {
    il=j;
    ir=i;
  }


  int fsgn;
  fsgn=bitcount(*x & (power_of_2(ir)-power_of_2(il+1)) );

  //flip bits
  *x = (*x ^ (1 << i));
  *x = (*x ^ (1 << j));

  if (fsgn%2 == 0) {
    return 1.0;
  }
  else {
    return -1.0;
  }
  
}

/* hard bosons and spin 1/2 */

// b^+_i b_j + b^+_j b_i,
// sigma^+_i sigma^-_j + h.c.
static inline double _hardboson_hop(int i, int j, repint_t *x) {

  if (i==j) {
    return _onethere(i,x);
  }
  else {
    repint_t mask;

    mask = (1 << i) | (1 << j);

    if ( (*x & mask) || (~(*x) & mask ) ) {
      // both set or unset
      return 0.0;
    }
    else {
      // flip bits
      *x = *x ^ mask;
      return 1.0;
    }
  }
  
}


// c^+_i c_i
double fermions_cdc(qspace ss, qstate * qs, int idof, int i) {
  assert(ss.whichdof[idof] == hardparticles);
  
  repint_t * x = qs->repvec + ss.srep[idof];

  double v = _onethere(i,x);

  // qs->fpsi *= v;

  return v;
}

// x -> scalar * c^+_i c_j x
double fermions_cdicj(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == hardparticles);
  
  repint_t * x = qs->repvec + ss.srep[idof];

  double v = _hardfermion_cdicj(i,j,x);

  qs->fpsi *= v;

  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v; // somewhat stupid

}
  
// n_i n_j
double fermions_nn(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == hardparticles);
  
  repint_t * x = qs->repvec + ss.srep[idof];

  double v = _boththere(i,j,x);

  // qs->fpsi *= v;

  return v;
}



// x -> scalar * (c^+_i c_j + c^+_j c_i) x
double fermions_hop(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == hardparticles);
  
  repint_t * x = qs->repvec + ss.srep[idof];

  double v = _hardfermion_hop(i,j,x);
  
  qs->fpsi *= v;

  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v;
}


// spins 1/2

double spins_zz(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == hardparticles);

  repint_t * x = qs->repvec + ss.srep[idof];

  //  double v = _boththere(i,j,x) - 0.25*_onethere(i,x) - 0.25*_onethere(j,x) + 0.25;
  //  return v;  
  
  if ( bitati(*x,i) == bitati(*x,j) ) {
    return 0.25;
  }
  else {
    return -0.25;
  }

  // qs->fpsi *= v;
    
}

// x -> scalar * (c^+_i c_j + c^+_j c_i) x
double spins_twoflip(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == hardparticles);
  
  repint_t * x = qs->repvec + ss.srep[idof];

  //  double v = 0.5 *  _hardboson_hop(i,j,x);
  double v;
  if (bitati(*x,i) == bitati(*x,j)) {
    v=0.0;
  }
  else {
    v=0.5;
    //flip bits
    *x = (*x ^ (1 << i));
    *x = (*x ^ (1 << j));
  }
  
  qs->fpsi *= v;

  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v;
}
