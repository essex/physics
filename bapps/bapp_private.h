#ifndef BAPP_PRIVATE_H
#define BAPP_PRIVATE_H

#include <ghost.h>

#include <bapp_aux.h>
#include <bapp_binary.h>
#include <bapp_structures.h>
#include <bapp_models.h>

#endif
