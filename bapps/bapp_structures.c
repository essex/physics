#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <assert.h>

#include "bapp_structures.h"

/* routines for single degree of freedom */

static inline ghost_gidx _make_cnt(int ineq, int n, int m, int s, ghost_gidx *cnt)
{
	ghost_gidx ncnt;
	int i,j,k;
	
	ncnt=0;
	
	for (i=0;i<n*(s+1);i++)
	{ 
		cnt[i]=0;
	}
	
	for (i=0;i<=m && i<=s;i++)
	{
		cnt[i]=1;
	}
	for (j=2;j<=n;j++)
	{
		for (k=0;k<=s;k++)
		{
			for (i=0;i<=m && i<=k;i++)
			{
				cnt[(s+1)*(j-1)+k]=cnt[(s+1)*(j-1)+k]+cnt[(s+1)*(j-2)+(k-i)];
			}
		}
	}
	/* total number of states */
	int ntot;
	if (ineq)
	{
		ntot=0;
		for (i=0;i<=s;i++)
		{
			ntot=ntot+cnt[(n-1)*(s+1)+i];
		}
	}
	else
	{
		ntot=cnt[n*(s+1)-1];
	}
	
	return ntot;
}


// TODO: include check on range of x[i] \in 0,..,M-1
// this routine (c/s)hould be optimized further
static inline ghost_gidx _look(int ineq, int n, int s, ghost_gidx *cnt, int *x)
{
	int i,j;
	int sum;
	ghost_gidx idx;
	
	idx=0;
	sum=x[n-1];
	
	for (i=2;i<=n;i++)
	{
		sum=sum+x[n-i];
		for (j=0;j<x[n-i];j++)
		{
			idx=idx+cnt[(s+1)*(i-2)+(sum-j)];
		}
	}
	
	if (ineq)
	{
		for (j=0;j<sum;j++)
		{
			idx=idx+cnt[(n-1)*(s+1)+j];
		}
	}

	return idx;
}

// specifically for m=1 and equality constraint (fermions, spin 1/2)
static inline ghost_gidx _look_12(int ineq, int n, int s, ghost_gidx *cnt, int *x)
{
	int i,j;
	int sum;
	ghost_gidx idx;
	
	idx=0;
	sum=x[n-1];
	
	for (i=2;i<=n;i++)
	{
		if (x[n-i])
		{
			sum++;
			idx+=cnt[(s+1)*(i-2)+sum];
		}
	}
	
	return idx;
}

static inline int _give(int ineq, int n, int s, ghost_gidx *cnt, ghost_gidx idx, int *x)
{
	int i,nn;
	int sum;
	
	if (ineq)
	{
		sum=0;
		while (idx>=cnt[(n-1)*(s+1)+sum])
		{
			idx=idx-cnt[(n-1)*(s+1)+sum];
			sum++;
		}
	}
	else
	{
		sum=s;
	}
		
	for (nn=1;nn<=n;nn++)
	{
		if (nn==n)
		{
			x[nn-1]=sum;
		}
		else
		{
			x[nn-1]=0;
			i=sum;
			while (idx >= cnt[(s+1)*(n-nn-1)+i])
			{
				x[nn-1]++;
				idx=idx-cnt[(s+1)*(n-nn-1)+i];
				i=i-1;
			}
			sum=sum-x[nn-1];
		}
	}
	
	return 0;
}


/* ***  manipulation of states *** */

/* fermions */

// c^+_i c_i
static inline double _fermion_cdc(int i, int *x)
{
	return (double) x[i];
}

// x -> scalar * c^+_i c_j x
static inline double _fermion_cdicj(int i, int j, int *x)
{
	if (x[i]==1 || x[j]==0)
	{
		return 0.0;
	}
	
	// fermionic sign
	int s1,s2;
	s1=0; s2=0;
	int k;
	for (k=0;k<i;k++) {s1=s1+x[k];}
	x[j]=0;
	for (k=0;k<j;k++) {s2=s2+x[k];}
	x[i]=1;
	
	if ((s1*s2)%2 == 0)
	{
		return 1.0;
	}
	else
	{
		return -1.0;
	}
}

// n_i n_j
static inline double _fermion_nn(int i, int j, int *x)
{
	if (x[i] && x[j])
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}

// x -> scalar * (c^+_i c_j + c^+_j c_i) x
static inline double _fermion_hop(int i, int j, int *x)
{
	if (i==j)
	{
		if (x[i])
		{
			return 2.0;
		}
		else
		{
			return 0.0;
		}
	}
	
	if (x[i]==0 && x[j]==0) return 0.0;
	if (x[i]==1 && x[j]==1) return 0.0;
	
	int il,ir;
	if (i<j)
	{
		il=i;
		ir=j;
	}
	else
	{
		il=j;
		ir=i;
	}
	int fsgn,k;
	fsgn=0;
	for (k=il+1;k<ir;k++)
	{
		if (x[k]) fsgn=1-fsgn;
	}
	x[i]=1-x[i];
	x[j]=1-x[j];
	if (fsgn)
	{
		return -1.0;
	}
	else
	{
		return 1.0;
	}
		
}


/* bosons */

// b^+_i b_i
static inline double _boson_bdb(int i, int *x)
{
	return (double) x[i];
}

// x -> scalar * b^+_i b_j x
// checks for max. bosons per site <=M
static inline double _boson_bdibj(int m, int i, int j, int *x)
{
	if (x[j]==0)
	{
		return 0.0;
	}
	if (x[i]>=m)
	{
		return 0.0;
	}
	else
	{
		x[i]++;
		x[j]--;
		return sqrt( (double) x[i]*(x[j]+1) );
	}
}


// spins: js = 2*SpinLength

// J^z_i
static inline double _spin_jz(int js, int i, int *x)
{
	return (double) x[i] - 0.5 * (double) js;
	//return (double) 2.*x[i] - 1.;
}

// J^+_i J^-_j
static inline double _spin_jpjm(int js, int i, int j, int *x)
{
	if (x[j]==0 || x[i] == js)
	{
		return 0.0;
	}
	else
	{
		double sc;
		sc=sqrt((double) (js-x[i])*(x[i]+1))*sqrt((double) x[j]*(js-x[j]+1));
		x[i]++;
		x[j]--;
		return sc;
		//return 1.0;
	}
}

// specifically for spin 1/2:  J^+ J^- + J^- J^+

// TODO



/* +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+ */
/* +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+ */

/*    wrappers for multiple degrees of freedom                   */


int obtain_fermions(fermions_dof * dof, ghost_gidx idx, void* rep) {
  assert(idx >=0 && idx < dof->nstates);
  _give(0,dof->n,dof->np,dof->cnt,idx,rep);
  return 0;
}

int lookup_fermions(fermions_dof * dof, void* rep, ghost_gidx * idx) {
  *idx=_look(0,dof->n,dof->np,dof->cnt,rep);
  return 0;
}




int obtain_consbosons(consbosons_dof * dof, ghost_gidx idx, void* rep) {
  assert(idx >=0 && idx < dof->nstates);
  _give(0,dof->n,dof->np,dof->cnt,idx,rep);
  return 0;
}

int lookup_consbosons(consbosons_dof * dof, void* rep, ghost_gidx * idx) {
  *idx=_look(0,dof->n,dof->np,dof->cnt,rep);
  return 0;
}

/* lookup/construct functions for multiple dofs */

ghost_gidx lookup_state(qspace ss, qstate * qs) {
	
  int i;
	
  //check whether idx is invalid
  if ((qs->idx) == INVALID_STATE) {
    return -1;
  }

  //check whether idx has to be looked up
  if (qs->idx == LOOKUP_STATE) {

    //check whether one dof is invalid
    for (i=0;i<ss.ndofs;i++) {
      if (qs->dofidx[i] == INVALID_STATE) {
	qs->idx= INVALID_STATE;
	return -1;
      }
    }

    //lookup individual dofs
    for (i=0;i<ss.ndofs;i++) {
      if (qs->dofidx[i] == LOOKUP_STATE) {
	// representation starts at qs->repvec+ss.srep[i]
	    // starts at qs->repvec+ss.srep[i];
	switch (ss.whichdof[i]) {
    
	case fermions:
	  lookup_fermions((fermions_dof *) ss.dofs[i], qs->repvec+ss.srep[i], &qs->dofidx[i]);
	  break;

	case consbosons:
	  lookup_consbosons((consbosons_dof *) ss.dofs[i], qs->repvec+ss.srep[i], &qs->dofidx[i]);  
	  break;

	case hardparticles:
	  lookup_hardparticles((hardparticles_dof *) ss.dofs[i], qs->repvec+ss.srep[i], &qs->dofidx[i]);
	  break;

	default:
	  abort();
	}
      }

    }

    //check whether indices in range (DEBUG only)
    for (i=0;i<ss.ndofs;i++) {
      assert(0 <= qs->dofidx[i] && qs->dofidx[i] < ss.dofsize[i].nstates);
    }
    
    //compute total index

    qs->idx = qs->dofidx[ss.ndofs-1];
    for (i=ss.ndofs-2;i>=0;i--) {
      qs->idx = qs->idx * ss.dofsize[i].nstates;
      qs->idx = qs->idx + qs->dofidx[i];
    }
    
  }

  assert(0 <= qs->idx && qs->idx < ss.ntotal);
  
  return qs->idx;
  
}



int obtain_state(qspace ss, ghost_gidx idx, qstate * qs) {
 
  if (idx<0 || idx>ss.ntotal) {
    qs->idx=-1;
    qs->fpsi=0.0;	
	return -1;
  }

  qs->idx=idx;

  int i;
 	
  for (i=0;i<ss.ndofs;i++) {
    qs->dofidx[i] = idx%ss.dofsize[i].nstates;
    idx=idx/ss.dofsize[i].nstates;
  }

  //obtain representation of individual dofs

  for (i=0;i<ss.ndofs;i++) {
    // starts at qs->repvec+ss.srep[i];
    switch (ss.whichdof[i]) {
    
    case fermions:
      obtain_fermions((fermions_dof *) ss.dofs[i], qs->dofidx[i], qs->repvec+ss.srep[i]);
      break;

    case consbosons:
      obtain_consbosons((consbosons_dof *) ss.dofs[i], qs->dofidx[i], qs->repvec+ss.srep[i]);  
      break;

    case hardparticles:
      obtain_hardparticles((hardparticles_dof *) ss.dofs[i], qs->dofidx[i], qs->repvec+ss.srep[i]);  
      break;
      
    default:
      abort();
    }
  }

  qs->fpsi=1.0;

  return 0;

}
  

int allocate_qstate(qspace ss , qstate * qs) {

  qs->idx=-1;
  qs->dofidx = malloc(ss.ndofs * sizeof (*qs->dofidx));
  qs->repvec = malloc(ss.sreptot);
  
  // TODO: check malloc
  return 0;
}

int copy_qstate(qspace ss, qstate * qs1, qstate * qs2) {

  qs2->idx=qs1->idx;
  memcpy(qs2->dofidx,qs1->dofidx,ss.ndofs * sizeof(*qs1->dofidx));
  memcpy(qs2->repvec,qs1->repvec,ss.sreptot);
  qs2->fpsi=qs1->fpsi;
  return 0;
}

int free_qstate(qspace ss, qstate *qs) {
  free(qs->dofidx);
  free(qs->repvec);
}


int allocate_qspace(int n, qspace * ss) {

  ss->ndofs=n;
  ss->dofs=malloc(n * sizeof(void*));
  ss->whichdof=malloc(n * sizeof(enum dof_t));
  ss->dofsize=malloc(n * sizeof(*ss->dofsize));
  //  ss->nstates=malloc(n * sizeof(ss->nstates));
  ss->idxfactor=malloc(n * sizeof(*ss->idxfactor));
  ss->srep=malloc(n * sizeof(*ss->srep));
  
  // TODO: check malloc
  return 0;
}

int prepare_qspace(qspace * ss) {

  int i;

  
  ss->sreptot=(size_t) 0;
  ss->ntotal=1;
  for (i=0;i<ss->ndofs;i++) {
    ss->sreptot = ss->sreptot + ss->dofsize[i].repsize;
    ss->ntotal = ss->ntotal * ss->dofsize[i].nstates;
  }

 

  ss->srep[0]=(size_t) 0;
  for (i=1;i<ss->ndofs;i++) {
    ss->srep[i] = ss->srep[i-1] + ss->dofsize[i-1].repsize;
  }
  
 

  //TODO: idxfactor 
  ss->idxfactor[0] = 1;
  return 0;
  for (i=1;i<ss->ndofs;i++) {
    ss->idxfactor[i]=ss->idxfactor[i-1] * ss->dofsize[i-1].nstates;
  }
  /*
    ss->idxfactor[0]=ss->dofsize[0].nstates;
    for (i=1;i<ss->ndofs;i++) {
    idxfactor[i]=idxfactor[i-1]*ss->dofsize[i].nstates;
    }
  */

  return 0;

}

// put N fermions on L sites
int make_fermions_dof(int L, int N, qspace * ss, int pos) {

  ss->whichdof[pos]=fermions;

  fermions_dof * mydof;

  mydof=malloc(sizeof(fermions_dof));

  mydof->n=L;
  mydof->np=N;

  mydof->cnt=malloc(	(N+1)*L * sizeof(*mydof->cnt));

  mydof->nstates=_make_cnt(0,L,1,N,mydof->cnt);

  ss->dofs[pos]=mydof;

  ss->dofsize[pos].repsize=L * sizeof(int);
  ss->dofsize[pos].nstates=mydof->nstates;

  return 0;

}

// put N bosons with particle number conservation on L sites
// per site <= NPS bosons
int make_many_bosons_dof(int L, int N, int NPS, qspace * ss, int pos) {

  ss->whichdof[pos]=consbosons;

  consbosons_dof * mydof;

  mydof=malloc(sizeof(consbosons_dof));

  mydof->n=L;
  mydof->np=N;
  mydof->npersite=NPS;

  mydof->cnt=malloc(	(N+1)*L * sizeof(*mydof->cnt));

  mydof->nstates=_make_cnt(0,L,NPS,N,mydof->cnt);
  
  ss->dofs[pos]=mydof;
  
  ss->dofsize[pos].repsize=L * sizeof(int);
  ss->dofsize[pos].nstates=mydof->nstates;

  return 0;

}



/* *** add entries to rows *** */

int add_qstate_to_row(qspace ss, qstate * qs, double alpha, ghost_lidx *nnz, ghost_gidx *cols, double *vals) {
  if (qs->fpsi == 0.0) {return 0;}
  if (alpha == 0.0) {return 0;}
  lookup_state(ss,qs);
  if (qs->idx<0) {return 0;}
  cols[*nnz]=qs->idx;
  vals[*nnz]=qs->fpsi * alpha;
  *nnz = *nnz + 1;
  // check for maxnnz
  return 0;
}


/* ***  manipulation of states *** */

// convention: diagonal operators do not multiply with the matrix element,
// but only return it

// off-diagonal operators change multiply with the matrix element

/* fermions */

// alternative operators. Bit pattern representations in bapp_binary.c

// c^+_i c_i
double alt_fermions_cdc(qspace ss, qstate * qs, int idof, int i) {
  assert(ss.whichdof[idof] == fermions);
  
  int * x = qs->repvec + ss.srep[idof];

  double v = _fermion_cdc(i,x);

  // qs->fpsi *= v;

  return v;
}

// x -> scalar * c^+_i c_j x
double alt_fermions_cdicj(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == fermions);
  
  int * x = qs->repvec + ss.srep[idof];

  double v = _fermion_cdicj(i,j,x);

  qs->fpsi *= v;

  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v; // somewhat stupid

}
  
// n_i n_j
double alt_fermions_nn(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == fermions);
  
  int * x = qs->repvec + ss.srep[idof];

  double v = _fermion_nn(i,j,x);

  // qs->fpsi *= v;

  return v;
}



// x -> scalar * (c^+_i c_j + c^+_j c_i) x
double alt_fermions_hop(qspace ss, qstate * qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == fermions);
  
  int * x = qs->repvec + ss.srep[idof];

  double v = _fermion_hop(i,j,x);
  
  qs->fpsi *= v;


  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v;
}

// x -> scalar * b^+_i b_j x
// checks for max. bosons per site <=M
double bosons_bdibj(qspace ss, qstate *qs, int idof, int i, int j) {
  assert(ss.whichdof[idof] == consbosons);

  int * x = qs->repvec + ss.srep[idof];

  int nps = ((consbosons_dof *) (ss.dofs[idof]))->npersite;

  double v = _boson_bdibj(nps,i,j,x);

  qs->fpsi *= v;
  
  qs->dofidx[idof]=LOOKUP_STATE;
  qs->idx=LOOKUP_STATE;

  return v;

}

// b^+_i b_i x
double bosons_bdb(qspace ss, qstate *qs, int idof, int i) {

  int * x = qs->repvec + ss.srep[idof];
  
  double v = _boson_bdb(i,x);
  
  // qs->fpsi *= v;
  
  return v;
}

double identity_state(qspace ss, qstate * qs) {
  // check for valid indices ?
  if (qs->idx == INVALID_STATE) {qs->fpsi = 0;}
  return qs->fpsi;
}

