//#include <essexamples.h>
#include <ghost.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <getopt.h>

// contains matfuncs_info_t, otherwise declared in matfuncs.h
#include "bapp.h"
//#include <essex-physics/matfuncs.h>

#define DP

#ifdef DP
GHOST_REGISTER_DT_D(vecdt) // vectors have double values
GHOST_REGISTER_DT_D(matdt) // matrix has double values
#define SQRT(a) sqrt(a)
#define FABS(a) fabs(a)
#define IMTQL1(a,b,c,d) imtql1_(a,b,c,d)
#else
GHOST_REGISTER_DT_S(vecdt) // vectors have float values
GHOST_REGISTER_DT_S(matdt) // matrix has float values
#define SQRT(a) sqrtf(a)
#define FABS(a) fabsf(a)
#define IMTQL1(a,b,c,d) imtql1f_(a,b,c,d)
#endif

#include "lanczos.h"

static char *matstr = NULL;

static int converged(matdt_t evmin)
{
    static matdt_t oldevmin = -1e9;

    int converged = FABS(evmin-oldevmin) < 1e-9;
    oldevmin = evmin;

    return converged;
}

static void lanczosStep(ghost_context_t *context, ghost_sparsemat *mat, ghost_densemat *vnew, ghost_densemat *vold,
        matdt_t *alpha, matdt_t *beta, ghost_spmv_flags opt)
{
    matdt_t minusbeta = -*beta;
    vnew->scale(vnew,&minusbeta);
    ghost_spmv(vnew, mat, vold, &opt);
    ghost_dot(alpha,vnew,vold);
    matdt_t minusalpha = -*alpha;
    vnew->axpy(vnew,vold,&minusalpha);
    ghost_dot(beta,vnew,vnew);
    *beta=SQRT(*beta);
    matdt_t recbeta = (matdt_t)1./(*beta);
    vnew->scale(vnew,&recbeta);
}


void my_create_context_and_matrix(ghost_sparsemat_fromRowFunc_t matfunc,ghost_context_t **ctx, ghost_sparsemat **mat, ghost_sparsematraits_t *mtraits)
{
        //ghost_sparsemat_fromRowFunc_t matfunc;
        matfuncs_info_t info;
        ghost_gidx DIM;
		 
        mtraits->format = GHOST_SPARSEMAT_CRS;
 		
        // query
        matfunc( -2, NULL, NULL, &info);

        ghost_sparsemat_src_rowfunc_t src = GHOST_SPARSEMAT_SRC_ROWFUNC_INITIALIZER;
        src.func = matfunc;
        DIM=info.nrows;
        src.maxrowlen =info.row_nnz;

	printf("Matrix info: dimension %ld\n",DIM);
        
        printf("ghost_context_create\n");
        ghost_context_create(ctx,DIM,DIM,GHOST_CONTEXT_DEFAULT,(void *)&src,GHOST_SPARSEMAT_SRC_FUNC,MPI_COMM_WORLD,1.0);
        printf("ghost_sparsemat_create\n");
        ghost_sparsemat_create(mat,*ctx,mtraits,1);
        printf("fromRowFunc\n");
        (*mat)->fromRowFunc(*mat, &src);
}


void my_process_options(int argc, char **argv)
{
    int c;
    ghost_hwconfig_t hwconfig = GHOST_HWCONFIG_INITIALIZER;

    while (1)
    {
        static struct option long_options[] =
        {
            {"cores",  required_argument, 0, 'c'},
            {"matrix",  required_argument, 0, 'm'},
//            {"chainlength", required_argument, 0, 'l'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "c:m:",
                long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c) {
	case 0:
	  /* If this option set a flag, do nothing else now. */
	  if (long_options[option_index].flag != 0)
	    break;
	  printf ("option %s", long_options[option_index].name);
	  if (optarg)
	    printf (" with arg %s", optarg);
	  printf ("\n");
	  break;
	  
	case 'c':
	  hwconfig.ncore = atoi(optarg);
	  break;
	  
	  
	case 'm':
	  matstr = optarg;
	  break;
	  
	case '?':
	  /* getopt_long already printed an error message. */
	  break;
	  
	default:
	  abort ();
        }
    }
    ghost_hwconfig_set(hwconfig);
}



static void *mainTask(void *arg)
{
    ghost_spmv_flags opt = GHOST_SPMV_DEFAULT;
    matdt_t alpha=0., beta=0.;
    int ferr, n, iteration, nIter = 500;
    matdt_t zero = 0.;
    int rank;
    const int printrank = 0;

    ghost_context_t *context;
    ghost_sparsemat *mat;
    ghost_densemat *vold;
    ghost_densemat *vnew;
    ghost_densemat *tmp;
    
    ghost_sparsematraits_t mtraits = GHOST_SPARSEMAT_TRAITS_INITIALIZER;
    ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;

    // the matrix function
    ghost_sparsemat_fromRowFunc_t matfunc;
        
    //essexamples_create_context_and_matrix(&context,&mat,&mtraits);
    printf("Create matrix\n");
    bapp_select_model(matstr,&matfunc);
    
    printf("Create context and matrix\n");
    my_create_context_and_matrix(matfunc,&context,&mat,&mtraits);
    // essexamples_set_spmv_flags(&opt);
    printf("Matrix created\n");
        
    opt |= GHOST_SPMV_AXPY;

    ghost_rank(&rank, context->mpicomm);

/*
  
    int numprocs;
    int myid;
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    printf("%d: We have %d processors\n", myid, numprocs);
    printf("My rank: %d,%d\n",myid,rank);
  
*/
    ghost_densemat_create(&vnew,context,vtraits);
    ghost_densemat_create(&vold,context,vtraits);

    vnew->fromScalar(vnew,&zero); // vnew = 0
    vold->fromRand(vold); // vold = random
    ghost_normalize(vold); // normalize vold
    vold->permute(vold,mat->context->permutation,GHOST_PERMUTATION_ORIG2PERM);

    matdt_t *alphas  = (matdt_t *)malloc(sizeof(matdt_t)*nIter);
    matdt_t *betas   = (matdt_t *)malloc(sizeof(matdt_t)*nIter);
    matdt_t *falphas = (matdt_t *)malloc(sizeof(matdt_t)*nIter);
    matdt_t *fbetas  = (matdt_t *)malloc(sizeof(matdt_t)*nIter);

    if (!alphas || !betas || !falphas || !fbetas) {
        fprintf(stderr,"Error in malloc!\n");
        exit(EXIT_FAILURE);
    }

    betas[0] = beta;

    for(iteration = 0, n=1; 
            iteration < nIter && !converged(falphas[0]); 
            iteration++, n++) 
    {
        lanczosStep(context,mat,vnew,vold,&alpha,&beta,opt);
        tmp = vnew;
        vnew = vold;
        vold = tmp;

        alphas[iteration] = alpha;
        betas[iteration+1] = beta;
        memcpy(falphas,alphas,n*sizeof(matdt_t)); // alphas and betas will be destroyed in imtql
        memcpy(fbetas,betas,n*sizeof(matdt_t));

        IMTQL1(&n,falphas,fbetas,&ferr);

        if(ferr != 0) printf("Error: the %d. eigenvalue could not be determined\n",ferr);
        if (rank == printrank) {
            printf("\rmin/max eigenvalue: %f/%f", falphas[0],falphas[n-1]);
        }
        fflush(stdout);
    }
    if (rank == printrank) {
        printf("%s, iterations: %d\n",converged(falphas[0])?" (converged!)":" (max. iterations reached!)",iteration);

    }
    essexamples_print_info(mat,printrank);

    vold->destroy(vold);
    vnew->destroy(vnew);
    ghost_context_destroy(context);
    mat->destroy(mat);

    return NULL;


}

int main(int argc, char* argv[])
{
    my_process_options(argc,argv);
    //essexamples_process_options(argc,argv);
    
    ghost_init(argc,argv); // has to be the first call

    ghost_task_t *t;
    ghost_task_create(&t,GHOST_TASK_FILL_ALL,0,&mainTask,NULL,GHOST_TASK_DEFAULT, NULL, 0);
    ghost_task_enqueue(t);
    ghost_task_wait(t);
    ghost_task_destroy(t);

    ghost_finalize();
    
    return EXIT_SUCCESS;
}
