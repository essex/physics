#include <stdio.h>
#include "bapp_aux.h"
#include "bapp_models.h"
#include "bapp_structures.h"
#include "bapp.h"

/* template function 

   row=-1: initialization
           expects parameters in nnz, vals
   row=-2: query
           returns matfuncs_info_t object via vals
   row=0..nrows-1:
           return row-th row of matrix
   row=-3:
           clean up
*/


int matfunc_template(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg) {

  // total number of states
  static ghost_gidx ns;
  // maximal number of entries per row
  static ghost_lidx maxnnz;
 
  //integer parameters
  static int iparam1,iparam2;
  //real parameters
  static double rparam1,rparam2;

  if (row==-1) {
    // initialization
    
    iparam1=(int) nnz[0];
    iparam2=(int) nnz[1];
    
    double * dvals = vals;
    rparam1=dvals[0]; 
    rparam2=dvals[1]; 

    ns=100;

    return 0;
  }
  else if( row == -2 ){
    // query	
    matfuncs_info_t *info = vals;

    info->version   = 1;
    info->base      = 0;
    info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
    info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
    info->nrows     = ns;
    info->ncols     = ns;
    info->row_nnz   = maxnnz;
    
    info->hermit    =  1;
    info->eig_info  =  0;
    
    return 0;
  }
  else if (0<= row && row < ns) {
    //wrap
    double * dvals = vals;
				
    //sort row entries
    
    sort_row(nnz,cols,vals);
					
    cols_error_check( ns , row, *nnz, cols );
	
    return 0;
		
  } 
  else if (row==-3) {
    // clean-up
  } 
  else { 
    printf("Wrong row index: %ld in matfunc -> ABORT\n",row);
    exit(EXIT_FAILURE);
  }
	
  // shouldn't get beyond this point
}





/* ************* */

int XXZSpinChain(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg)
{
  UNUSED(arg);
  /* parameters */
  static int chainlength;
  static int spinsup;
  static int useOBC;
  static double Jzz,Jxy;
	
  // total number of states
  static ghost_gidx ns;
  // maximal number of entries per row
  static ghost_lidx maxnnz;

  // quantum space
  static qspace ss;	
  
  if (row==-1) {
    // initialization
    
    chainlength=(int) nnz[0];
    spinsup=(int) nnz[1];
    useOBC=(int) nnz[2];
    double * dvals = vals;
    Jzz=dvals[0]; 
    Jxy=dvals[1]; 
    
    allocate_qspace(1, &ss);
    make_hardparticles_dof(chainlength,spinsup,&ss,0);
    prepare_qspace(&ss);
    
    //total number of states
    ns=ss.ntotal;
    
    //number of operators: one diagonal + n off-diagonal
    maxnnz=(ghost_lidx) chainlength+1;

    return 0;
  }
  else if( row == -2 ){
    // query	
    matfuncs_info_t *info = vals;
    
    info->version   = 1;
    info->base      = 0;
    info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
    info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
    info->nrows     = ns;
    info->ncols     = ns;
    info->row_nnz   = maxnnz;
    
    info->hermit    =  1;
    info->eig_info  =  0;
    
    
    return 0;
  }
  else if (0<= row && row < ns) {
  
    // quantum states
    qstate qs1,qs2;

    allocate_qstate(ss,&qs1);
    allocate_qstate(ss,&qs2);
  
    //get state
    obtain_state(ss,row,&qs1);

    ghost_lidx mynnz;
    ghost_gidx *mycols;
    double *myvals;

    mycols=malloc(maxnnz * sizeof *mycols);
    myvals=malloc(maxnnz * sizeof *myvals);
    mynnz=0;
      
    //loop over operators
    int i;
    
    //diagonal: J_z J_z
    if (Jzz != 0.0)
      {
	double v=0.0;
	for (i=0;i<chainlength-useOBC;i++) { 
	  v=v+spins_zz(ss,&qs1, 0, i,(i+1)%chainlength); 
	}
	add_qstate_to_row(ss,&qs1, v*Jzz ,&mynnz,mycols,myvals);
      }
    
    //off-diagonal: J_x J_x + J_z J_z = J_+ J_- + J_- J_+
    if (Jxy != 0.0) {
      for (i=0;i<chainlength-useOBC;i++) {
	copy_qstate(ss,&qs1,&qs2);
	spins_twoflip(ss,&qs2, 0, i,(i+1)%chainlength);
	if (mynnz==maxnnz) {printf("small maxnnz\n"); exit(EXIT_FAILURE);}
	add_qstate_to_row(ss,&qs2, Jxy ,&mynnz,mycols,myvals);
	
      }
	    
    }
    
    free_qstate(ss,&qs1);
    free_qstate(ss,&qs2);
      
		
    //sort row entries
      
    sort_row(&mynnz,mycols,myvals);
    
    cols_error_check( ns , row, mynnz, mycols );

    *nnz=mynnz;
    memcpy(cols,mycols,mynnz * sizeof *mycols);
    memcpy(vals,myvals,mynnz * sizeof *myvals);

    free(mycols);
    free(myvals);

    return 0;
      
  }
  else  { 
    printf("Wrong row index: %ld in matfunc -> ABORT\n",row);
    exit(EXIT_FAILURE);
  }
  
  // shouldn't get here
  return 1;
}


int SpinlessFermions_tV(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg) {
  UNUSED(arg);

  /* parameters */
  static int nchain;
  static int nfermions;
  static int useOBC;
  static double t,U;
  /*            */
	
	
  // total number of states
  static ghost_gidx ns;
  // maximal number of entries per row
  static ghost_lidx maxnnz;

  // quantum space
  static qspace ss;	
  
  if (row==-1) {
    // initialization
    
    nchain=(int) nnz[0];
    nfermions=(int) nnz[1];
    useOBC=(int) nnz[2];
    double * dvals = vals;
    t=dvals[0]; 
    U=dvals[1]; 
    
    allocate_qspace(1, &ss);
    //    make_fermions_dof(nchain,nfermions,&ss,0);
    make_hardparticles_dof(nchain,nfermions,&ss,0);
    prepare_qspace(&ss);
    
    //total number of states
    ns=ss.ntotal;
    
    //number of operators: one diagonal + n off-diagonal
    maxnnz=(ghost_lidx) nchain+1;

    return 0;
  }
  else if( row == -2 ){
    // query	
    matfuncs_info_t *info = vals;
    
    info->version   = 1;
    info->base      = 0;
    info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
    info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
    info->nrows     = ns;
    info->ncols     = ns;
    info->row_nnz   = maxnnz;
    
    info->hermit    =  1;
    info->eig_info  =  0;
    
    
    return 0;
  }
  else if (0<= row && row < ns) {
  
    // quantum states
    qstate qs1,qs2;

    allocate_qstate(ss,&qs1);
    allocate_qstate(ss,&qs2);
  
    //get state
    obtain_state(ss,row,&qs1);

    ghost_lidx mynnz;
    ghost_gidx *mycols;
    double *myvals;

    mycols=malloc(maxnnz * sizeof *mycols);
    myvals=malloc(maxnnz * sizeof *myvals);
    mynnz=0;
      
    //loop over operators
    int i;
    
    //diagonal: U
    if (U != 0.0)
      {
	double v=0.0;
	for (i=0;i<nchain-useOBC;i++) { 
	  v=v+fermions_nn(ss,&qs1, 0, i,(i+1)%nchain); 
	}
	add_qstate_to_row(ss,&qs1, v*U ,&mynnz,mycols,myvals);
      }
  
    /*  
    qs1.idx=LOOKUP_STATE;
    add_qstate_to_row(ss,&qs1,1.0,nnz,cols,vals);
    */
        
    /*
    copy_qstate(ss,&qs1,&qs2);
    qs2.idx=LOOKUP_STATE;
    add_qstate_to_row(ss,&qs2,1.0,nnz,cols,vals);
    */

    
    //off-diagonal: t
    if (t != 0.0) {
      for (i=0;i<nchain-useOBC;i++) {
	copy_qstate(ss,&qs1,&qs2);
	fermions_hop(ss,&qs2, 0, i,(i+1)%nchain);
	if (mynnz==maxnnz) {printf("small maxnnz\n"); exit(EXIT_FAILURE);}
	add_qstate_to_row(ss,&qs2, -t ,&mynnz,mycols,myvals);
	
      }
	    
    }
      
      
    free_qstate(ss,&qs1);
    free_qstate(ss,&qs2);
      
		
    //sort row entries
      
    sort_row(&mynnz,mycols,myvals);
    
    if (mynnz==0) { printf("Warnung: nnz=0\n"); }
          
    cols_error_check( ns , row, mynnz, mycols );

    *nnz=mynnz;
    memcpy(cols,mycols,mynnz * sizeof *mycols);
    memcpy(vals,myvals,mynnz * sizeof *myvals);

    free(mycols);
    free(myvals);

    return 0;
      
  }
  else  { 
    printf("Wrong row index: %ld in matfunc -> ABORT\n",row);
    exit(EXIT_FAILURE);
  }
  
  // shouldn't get here
  return 1;
}




int Hubbard(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg) {
  UNUSED(arg);

  /* parameters */
  static int nchain;
  static int nfermions;
  static int useOBC;
  static double t,U;
  /*            */
	
	
  // total number of states
  static ghost_gidx ns;
  // maximal number of entries per row
  static ghost_lidx maxnnz;

  // quantum space
  static qspace ss;	
  
  if (row==-1) {
    // initialization
    
    nchain=(int) nnz[0];
    nfermions=(int) nnz[1];
    useOBC=(int) nnz[2];
    double * dvals = vals;
    t=dvals[0]; 
    U=dvals[1]; 
    
    allocate_qspace(2, &ss);
    //make_fermions_dof(nchain,nfermions,&ss,0);
    //make_fermions_dof(nchain,nfermions,&ss,1);
    make_hardparticles_dof(nchain,nfermions,&ss,0);
    make_hardparticles_dof(nchain,nfermions,&ss,1);
    prepare_qspace(&ss);
    
    //total number of states
    ns=ss.ntotal;
    
    //number of operators: one diagonal + 2n off-diagonal
    maxnnz=(ghost_lidx) 2*nchain+1;

    return 0;
  }
  else if( row == -2 ){
    // query	
    matfuncs_info_t *info = vals;
    
    info->version   = 1;
    info->base      = 0;
    info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
    info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
    info->nrows     = ns;
    info->ncols     = ns;
    info->row_nnz   = maxnnz;
    
    info->hermit    =  1;
    info->eig_info  =  0;
    
    
    return 0;
  }
  else if (0<= row && row < ns) {
  
    // quantum states
    qstate qs1,qs2;

    allocate_qstate(ss,&qs1);
    allocate_qstate(ss,&qs2);
  
    //get state
    obtain_state(ss,row,&qs1);

    ghost_lidx mynnz;
    ghost_gidx *mycols;
    double *myvals;

    mycols=malloc(maxnnz * sizeof *mycols);
    myvals=malloc(maxnnz * sizeof *myvals);
    mynnz=0;
      
    //loop over operators
    int i;
    
    //diagonal: U
    if (U != 0.0)
      {
	double v=0.0;
	for (i=0;i<nchain;i++) { 
	  v=v+fermions_cdc(ss,&qs1, 0, i)*fermions_cdc(ss,&qs1, 1, i); 
	}
	add_qstate_to_row(ss,&qs1,U*v,&mynnz,mycols,myvals);
      }
  
    /*  
    qs1.idx=LOOKUP_STATE;
    add_qstate_to_row(ss,&qs1,1.0,nnz,cols,vals);
    */
        
    /*
    copy_qstate(ss,&qs1,&qs2);
    qs2.idx=LOOKUP_STATE;
    add_qstate_to_row(ss,&qs2,1.0,nnz,cols,vals);
    */

    
    //off-diagonal: t
    if (t != 0.0)
      {
	for (i=0;i<nchain-useOBC;i++) {
	  // printf("%d\n",i);
	  copy_qstate(ss,&qs1,&qs2);
	  fermions_hop(ss,&qs2, 0, i,(i+1)%nchain);
	  // printf("%f\n",qs2.fpsi);
	  if (mynnz==maxnnz) {printf("small maxnnz\n"); exit(EXIT_FAILURE);}
	  add_qstate_to_row(ss,&qs2, t ,&mynnz,mycols,myvals);
	  //	  printf("%d\n",*nnz);
	  // makes no sense here
	
	  
	}
	    
      }
      

    if (t != 0.0)
      {
	for (i=0;i<nchain-useOBC;i++) {
	  // printf("%d\n",i);
	  copy_qstate(ss,&qs1,&qs2);
	  fermions_hop(ss,&qs2, 1, i,(i+1)%nchain);
	  // printf("%f\n",qs2.fpsi);
	  if (mynnz==maxnnz) {printf("small maxnnz\n"); exit(EXIT_FAILURE);}
	  add_qstate_to_row(ss,&qs2, t ,&mynnz,mycols,myvals);
	  //	  printf("%d\n",*nnz);
	  // makes no sense here
	
	  
	}
	    
      }
      
    free_qstate(ss,&qs1);
    free_qstate(ss,&qs2);
      
		
    //sort row entries
      
    sort_row(&mynnz,mycols,myvals);
    
    if (mynnz==0) { printf("Warnung: nnz=0\n"); }
    
      
    cols_error_check( ns , row, mynnz, mycols );

    *nnz=mynnz;
    memcpy(cols,mycols,mynnz * sizeof *mycols);
    memcpy(vals,myvals,mynnz * sizeof *myvals);

    free(mycols);
    free(myvals);

    return 0;
      
  }
  else  { 
    printf("Wrong row index: %ld in matfunc -> ABORT\n",row);
    exit(EXIT_FAILURE);
  }
  
  // shouldn't get here
  return 1;
}




int BoseHubbard(ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg) {
  UNUSED(arg);
  /* parameters */
  static int nchain;
  static int nbosons;
  static int nbpersite;
  static int useOBC;
  static double t,U;
  /*            */
  
  // total number of states
  static ghost_gidx ns;
  // maximal number of entries per row
  static ghost_lidx maxnnz;
  
  // quantum space
  static qspace ss;	
  
  if (row==-1) {
    // initialization
    
    nchain=(int) nnz[0];
    nbosons=(int) nnz[1];
    nbpersite=(int) nnz[2];
    useOBC=(int) nnz[3];
    double * dvals = vals;
    t=dvals[0]; 
    U=dvals[1]; 
    
    allocate_qspace(1, &ss);
    make_many_bosons_dof(nchain,nbosons,nbpersite,&ss,0);
    prepare_qspace(&ss);
    
    //total number of states
    ns=ss.ntotal;
    
    //number of operators: one diagonal + 2n off-diagonal
    maxnnz=(ghost_lidx) 2*nchain+1;

    return 0;
  }
  else if( row == -2 ){
    // query	
    matfuncs_info_t *info = vals;
    
    info->version   = 1;
    info->base      = 0;
    info->symmetry  = GHOST_SPARSEMAT_SYMM_GENERAL;
    info->datatype  = GHOST_DT_DOUBLE|GHOST_DT_REAL;
    info->nrows     = ns;
    info->ncols     = ns;
    info->row_nnz   = maxnnz;
    
    info->hermit    =  1;
    info->eig_info  =  0;
    
    
    return 0;
  }
  else if (0<= row && row < ns) {
  
    // quantum states
    qstate qs1,qs2;

    allocate_qstate(ss,&qs1);
    allocate_qstate(ss,&qs2);
  
    //get state
    obtain_state(ss,row,&qs1);

    ghost_lidx mynnz;
    ghost_gidx *mycols;
    double *myvals;

    mycols=malloc(maxnnz * sizeof *mycols);
    myvals=malloc(maxnnz * sizeof *myvals);
    mynnz=0;
      
    //loop over operators
    int i;
    
    //diagonal: U
    if (U != 0.0) {
      double v=0.0;
      for (i=0;i<nchain;i++) { 
	double hp;
	hp=bosons_bdb(ss,&qs1,0,i);
	v=v+hp*(hp-1.0);
      }
      add_qstate_to_row(ss,&qs1,U*v,&mynnz,mycols,myvals);
    }

    //off-diagonal: t
    if (t != 0.0) {
      for (i=0;i<nchain-useOBC;i++) {
	copy_qstate(ss,&qs1,&qs2);
	bosons_bdibj(ss,&qs2, 0, i,(i+1)%nchain);
	add_qstate_to_row(ss,&qs2, t ,&mynnz,mycols,myvals);
	copy_qstate(ss,&qs1,&qs2);
	bosons_bdibj(ss,&qs2, 0, (i+1)%nchain,i);
	add_qstate_to_row(ss,&qs2, t ,&mynnz,mycols,myvals);
      }
    }
            
    free_qstate(ss,&qs1);
    free_qstate(ss,&qs2);
      
		
    //sort row entries
      
    sort_row(&mynnz,mycols,myvals);
    
    if (mynnz==0) { printf("Warnung: nnz=0\n"); }
    
      
    cols_error_check( ns , row, mynnz, mycols );

    *nnz=mynnz;
    memcpy(cols,mycols,mynnz * sizeof *mycols);
    memcpy(vals,myvals,mynnz * sizeof *myvals);

    free(mycols);
    free(myvals);

    return 0;
      
  }
  else  { 
    printf("Wrong row index: %ld in matfunc -> ABORT\n",row);
    exit(EXIT_FAILURE);
  }
  
  // shouldn't get here
  return 1;
}


/* ------------------------------------------------------------------ */

	



