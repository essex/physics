#ifndef BAPP_H
#define BAPP_H

#include <ghost.h>

// beware: same as in essex-physics/matfuncs.h
#ifndef MATFUNCS_INFO_T
#define MATFUNCS_INFO_T
typedef struct {

	int32_t version;
	int32_t base;
	int32_t symmetry;
	int32_t datatype;
	ghost_gidx nrows;
	ghost_gidx ncols;
	ghost_lidx row_nnz;

	int32_t      hermit;
	int32_t      eig_info;
	double       eig_down;
	double       eig_top;

	} matfuncs_info_t;
#endif

#define UNUSED(x) (void)(x)

void bapp_select_model(char *matstr, ghost_sparsemat_rowfunc *matfunc);

#endif
