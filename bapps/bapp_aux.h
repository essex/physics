#include <ghost.h>

int cols_error_check(ghost_gidx dim, ghost_gidx row, ghost_lidx nnz,  ghost_gidx * cols );
int sort_row(ghost_lidx *n, ghost_gidx *cind, double *val);

