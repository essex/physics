#include "bapp_aux.h"
#include <stdlib.h>

int cols_error_check(ghost_gidx dim, ghost_gidx row, ghost_lidx nnz,  ghost_gidx * cols ) {
	
  ghost_gidx i;
  for(i=0; i<nnz; i++) if( (cols[i] >= dim) || (cols[i] < 0) ) {printf("error: row %"PRIDX": col[%"PRIDX"] = %"PRIDX" >= %"PRIDX" \n", row, i, cols[i], dim); exit(0);}
  
  return 0;
	
}


/* sort & prune row vector */

static int mycmp(const void *a, const void *b) {
  ghost_gidx x = *(ghost_gidx *) a;
  ghost_gidx y = *(ghost_gidx *) b;
  if (x == y)
    return 0;
  else
    return (x < y) ? -1 : 1;
}

int sort_row(ghost_lidx *n, ghost_gidx *cind, double *val) {
  
  if (*n==0) return 0;
  
  ghost_lidx i,j;
  
  // sort
  size_t size_c=sizeof cind;
  size_t size_v=sizeof val;
  char *dummy = malloc (*n * (size_c+size_v));
  for (i=0;i<*n;i++) {
    memcpy(dummy+i*(size_c+size_v),       cind+i, size_c);
    memcpy(dummy+i*(size_c+size_v)+size_c, val+i,  size_v);
  }
		
  qsort(dummy,*n,size_c+size_v,mycmp);

  for (i=0;i<*n;i++) {
    memcpy(cind+i, dummy+i*(size_c+size_v),        size_c);
    memcpy(val+i,  dummy+i*(size_c+size_v)+size_c,  size_v);
  }
  
  free(dummy);
  
  

  // eliminate doublets
    
  ghost_gidx k;
  double v;
  k=cind[0];
  v=val[0];
  j=0;
  for (i=1;i<*n;i++) {
    if (cind[i]==k) {
      v=v+val[i];
    }
    else {
      if (v != 0.0) {
	cind[j]=k;
	val[j]=v;
	j++;
      }
      k=cind[i];
      v=val[i];
    }
  }
  // last element
  if (v != 0.0) {
    cind[j]=k;
    val[j]=v;
    j++;
  }
  
  *n=j;
  
  return 0;
}
