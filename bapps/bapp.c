#include <stdio.h>
#include <string.h>
#include <strings.h>
#include "bapp.h"
#include "bapp_models.h"

// --- parse options

#define argint 1
#define argreal 2
#define argbool 3

typedef struct {
  const char *name;
  int argtype;
} bappoptions;


int getbappopt(char *optstr, int nopts, const bappoptions *opts, void *vals) {
  
  const char delims[]=",";
  const char delims2[]="=";

  char *opt,*val;
  // parse options
  while (optstr != NULL)
    {
      val = strsep(&optstr,delims);
      opt = strsep(&val,delims2);
      //  printf("Option: |%s|, Value: |%s|\n",opt,val);
      int i;
      for (i=0;i<nopts;i++) {
	if (!strcasecmp(opt,opts[i].name)) {
	  //	  	  printf("Option detected: %d, %s\n",i,opts[i].name);
	  switch (opts[i].argtype) {
	  case argint:
	    *((int **) vals)[i] = atoi(val);
	    break;
	  case argreal:
	    *((double **) vals)[i] = atof(val);
	    break;
	  case argbool:
	    *((int **) vals)[i] = 1;  
	    // check for value passed to flag
	    break;
	  default:
	    abort();
	  }
	  break; // for loop
	}
      }
      //      printf("%d\n",i);
      if (i==nopts) {
	printf("-- Unknown BAPP option: ""%s"". Abort. \n",opt);
	exit(EXIT_FAILURE);
      }
    }

  return 0;
}

void prepare_model_spinchain(char *matopt, ghost_sparsemat_rowfunc *matfunc) {
  // default parameters
  int chainlength = 10;
  int spinsup = 5;
  int useOBC = 0;
  double Jzz=1.0;
  double Jxy=1.0;

  static bappoptions myoptions[] = {
    {"l", argint},
    {"jzz", argreal},
    {"jxy", argreal},
    {"OBC", argbool}
  };

  void * params[4]={&chainlength,&Jzz,&Jxy,&useOBC};

  getbappopt(matopt, 4, myoptions, &params);  

  spinsup=chainlength/2;

  ghost_lidx parint[3];
  double parfloat[2];
  
  parint[0]=chainlength;
  parint[1]=spinsup;
  parint[2]=useOBC;
  parfloat[0]=Jzz;
  parfloat[1]=Jxy;
  
  *matfunc = XXZSpinChain;
  // initialize. Not threadsafe.
  (*matfunc)( -1, parint, NULL, parfloat, NULL);
}

void prepare_model_tV(char* optstr, ghost_sparsemat_rowfunc *matfunc) {
  // default parameters
  int chainlength = 4;
  int nfermions=-1;
  int useOBC = 0;
  double t=1.0;
  double U=0.0;

  static bappoptions myoptions[] = {
    {"l", argint},
    {"nf", argint},
    {"t", argreal},
    {"U", argreal},
    {"OBC", argbool}
  };

  void * params[5]={&chainlength,&nfermions,&t,&U,&useOBC};

  getbappopt(optstr, 5, myoptions, &params);  

  if (nfermions<0) { 
    nfermions=chainlength/2; //half-filling
  }


  ghost_lidx parint[3];
  double parfloat[2];
  
  parint[0]=chainlength;
  parint[1]=nfermions; 
  parint[2]=useOBC; 
  parfloat[0]=t;
  parfloat[1]=U;
  
  *matfunc = SpinlessFermions_tV;
  // initialize. Not threadsafe.
  (*matfunc)( -1, parint, NULL, parfloat, NULL);
}


void prepare_model_hubbard(char* optstr, ghost_sparsemat_rowfunc *matfunc)
{
  ghost_lidx parint[2];
  double parfloat[2];
	
  // default parameters
  int chainlength = 4;
  int nfermions=-1;
  int useOBC = 0;
  double t=1.0;
  double U=0.0;



  static bappoptions myoptions[] = {
    {"l", argint},
    {"nf", argint},
    {"t", argreal},
    {"U", argreal},
    {"OBC", argbool}
  };

  void * params[5]={&chainlength,&nfermions,&t,&U,&useOBC};

  getbappopt(optstr, 5, myoptions, &params);  

  if (nfermions<0) { 
    nfermions=chainlength/2; //half-filling
  }

  parint[0]=chainlength;
  parint[1]=nfermions;
  parint[2]=useOBC; 
  parfloat[0]=t;
  parfloat[1]=U;
  
  *matfunc = Hubbard;
  // initialize. Not threadsafe.
  (*matfunc)( -1, parint, NULL, parfloat, NULL);
}


void prepare_model_bosehubbard(char* optstr, ghost_sparsemat_rowfunc *matfunc) {
  // default parameters
  int chainlength = 4;
  int nbosons=1;
  int useOBC = 0;
  double t=1.0;
  double U=0.0;
  int nbpersite=-1;

  static bappoptions myoptions[] = {
    {"l", argint},
    {"nb", argint},
    {"persite",argreal},
    {"t", argreal},
    {"U", argreal},
    {"OBC", argbool}
  };

  void * params[6]={&chainlength,&nbosons,&nbpersite,&t,&U,&useOBC};
  
  getbappopt(optstr, 6, myoptions, &params);  

  if (nbosons<0) { 
    nbosons=chainlength/2; //half-filling
  }

  if (nbpersite<0) {nbpersite=nbosons;}
  
  ghost_lidx parint[4];
  double parfloat[2];
  
  parint[0]=chainlength;
  parint[1]=nbosons;
  parint[2]=nbpersite; 
  parint[3]=useOBC; 
  parfloat[0]=t;
  parfloat[1]=U;
  
  *matfunc = BoseHubbard;
  // initialize. Not threadsafe.
  (*matfunc)( -1, parint, NULL, parfloat, NULL);
}


void bapp_select_model(char *matstr, ghost_sparsemat_rowfunc *matfunc) {
  const char delims[]=",";
  
  static char *matname = NULL;
  static char *matopt = NULL;
  
  matname = strsep(&matstr,delims);
  matopt=matstr;
  
  if (matname == NULL) {
    printf("No matrix name supplied\n");
    exit(EXIT_FAILURE);
  }
  else if (!strcasecmp(matname,"spin")) {
    printf("Spin chain\n");
    prepare_model_spinchain(matopt,matfunc);
  }
  else if (!strcasecmp(matname,"tV")) {
    printf("t-V model\n");
    prepare_model_tV(matopt,matfunc);
  }
  else if (!strcasecmp(matname,"boshub")) {
    printf("Bose Hubbard model\n");
    prepare_model_bosehubbard(matopt,matfunc);
  }
  else if (!strcasecmp(matname,"hubbard")) {
    printf("Hubbard model\n");
    prepare_model_hubbard(matopt,matfunc);
  }
  else {
    printf("Unknown matrix: \"%s\"\n",matname);
    exit(EXIT_FAILURE);
  }
  
}


