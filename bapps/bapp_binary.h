#include <ghost.h>

typedef struct {
  /* number of sites */
  int n;
  /* number of particles */
  int np;
  /* size of representation vector */
  size_t srep;
  /* number of states */
  ghost_gidx nstates;
  /* lookup tables */
  int nleft, nright;  //number of sites in left/right lookup table
  ghost_gidx *lutlead;
  ghost_gidx *luttrail;
  ghost_gidx *cnt;
  ghost_gidx *lutrev;
  ghost_gidx *revcnt;
} hardparticles_dof;

int obtain_hardparticles(hardparticles_dof * dof, ghost_gidx idx, void* rep);
int lookup_hardparticles(hardparticles_dof * dof, void* rep, ghost_gidx * idx);
int make_hardparticles_dof(int L, int N, qspace * ss, int pos);


