#include <ghost.h>



int XXZSpinChain(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int SpinlessFermions_tV(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int Hubbard(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);
int BoseHubbard(   ghost_gidx row, ghost_lidx *nnz, ghost_gidx *cols, void *vals, void *arg);

