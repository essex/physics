#include <ghost.h>

#define LOOKUP_STATE -2
#define INVALID_STATE -1

/* quantum states */

enum dof_t {
  fermions,
  bosons,
  consbosons,
  hardbosons,
  hardparticles,  // local d.o.f: 0 or 1 -> bit patterns
  spins,
  particle,
  // to do
  fewfermions,
  fewbosons,
  onespin
};

static const char *DOFT_STRING[] = {
  "many fermions", "many bosons", "many conserved bosons", "many hard bosons", "many hard particles", "many spins", "one particle", "few fermions", "few bosons","one spin"
};

typedef struct {
  /* size of representation vector */
  size_t repsize;
  /* number of states */
  ghost_gidx nstates;
} dof_size_t;

typedef struct {
  /* number of sites */
  int n;
  /* number of particles */
  int np;
  /* size of representation vector */
  size_t srep;
  /* count information */
  ghost_gidx *cnt;
  /* number of states */
  ghost_gidx nstates;
} fermions_dof;

typedef struct {
  /* number of sites */
  int n;
  /* number of particles */
  int np;
  /* number of particles per site */
  int npersite;
  /* size of representation vector */
  size_t srep;
  /* count information */
  ghost_gidx *cnt;
  /* number of states */
  ghost_gidx nstates;
} consbosons_dof;

typedef struct {
  /* number of sites */
  int n;
  /* number of particles */
  int np;
  /* size of representation vector */
  size_t srep;
  /* number of states */
  ghost_gidx nstates;
  /* lookup tables */
  int nleft, nright;  //number of sites in left/right lookup table
  ghost_gidx *lutlead;
  ghost_gidx *luttrail;
  ghost_gidx *cnt;
  ghost_gidx *lutrev;
  ghost_gidx *revcnt;
} hardparticles_dof;


typedef struct {
  /* number of dofs */
  int ndofs;
  /* which type of dof */
  enum dof_t *whichdof;
  /* the respective dofs */
  void **dofs; // void **dof;
  /* and the common information */
  dof_size_t *dofsize;
  /* total number of states */
  ghost_gidx ntotal;
  /* factors for conversion between dofidx and idx */
  ghost_gidx *idxfactor;
  /* total size of representation vector */
  size_t sreptot;
  /* start of representation of invidual dofs */
  size_t *srep;
} qspace;

typedef struct {
  /* index of state */
  ghost_gidx idx;
  /* index of individual dof */
  ghost_gidx *dofidx;
  /* representation vector */
  void *repvec; // char *rep;
  //  /* pointers to start of representation of individual dof */
  // void **dofinrep; // char *
  /* factor in front of basis state */
  double fpsi;
} qstate;

int obtain_fermions(fermions_dof * dof, ghost_gidx idx, void* rep);
int lookup_fermions(fermions_dof * dof, void* rep, ghost_gidx * idx);
int obtain_consbosons(consbosons_dof * dof, ghost_gidx idx, void* rep);
int lookup_consbosons(consbosons_dof * dof, void* rep, ghost_gidx * idx);
int obtain_hardparticles(hardparticles_dof * dof, ghost_gidx idx, void* rep);
int lookup_hardparticles(hardparticles_dof * dof, void* rep, ghost_gidx * idx);
ghost_gidx lookup_state(qspace ss, qstate * qs);
int obtain_state(qspace ss, ghost_gidx idx, qstate * qs);
int allocate_qstate(qspace ss , qstate * qs);
int copy_qstate(qspace ss, qstate * qs1, qstate * qs2);
int free_qstate(qspace ss, qstate *qs);
int allocate_qspace(int n, qspace * ss);
int prepare_qspace(qspace * ss);
int make_fermions_dof(int L, int N, qspace * ss, int pos);
int make_many_bosons_dof(int L, int N, int NPS, qspace * ss, int pos);
int make_hardparticles_dof(int L, int N, qspace * ss, int pos);
int add_qstate_to_row(qspace ss, qstate * qs, double alpha, ghost_lidx *nnz, ghost_gidx *cols, double *vals);
double fermions_cdc(qspace ss, qstate * qs, int idof, int i);
double fermions_cdicj(qspace ss, qstate * qs, int idof, int i, int j);
double fermions_nn(qspace ss, qstate * qs, int idof, int i, int j);
double fermions_hop(qspace ss, qstate * qs, int idof, int i, int j);
double identity_state(qspace ss, qstate * qs);

double bosons_bdibj(qspace ss, qstate *qs, int idof, int i, int j);
double bosons_bdb(qspace ss, qstate *qs, int idof, int i);

double spins_zz(qspace ss, qstate * qs, int idof, int i, int j);
double spins_twoflip(qspace ss, qstate * qs, int idof, int i, int j);

/*
ghost_gidx make_cnt(int ineq, int n, int m, int s, ghost_gidx *cnt);
ghost_gidx look(int ineq, int n, int s, ghost_gidx *cnt, int *x);
ghost_gidx look_12(int ineq, int n, int s, ghost_gidx *cnt, int *x);
int give(int ineq, int n, int s, ghost_gidx *cnt, ghost_gidx idx, int *x);
double fermion_cdc(int i, int *x);
double fermion_cdicj(int i, int j, int *x);
double fermion_nn(int i, int j, int *x);
double fermion_hop(int i, int j, int *x);
double boson_bdb(int i, int *x);
double boson_bdibj(int m, int i, int j, int *x);
double spin_jz(int js, int i, int *x);
double spin_jpjm(int js, int i, int j, int *x);
*/

