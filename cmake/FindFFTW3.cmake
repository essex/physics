# - Try to find fftw3
# Once done, this will define
#
#  FFTW3_FOUND - system has fftw3
#  FFTW3_INCLUDE_DIRS - the fftw3 include directories
#  FFTW3_LIBRARIES - link these to use fftw3
#

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(FFTW3_PKGCONF fftw3)

# Include dir
find_path(FFTW3_INCLUDE_DIR
  NAMES fftw3.h
  PATHS ${FFTW3_PKGCONF_INCLUDE_DIRS}
)

# Finally the library itself
find_library(FFTW3_LIBRARY
  NAMES fftw3
  PATHS ${FFTW3_PKGCONF_LIBRARY_DIRS}
)


libfind_process(FFTW3)

set(FFTW3_FOUND ${FFTW3_FOUND} PARENT_SCOPE)
set(FFTW3-NOTFOUND ${FFTW3-NOTFOUN} PARENT_SCOPE)
set(FFTW3_INCLUDE_DIRS ${FFTW3_INCLUDE_DIRS} PARENT_SCOPE)
set(FFTW3_LIBRARIES ${FFTW3_LIBRARIES} PARENT_SCOPE)
